import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { GrupocorreosService } from '../../services/grupocorreos.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-grupocorreos-add',
  templateUrl: './grupocorreos-add.component.html',
  styleUrls: ['./grupocorreos-add.component.css']
})
export class GrupocorreosAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Grupocorreos_ser: GrupocorreosService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addGrupocorreos(nombre,
descripcion,
abreviacion ) {
    this.Grupocorreos_ser.addGrupocorreos(nombre,
descripcion,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
