import { Component, OnInit } from '@angular/core';
import Grupocorreos from '../../models/Grupocorreos';
import { GrupocorreosService } from '../../services/grupocorreos.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-grupocorreos-get',
  templateUrl: './grupocorreos-get.component.html',
  styleUrls: ['./grupocorreos-get.component.css']
})

export class GrupocorreosGetComponent implements OnInit {

  grupocorreos: Grupocorreos[];

  constructor( private bs: GrupocorreosService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getGrupocorreos()
      .subscribe((data: Grupocorreos[]) => {
        this.grupocorreos = data;
    });
  }

  deleteGrupocorreos(id) {
    this.bs.deleteGrupocorreos(id).subscribe(res => {

      this.bs
      .getGrupocorreos()
      .subscribe((data: Grupocorreos[]) => {
        this.grupocorreos = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

