import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { GrupocorreosService } from '../../services/grupocorreos.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-grupocorreos-edit',
  templateUrl: './grupocorreos-edit.component.html',
  styleUrls: ['./grupocorreos-edit.component.css']
})
export class GrupocorreosEditComponent implements OnInit {
     angForm: FormGroup;
  grupocorreos: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: GrupocorreosService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editGrupocorreos(params['id']).subscribe(res => {
        this.grupocorreos = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateGrupocorreos(nombre,
descripcion,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateGrupocorreos(nombre,
descripcion,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['grupocorreos']);
   });
}
}
