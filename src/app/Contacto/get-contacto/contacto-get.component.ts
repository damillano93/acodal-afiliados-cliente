import { Component, OnInit } from '@angular/core';
import Contacto from '../../models/Contacto';
import { ContactoService } from '../../services/contacto.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-contacto-get',
  templateUrl: './contacto-get.component.html',
  styleUrls: ['./contacto-get.component.css']
})

export class ContactoGetComponent implements OnInit {

  contacto: Contacto[];

  constructor( private bs: ContactoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getContacto()
      .subscribe((data: Contacto[]) => {
        this.contacto = data;
    });
  }

  deleteContacto(id) {
    this.bs.deleteContacto(id).subscribe(res => {

      this.bs
      .getContacto()
      .subscribe((data: Contacto[]) => {
        this.contacto = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

