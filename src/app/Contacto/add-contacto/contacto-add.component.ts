import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Afiliado from '../../models/Afiliado';
import Tipocontacto from '../../models/Tipocontacto';

import { ContactoService } from '../../services/contacto.service';
import { AfiliadoService } from '../../services/afiliado.service';
import { TipocontactoService } from '../../services/tipocontacto.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-contacto-add',
  templateUrl: './contacto-add.component.html',
  styleUrls: ['./contacto-add.component.css']
})
export class ContactoAddComponent implements OnInit {
   Afiliado: Afiliado[];
Tipocontacto: Tipocontacto[];
   angForm: FormGroup;
  constructor(
private AfiliadoSer: AfiliadoService,
private TipocontactoSer: TipocontactoService,

   private fb: FormBuilder,
   private Contacto_ser: ContactoService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       afiliado: ['', Validators.required ],
tipocontacto: ['', Validators.required ],
valor: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addContacto(afiliado,
tipocontacto,
valor ) {
    this.Contacto_ser.addContacto(afiliado,
tipocontacto,
valor );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.AfiliadoSer
.getAfiliado()
.subscribe((data: Afiliado[]) => {
this.Afiliado = data;
});
this.TipocontactoSer
.getTipocontacto()
.subscribe((data: Tipocontacto[]) => {
this.Tipocontacto = data;
});
  }

}
