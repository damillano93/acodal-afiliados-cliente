import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Afiliado from '../../models/Afiliado';
import Tipocontacto from '../../models/Tipocontacto';

import { ContactoService } from '../../services/contacto.service';
import { AfiliadoService } from '../../services/afiliado.service';
import { TipocontactoService } from '../../services/tipocontacto.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-contacto-edit',
  templateUrl: './contacto-edit.component.html',
  styleUrls: ['./contacto-edit.component.css']
})
export class ContactoEditComponent implements OnInit {
  Afiliado: Afiliado[];
Tipocontacto: Tipocontacto[];
   angForm: FormGroup;
  contacto: any = {};

  constructor(private route: ActivatedRoute, private AfiliadoSer: AfiliadoService,
 private TipocontactoSer: TipocontactoService,

    private router: Router,
    public toastr: ToastrManager,
    private bs: ContactoService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        afiliado: ['', Validators.required ],
tipocontacto: ['', Validators.required ],
valor: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editContacto(params['id']).subscribe(res => {
        this.contacto = res;
      });
    });
     this.AfiliadoSer
.getAfiliado()
.subscribe((data: Afiliado[]) => {
this.Afiliado = data;
});
this.TipocontactoSer
.getTipocontacto()
.subscribe((data: Tipocontacto[]) => {
this.Tipocontacto = data;
});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateContacto(afiliado,
tipocontacto,
valor ) {
   this.route.params.subscribe(params => {
      this.bs.updateContacto(afiliado,
tipocontacto,
valor  , params['id']);
      this.showInfo();
      this.router.navigate(['contacto']);
   });
}
}
