import { Component, OnInit } from '@angular/core';
import Subafinidad from '../../models/Subafinidad';
import { SubafinidadService } from '../../services/subafinidad.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-subafinidad-get',
  templateUrl: './subafinidad-get.component.html',
  styleUrls: ['./subafinidad-get.component.css']
})

export class SubafinidadGetComponent implements OnInit {

  subafinidad: Subafinidad[];

  constructor( private bs: SubafinidadService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getSubafinidad()
      .subscribe((data: Subafinidad[]) => {
        this.subafinidad = data;
    });
  }

  deleteSubafinidad(id) {
    this.bs.deleteSubafinidad(id).subscribe(res => {

      this.bs
      .getSubafinidad()
      .subscribe((data: Subafinidad[]) => {
        this.subafinidad = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

