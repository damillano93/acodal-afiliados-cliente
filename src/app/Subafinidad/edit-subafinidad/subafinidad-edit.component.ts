import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { SubafinidadService } from '../../services/subafinidad.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-subafinidad-edit',
  templateUrl: './subafinidad-edit.component.html',
  styleUrls: ['./subafinidad-edit.component.css']
})
export class SubafinidadEditComponent implements OnInit {
     angForm: FormGroup;
  subafinidad: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: SubafinidadService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editSubafinidad(params['id']).subscribe(res => {
        this.subafinidad = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateSubafinidad(nombre,
descripcion,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateSubafinidad(nombre,
descripcion,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['subafinidad']);
   });
}
}
