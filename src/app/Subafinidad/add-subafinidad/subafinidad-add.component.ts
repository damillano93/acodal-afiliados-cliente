import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { SubafinidadService } from '../../services/subafinidad.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-subafinidad-add',
  templateUrl: './subafinidad-add.component.html',
  styleUrls: ['./subafinidad-add.component.css']
})
export class SubafinidadAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Subafinidad_ser: SubafinidadService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addSubafinidad(nombre,
descripcion,
abreviacion ) {
    this.Subafinidad_ser.addSubafinidad(nombre,
descripcion,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
