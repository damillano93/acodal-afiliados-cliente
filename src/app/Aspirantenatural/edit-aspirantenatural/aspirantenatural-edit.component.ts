import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Tipodocumento from '../../models/Tipodocumento';
import Ciudad from '../../models/Ciudad';
import Seccional from '../../models/Seccional';
import Grupoafinidad from '../../models/Grupoafinidad';

import { AspirantenaturalService } from '../../services/aspirantenatural.service';
import { TipodocumentoService } from '../../services/tipodocumento.service';
import { CiudadService } from '../../services/ciudad.service';
import { SeccionalService } from '../../services/seccional.service';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-aspirantenatural-edit',
  templateUrl: './aspirantenatural-edit.component.html',
  styleUrls: ['./aspirantenatural-edit.component.css']
})
export class AspirantenaturalEditComponent implements OnInit {
  Tipodocumento: Tipodocumento[];
Ciudad: Ciudad[];
Seccional: Seccional[];
Grupoafinidad: Grupoafinidad[];
Subafinidad: Array<any>;
   angForm: FormGroup;
  aspirantenatural: any = {};

  constructor(private route: ActivatedRoute, private TipodocumentoSer: TipodocumentoService,
 private CiudadSer: CiudadService,
 private SeccionalSer: SeccionalService,
 private GrupoafinidadSer: GrupoafinidadService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: AspirantenaturalService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        fecha: ['', Validators.required ],
nombres: ['', Validators.required ],
apellidos: ['', Validators.required ],
tipodocumento: ['', Validators.required ],
documento: ['', Validators.required ],
fechanacimiento: ['', Validators.required ],
ciudad: ['', Validators.required ],
direccion: ['', Validators.required ],
telefono: ['', Validators.required ],
celular: ['', Validators.required ],
email: ['', Validators.required ],
profesion: ['', Validators.required ],
anogrado: ['', Validators.required ],
matriculaprofesional: ['', Validators.required ],
universidad: ['', Validators.required ],
seccional: ['', Validators.required ],
subafinidad: ['', Validators.required ],
abeasdata: ['', Validators.required ],
lavadoactivos: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editAspirantenatural(params['id']).subscribe(res => {
        this.aspirantenatural = res;
      });
    });
     this.TipodocumentoSer
.getTipodocumento()
.subscribe((data: Tipodocumento[]) => {
this.Tipodocumento = data;
});
this.CiudadSer
.getCiudad()
.subscribe((data: Ciudad[]) => {
this.Ciudad = data;
});
this.SeccionalSer
.getSeccional()
.subscribe((data: Seccional[]) => {
this.Seccional = data;
});
this.GrupoafinidadSer
.getGrupoafinidad()
.subscribe((data: Grupoafinidad[]) => {
this.Grupoafinidad = data;

});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateAspirantenatural(fecha,
nombres,
apellidos,
tipodocumento,
documento,
fechanacimiento,
ciudad,
direccion,
telefono,
celular,
email,
profesion,
anogrado,
matriculaprofesional,
universidad,
seccional,
abeasdata,
lavadoactivos ) {
   this.route.params.subscribe(params => {
      this.bs.updateAspirantenatural(fecha,
nombres,
apellidos,
tipodocumento,
documento,
fechanacimiento,
ciudad,
direccion,
telefono,
celular,
email,
profesion,
anogrado,
matriculaprofesional,
universidad,
seccional,
abeasdata,
lavadoactivos  , params['id']);
      this.showInfo();
      this.router.navigate(['aspirantenatural']);
   });
}

changeCountry(count) {
  this.Subafinidad = this.Grupoafinidad.find(con => con.nombre === count).subafinidad;

}
}
