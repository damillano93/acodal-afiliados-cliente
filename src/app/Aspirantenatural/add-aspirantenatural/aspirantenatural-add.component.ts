import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Tipodocumento from '../../models/Tipodocumento';
import Ciudad from '../../models/Ciudad';
import Seccional from '../../models/Seccional';
import Grupoafinidad from '../../models/Grupoafinidad';

import { AspirantenaturalService } from '../../services/aspirantenatural.service';
import { TipodocumentoService } from '../../services/tipodocumento.service';
import { CiudadService } from '../../services/ciudad.service';
import { SeccionalService } from '../../services/seccional.service';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';


import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-aspirantenatural-add',
  templateUrl: './aspirantenatural-add.component.html',
  styleUrls: ['./aspirantenatural-add.component.css']
})
export class AspirantenaturalAddComponent implements OnInit {
   Tipodocumento: Tipodocumento[];
Ciudad: Ciudad[];
Seccional: Seccional[];
Grupoafinidad: Grupoafinidad[];
Subafinidad: Array<any>;
   angForm: FormGroup;
  constructor(
private TipodocumentoSer: TipodocumentoService,
private CiudadSer: CiudadService,
private GrupoafinidadSer: GrupoafinidadService,
private SeccionalSer: SeccionalService,

   private fb: FormBuilder,
   private Aspirantenatural_ser: AspirantenaturalService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       fecha: ['', Validators.required ],
nombres: ['', Validators.required ],
apellidos: ['', Validators.required ],
tipodocumento: ['', Validators.required ],
documento: ['', Validators.required ],
fechanacimiento: ['', Validators.required ],
ciudad: ['', Validators.required ],
direccion: ['', Validators.required ],
telefono: ['', Validators.required ],
celular: ['', Validators.required ],
email: ['', Validators.required ],
profesion: ['', Validators.required ],
anogrado: ['', Validators.required ],
matriculaprofesional: ['', Validators.required ],
universidad: ['', Validators.required ],
seccional: ['', Validators.required ],
subafinidad: ['', Validators.required ],
abeasdata: ['', Validators.required ],
lavadoactivos: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addAspirantenatural(fecha,
nombres,
apellidos,
tipodocumento,
documento,
fechanacimiento,
ciudad,
direccion,
telefono,
celular,
email,
profesion,
anogrado,
matriculaprofesional,
universidad,
seccional,
subafinidad,
abeasdata,
lavadoactivos ) {
    this.Aspirantenatural_ser.addAspirantenatural(fecha,
nombres,
apellidos,
tipodocumento,
documento,
fechanacimiento,
ciudad,
direccion,
telefono,
celular,
email,
profesion,
anogrado,
matriculaprofesional,
universidad,
seccional,
subafinidad,
abeasdata,
lavadoactivos );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.TipodocumentoSer
.getTipodocumento()
.subscribe((data: Tipodocumento[]) => {
this.Tipodocumento = data;
});
this.CiudadSer
.getCiudad()
.subscribe((data: Ciudad[]) => {
this.Ciudad = data;
});
this.SeccionalSer
.getSeccional()
.subscribe((data: Seccional[]) => {
this.Seccional = data;
});
this.GrupoafinidadSer
.getGrupoafinidad()
.subscribe((data: Grupoafinidad[]) => {
this.Grupoafinidad = data;

});
  }
  changeCountry(count) {
    this.Subafinidad = this.Grupoafinidad.find(con => con.nombre === count).subafinidad;

  }
}
