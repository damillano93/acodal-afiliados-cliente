import { Component, OnInit } from '@angular/core';
import Aspirantenatural from '../../models/Aspirantenatural';
import { AspirantenaturalService } from '../../services/aspirantenatural.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-aspirantenatural-get',
  templateUrl: './aspirantenatural-get.component.html',
  styleUrls: ['./aspirantenatural-get.component.css']
})

export class AspirantenaturalGetComponent implements OnInit {

  aspirantenatural: Aspirantenatural[];

  constructor( private bs: AspirantenaturalService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getAspirantenatural()
      .subscribe((data: Aspirantenatural[]) => {
        this.aspirantenatural = data;
    });
  }

  deleteAspirantenatural(id) {
    this.bs.deleteAspirantenatural(id).subscribe(res => {

      this.bs
      .getAspirantenatural()
      .subscribe((data: Aspirantenatural[]) => {
        this.aspirantenatural = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

