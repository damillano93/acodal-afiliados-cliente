import { Component, OnInit, PipeTransform, Pipe, } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Tipoafiliado from '../../models/Tipoafiliado';
import Categoria from '../../models/Categoria';
import Seccional from '../../models/Seccional';
import Ciudad from '../../models/Ciudad';
import Contacto from '../../models/Contacto';
import Grupocorreos from '../../models/Grupocorreos';
import Documento from '../../models/Documento';
import Tipodocumentosoporte from '../../models/Tipodocumentosoporte';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import { AfiliadoService } from '../../services/afiliado.service';
import { TipoafiliadoService } from '../../services/tipoafiliado.service';
import { CategoriaService } from '../../services/categoria.service';
import { SeccionalService } from '../../services/seccional.service';
import { CiudadService } from '../../services/ciudad.service';
import { ContactoService } from '../../services/contacto.service';
import { GrupocorreosService } from '../../services/grupocorreos.service';
import { DocumentoService } from '../../services/documento.service';
import { TipodocumentosoporteService } from '../../services/tipodocumentosoporte.service';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';
import { FacturaService } from '../../services/factura.service';
import { ToastrManager } from 'ng6-toastr-notifications';


@Component({
  selector: 'app-afiliado-detail',
  templateUrl: './afiliado-detail.component.html',
  styleUrls: ['./afiliado-detail.component.css']
})
export class AfiliadoDetailComponent implements OnInit {
  constructor(private route: ActivatedRoute, private TipoafiliadoSer: TipoafiliadoService,
    private CategoriaSer: CategoriaService,
    private http: HttpClient,
    private SeccionalSer: SeccionalService,
    private CiudadSer: CiudadService,
    private ContactoSer: ContactoService,
    private GrupocorreosSer: GrupocorreosService,
    private DocumentoSer: DocumentoService,
    private TipodocumentosoporteSer: TipodocumentosoporteService,
    private GrupoafinidadSer: GrupoafinidadService,
    private FacturaSer: FacturaService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: AfiliadoService,
    private _location: Location,
    private fb: FormBuilder) {
    this.createForm();
  }
  percentDone: number;
  elId: string;
  Isnatural = true;
  IsJuridico = true;
  IsFacturas = false;
  Facturas: Object;
  uploadSuccess: boolean;
  Tipoafiliado: Tipoafiliado[];
  Categoria: Categoria[];
  Seccional: Seccional[];
  Ciudad: Ciudad[];
  Contacto: Contacto[];
  Grupocorreos: Grupocorreos[];
  Documento: Documento[];
  documento: any = [];
  angForm: FormGroup;
  afiliado: any = {};
  grupoafinidad: any = {};
  fileToUpload: File = null;
  archivo: any = {};
  todasAfinidades = [];
  url = 'https://i.stack.imgur.com/l60Hf.png';
  city = '';
  Tipodocumentosoporte: Tipodocumentosoporte[];
  tipodoc = '';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  createForm() {
    this.angForm = this.fb.group({

    });
  }


  ngOnInit() {
    this.IsFacturas = true;

    this.route.params.subscribe(params => {
    this.elId = params['id'];
      this.bs.editAfiliado(params['id']).subscribe(res => {
        this.afiliado = res;
        this.GrupoafinidadSer
          .getGrupoafinidad()
          .subscribe((data) => {
            this.grupoafinidad = data;
            const grupAfinidades = {};
            for (const subusuario of this.afiliado.subafinidad) {
              for (const afinidad of this.grupoafinidad) {
                for (const subafinidadmia of afinidad.subafinidad) {
                  if (subusuario._id === subafinidadmia._id) {

                     if (grupAfinidades[afinidad.nombre]) {
                      grupAfinidades[afinidad.nombre].push(subusuario.nombre);
                     } else {
                      grupAfinidades[afinidad.nombre] = [];
                      grupAfinidades[afinidad.nombre].push(subusuario.nombre);
                     }

                  }
                }
              }
            }


            const nombres =  Object.getOwnPropertyNames(grupAfinidades);
            for (let i = 0; i < nombres.length; i++) {
              this.todasAfinidades[i] = {
                nombre: nombres[i],
                sub: grupAfinidades[nombres[i]]
              };
            }
          });

        if (this.afiliado.tipoafiliado[0]._id === '5d1f888bb0faed0009d6b4d5') {
          this.Isnatural = false;
          this.IsJuridico = true;
        }
        if (this.afiliado.tipoafiliado[0]._id === '5d1f88c8b0faed0009d6b4d6') {
          this.IsJuridico = false;
          this.Isnatural = true;

        }
        if (this.afiliado.docsoporte.length) {
          for (let i = 0; i < this.afiliado.docsoporte.length; i++) {
            if (this.afiliado.docsoporte[i].tipodocumentosoporte[0].nombre === 'LOGO DE EMPRESA') {
              this.url = this.afiliado.docsoporte[i].urldocumento;
            }
          }
        } else { (
          this.url = 'https://i.stack.imgur.com/l60Hf.png'
        );
        }
        if (this.afiliado.tipodocumento.length) {
          this.tipodoc = this.afiliado.tipodocumento[0].nombre;
        }
        if (this.afiliado.ciudad.length) {
          this.city = this.afiliado.ciudad[0].nombre;
        }
      });
    });

  }
  showInfo() {
    this.toastr.infoToastr('El archivo fue subido de manera correcta', 'Archivo Guardado');
  }
  getfacturas() {
    this.IsFacturas = false;
    this.FacturaSer.getFacturaByAfiliado(this.elId)
    .subscribe((data) => {
      this.Facturas = data;
    });
  }

  atras() {
    this._location.back();
  }


}
