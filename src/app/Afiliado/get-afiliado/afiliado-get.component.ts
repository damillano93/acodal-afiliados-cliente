import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import Afiliado from '../../models/Afiliado';
import Tipoafiliado from '../../models/Tipoafiliado';
import Categoria from '../../models/Categoria';
import Clasificacion from '../../models/Clasificacion';
import Tipodocumento from '../../models/Tipodocumento';
import Seccional from '../../models/Seccional';
import Grupoafinidad from '../../models/Grupoafinidad';
import Subafinidad from '../../models/Subafinidad';
import Ciudad from '../../models/Ciudad';

import { AfiliadoService } from '../../services/afiliado.service';
import { TipoafiliadoService } from '../../services/tipoafiliado.service';
import { CategoriaService } from '../../services/categoria.service';
import { ClasificacionService } from '../../services/clasificacion.service';
import { TipodocumentoService } from '../../services/tipodocumento.service';
import { SeccionalService } from '../../services/seccional.service';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';
import { SubafinidadService } from '../../services/subafinidad.service';
import { CiudadService } from '../../services/ciudad.service';

import { ToastrManager } from 'ng6-toastr-notifications';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-afiliado-get',
  templateUrl: './afiliado-get.component.html',
  styleUrls: ['./afiliado-get.component.css']
})

export class AfiliadoGetComponent implements OnInit {
  closeResult: string;
  dtOptions: DataTables.Settings = {};
  IsAdmin: Boolean;
  IsSeccional: Boolean;
  User = JSON.parse(localStorage.getItem('currentUser'));
  userSec = '';
  myTable: Boolean;
  afiliado: Afiliado[];
  afi: Array<any>;
Tipoafiliado: Tipoafiliado[];
Categoria: Categoria[];
Clasificacion: Clasificacion[];
Tipodocumento: Tipodocumento[];
Seccional: Seccional[];
Grupoafinidad: Grupoafinidad[];
Subafinidad: Array<any>;
Ciudad: Ciudad[];
Otro;
Vertodo: Boolean;
Verfiltro: Boolean;
ValueTipoafiliado: any;
ValueCategoria: any;
ValueClasificacion: any;
ValueSeccional: any;
ValueGrupoafinidad: any;
ValueSubafinidad: any;
ValueCiudad: any;

  constructor( private bs: AfiliadoService,
    private modalService: NgbModal,
    private TipoafiliadoSer: TipoafiliadoService,
private CategoriaSer: CategoriaService,
private ClasificacionSer: ClasificacionService,
private TipodocumentoSer: TipodocumentoService,
private SeccionalSer: SeccionalService,
private GrupoafinidadSer: GrupoafinidadService,
private SubafinidadSer: SubafinidadService,
private CiudadSer: CiudadService,
public toastr: ToastrManager,
private _location: Location) {}

  ngOnInit() {
    this.showloading();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[1, 'asc']],
      responsive: true,
  /* below is the relevant part, e.g. translated to spanish */
  language: {
    processing: 'Procesando...',
    search: 'Buscar:',
    lengthMenu: 'Mostrar _MENU_ elementos',
    info: 'Mostrando desde _START_ al _END_ de _TOTAL_ elementos',
    infoEmpty: 'Mostrando ningún elemento.',
    infoFiltered: '(filtrado _MAX_ elementos total)',
    infoPostFix: '',
    loadingRecords: 'Cargando registros...',
    zeroRecords: 'No se encontraron registros',
    emptyTable: 'No hay datos disponibles en la tabla',
    'paginate': {
      'first': '<i class="fa fa-fast-backward" aria-hidden="true"></i>',
      'last': '<i class="fa fa-fast-forward" aria-hidden="true"></i>',
      'next': '<i class="fa fa-step-forward large" aria-hidden="true"></i>',
      'previous': '<i class="fa fa-step-backward" aria-hidden="true"></i>'
  },
    aria: {
      sortAscending: ': Activar para ordenar la tabla en orden ascendente',
      sortDescending: ': Activar para ordenar la tabla en orden descendente'
    }
  }
    };
    const user = JSON.parse(localStorage.getItem('currentUser'));
    if (user.admin) {
      this.IsAdmin = false;
    } else {
      this.IsAdmin = true;
    }

    if (user.permisos === 'SECCIONAL') {
      this.IsSeccional = false;
    }
    this.userSec = user.seccional;
    this.Vertodo = true;
    this.Verfiltro = true;
    this.myTable = false;
    this.bs
    .getAfiliado()
    .subscribe((data: Afiliado[]) => {
      this.afiliado = data;
      this.myTable = true;

  });
    this.bs
      .getAfiliado()
      .subscribe((data: Afiliado[]) => {

        this.afi = data;
    });

    this.CategoriaSer
.getCategoria()
.subscribe((data: Categoria[]) => {
this.Categoria = data;
});
this.TipoafiliadoSer
.getTipoafiliado()
.subscribe((data: Tipoafiliado[]) => {
this.Tipoafiliado = data;
});
this.ClasificacionSer
.getClasificacion()
.subscribe((data: Clasificacion[]) => {
this.Clasificacion = data;
});
this.TipodocumentoSer
.getTipodocumento()
.subscribe((data: Tipodocumento[]) => {
this.Tipodocumento = data;
});
this.SeccionalSer
.getSeccional()
.subscribe((data: Seccional[]) => {
this.Seccional = data;
});
this.GrupoafinidadSer
.getGrupoafinidad()
.subscribe((data: Grupoafinidad[]) => {
this.Grupoafinidad = data;

});
this.SubafinidadSer
.getSubafinidad()
.subscribe((data: Subafinidad[]) => {
this.Subafinidad = data;
});
this.CiudadSer
.getCiudad()
.subscribe((data: Ciudad[]) => {
this.Ciudad = data;
});
  }

  deleteAfiliado(id) {
    this.bs.deleteAfiliado(id).subscribe(res => {
      this.bs
      .getAfiliado()
      .subscribe((data: Afiliado[]) => {
        this.afiliado = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}

showloading() {
  this.toastr.customToastr(

    `<div class="center">
    <h3 class="center" style='color:black;'>
    Cargando
    <img class="center" width="40px" height="40px"
    src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///
    wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/
    hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+
    QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnA
    dOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQ
    JCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGB
    MRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujI
    jK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQo
    AAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDg
    sGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIp
    z8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradyleso
    jEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WG
    c3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDI
    I7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />    </h3>

  </div>`,
  null,
  { enableHTML: true,
    position: 'top-center',
    toastTimeout: 1000 }
  );
}
uploadFile() {
this.toastr.customToastr(

  `<div class="center">
  <h3 class="center" style='color:black;'>
  Subiendo
  <img class="center" width="40px" height="40px"
  src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///
  wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/
  hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+
  QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnA
  dOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQ
  JCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGB
  MRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujI
  jK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQo
  AAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDg
  sGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIp
  z8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradyleso
  jEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WG
  c3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDI
  I7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />   </h3>

</div>`,
null,
{ enableHTML: true,
  position: 'top-center',
  toastTimeout: 2000 }
);
}

showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
ver() {
  this.bs
  .getAfiliado()
  .subscribe((data: Afiliado[]) => {

    this.afi = data;
});
}
buscar() {

 this.bs
  .getAfiliado()
  .subscribe((data: Afiliado[]) => {

    this.afi = data;
});

  const fil: {[k: string]: any} = {};

  if (this.ValueClasificacion) {
    fil.clasificacion = this.ValueClasificacion;
  }
  if (this.ValueTipoafiliado) {
    fil.tipoafiliado = this.ValueTipoafiliado;
  }
  if (this.ValueCategoria) {
    fil.categoria = this.ValueCategoria;
  }
  if (this.ValueSeccional) {
    fil.seccional = this.ValueSeccional;
  }
  if (this.ValueSubafinidad) {
    fil.subafinidad = this.ValueSubafinidad;
  }
  if (this.ValueCiudad) {
    fil.ciudad = this.ValueCiudad;
  }



  for (let i = 0; i < this.afi.length; i++) {
    this.afi[i].clasificacion = this.afi[i].clasificacion[0]._id;
    this.afi[i].tipoafiliado = this.afi[i].tipoafiliado[0]._id;
    this.afi[i].categoria = this.afi[i].categoria[0]._id;
    this.afi[i].seccional = this.afi[i].seccional[0]._id;
    if (this.afi[i].subafinidad.length) {
      this.afi[i].subafinidad = this.afi[i].subafinidad[0]._id;
    }

    this.afi[i].ciudad = this.afi[i].ciudad[0]._id;
  }


  this.Otro = this.afi.filter(function(item) {
    for (const key in fil) {
      if (item[key] === undefined || item[key] !== fil[key]) {
        return false;
      }
    }
    return true;
  });



}
onClasificacion(value: any) {this.ValueClasificacion = value; }
onTipoafiliado(value: any) {this.ValueTipoafiliado = value; }
onCategoria(value: any) {this.ValueCategoria = value; }
onSeccional(value: any) {this.ValueSeccional = value; }
onGrupoafinidad(value: any) {this.ValueGrupoafinidad = value; }
onSubafinidad(value: any) {this.ValueSubafinidad = value; }
onCiudad(value: any) {this.ValueCiudad = value; }

todo() {
 // this.ver();
  this.Vertodo = false;
  this.Verfiltro = true;
}
filtro() {
//  this.ver();
  this.Vertodo = true;
  this.Verfiltro = false;
}
changeCountry(count) {
  this.Subafinidad = this.Grupoafinidad.find(con => con.nombre === count).subafinidad;

}
atras() {
  this._location.back();
}

open(content) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}

}

