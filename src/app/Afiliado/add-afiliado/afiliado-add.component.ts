import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Tipoafiliado from '../../models/Tipoafiliado';
import Categoria from '../../models/Categoria';
import Clasificacion from '../../models/Clasificacion';
import Tipodocumento from '../../models/Tipodocumento';
import Seccional from '../../models/Seccional';
import Grupoafinidad from '../../models/Grupoafinidad';
import Ciudad from '../../models/Ciudad';
import Pais from '../../models/Pais';
import Estudio from '../../models/Estudio';
import Tipodocumentosoporte from '../../models/Tipodocumentosoporte';
import Documento from '../../models/Documento';
import { AfiliadoService } from '../../services/afiliado.service';
import { TipoafiliadoService } from '../../services/tipoafiliado.service';
import { CategoriaService } from '../../services/categoria.service';
import { ClasificacionService } from '../../services/clasificacion.service';

import { TipodocumentoService } from '../../services/tipodocumento.service';
import { SeccionalService } from '../../services/seccional.service';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';

import { CiudadService } from '../../services/ciudad.service';
import { PaisService } from '../../services/pais.service';
import { EstudioService } from '../../services/estudio.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { TipodocumentosoporteService } from '../../services/tipodocumentosoporte.service';
import { DocumentoService } from '../../services/documento.service';

import { environment } from '../../env/env';
@Component({
  selector: 'app-afiliado-add',
  templateUrl: './afiliado-add.component.html',
  styleUrls: ['./afiliado-add.component.css']
})
export class AfiliadoAddComponent implements OnInit {
  Country = 'Colombia';
  Tipoafiliado: Tipoafiliado[];
  Categoria: Categoria[];
  Clasificacion: Clasificacion[];
  Tipodocumento: Tipodocumento[];
  Seccional: Seccional[];
  Grupoafinidad: Grupoafinidad[];
  Subafinidad: Array<any>;
  Ciudad: Ciudad[];
  Estudio: Estudio[];
  angForm: FormGroup;
  form: FormGroup;
  formAfinidad: FormGroup;
  formDocumento: FormGroup;
  formBeneficiario: FormGroup;
  respuesta: any;
  estudios = [];
  estudiosAll = [];
  afinidades = [];
  afinidadesId = [];
  documentos = [];
  documentosId = [];
  beneficiarios = [];
  IsApellidos: Boolean;
  IsCiiu: Boolean;
  IsFechanacimiento: Boolean;
  IsNombre: Boolean;
  IsEmpresa: Boolean;
  ValidateStudies: Boolean;
  ValidateAfinidad: Boolean;
  ValidateDocumento: Boolean;
  ValidateBeneficiario: Boolean;
  Documento: Documento[];
  Tipodocumentosoporte: Tipodocumentosoporte[];
  tipodocsop: Tipodocumentosoporte[];
  fileToUpload: File = null;
  archivo: any = {};
  archivo2: any = {};
  Pais: Pais[];
  Ciudades: Array<any>;
  pretipo = '';
  elnombre = '';
  newciudad = '';
  idmunicipio = '5dd45baba37661000885033e';
  municipios = [
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Medellín',
        'id': '5e206cfac6788b7bda6084ed'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Abejorral',
        'id': '5e206cfac6788b7bda6084ee'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Abriaquí',
        'id': '5e206cfac6788b7bda6084ef'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Alejandría',
        'id': '5e206cfac6788b7bda6084f0'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Amagá',
        'id': '5e206cfac6788b7bda6084f1'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Amalfi',
        'id': '5e206cfac6788b7bda6084f2'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Andes',
        'id': '5e206cfac6788b7bda6084f3'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Angelópolis',
        'id': '5e206cfac6788b7bda6084f4'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Angostura',
        'id': '5e206cfbc6788b7bda6084f5'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Anorí',
        'id': '5e206cfbc6788b7bda6084f6'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Chimá',
        'id': '5e206cfbc6788b7bda6084f7'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Anza',
        'id': '5e206cfbc6788b7bda6084f8'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Apartadó',
        'id': '5e206cfbc6788b7bda6084f9'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Arboletes',
        'id': '5e206cfbc6788b7bda6084fa'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Argelia',
        'id': '5e206cfbc6788b7bda6084fb'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Armenia',
        'id': '5e206cfbc6788b7bda6084fc'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Barbosa',
        'id': '5e206cfcc6788b7bda6084fd'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Bello',
        'id': '5e206cfcc6788b7bda6084fe'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Betania',
        'id': '5e206cfcc6788b7bda6084ff'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Betulia',
        'id': '5e206cfcc6788b7bda608500'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Ciudad Bolívar',
        'id': '5e206cfcc6788b7bda608501'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Briceño',
        'id': '5e206cfcc6788b7bda608502'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Buriticá',
        'id': '5e206cfcc6788b7bda608503'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Cáceres',
        'id': '5e206cfcc6788b7bda608504'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Caicedo',
        'id': '5e206cfdc6788b7bda608505'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Caldas',
        'id': '5e206cfdc6788b7bda608506'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Campamento',
        'id': '5e206cfdc6788b7bda608507'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Cañasgordas',
        'id': '5e206cfdc6788b7bda608508'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Caracolí',
        'id': '5e206cfdc6788b7bda608509'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Caramanta',
        'id': '5e206cfdc6788b7bda60850a'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Carepa',
        'id': '5e206cfdc6788b7bda60850b'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Sampués',
        'id': '5e206cfdc6788b7bda60850c'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Carolina',
        'id': '5e206cfec6788b7bda60850d'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Caucasia',
        'id': '5e206cfec6788b7bda60850e'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Chigorodó',
        'id': '5e206cfec6788b7bda60850f'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Cisneros',
        'id': '5e206cfec6788b7bda608510'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Cocorná',
        'id': '5e206cfec6788b7bda608511'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Concepción',
        'id': '5e206cfec6788b7bda608512'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Concordia',
        'id': '5e206cfec6788b7bda608513'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Copacabana',
        'id': '5e206cfec6788b7bda608514'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Dabeiba',
        'id': '5e206cffc6788b7bda608515'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Don Matías',
        'id': '5e206cffc6788b7bda608516'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Ebéjico',
        'id': '5e206cffc6788b7bda608517'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'El Bagre',
        'id': '5e206cffc6788b7bda608518'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Entrerrios',
        'id': '5e206cffc6788b7bda608519'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Envigado',
        'id': '5e206cffc6788b7bda60851a'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Fredonia',
        'id': '5e206cffc6788b7bda60851b'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Giraldo',
        'id': '5e206cffc6788b7bda60851c'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Girardota',
        'id': '5e206cffc6788b7bda60851d'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Gómez Plata',
        'id': '5e206d00c6788b7bda60851e'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Nunchía',
        'id': '5e206d00c6788b7bda60851f'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Guadalupe',
        'id': '5e206d00c6788b7bda608520'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Guarne',
        'id': '5e206d00c6788b7bda608521'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Guatapé',
        'id': '5e206d00c6788b7bda608522'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Heliconia',
        'id': '5e206d00c6788b7bda608523'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Hispania',
        'id': '5e206d00c6788b7bda608524'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Itagui',
        'id': '5e206d00c6788b7bda608525'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Ituango',
        'id': '5e206d01c6788b7bda608526'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Pamplona',
        'id': '5e206d01c6788b7bda608527'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Jericó',
        'id': '5e206d01c6788b7bda608528'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'La Ceja',
        'id': '5e206d01c6788b7bda608529'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'La Estrella',
        'id': '5e206d01c6788b7bda60852a'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'La Pintada',
        'id': '5e206d01c6788b7bda60852b'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'La Unión',
        'id': '5e206d01c6788b7bda60852c'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Liborina',
        'id': '5e206d01c6788b7bda60852d'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Maceo',
        'id': '5e206d02c6788b7bda60852e'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Marinilla',
        'id': '5e206d02c6788b7bda60852f'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Montebello',
        'id': '5e206d02c6788b7bda608530'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Murindó',
        'id': '5e206d02c6788b7bda608531'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Mutatá',
        'id': '5e206d02c6788b7bda608532'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Nariño',
        'id': '5e206d02c6788b7bda608533'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Necoclí',
        'id': '5e206d02c6788b7bda608534'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Nechí',
        'id': '5e206d02c6788b7bda608535'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Olaya',
        'id': '5e206d02c6788b7bda608536'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Peñol',
        'id': '5e206d03c6788b7bda608537'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Peque',
        'id': '5e206d03c6788b7bda608538'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Pueblorrico',
        'id': '5e206d03c6788b7bda608539'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Puerto Berrío',
        'id': '5e206d03c6788b7bda60853a'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Puerto Nare',
        'id': '5e206d03c6788b7bda60853b'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Puerto Triunfo',
        'id': '5e206d03c6788b7bda60853c'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Remedios',
        'id': '5e206d03c6788b7bda60853d'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Retiro',
        'id': '5e206d03c6788b7bda60853e'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Rionegro',
        'id': '5e206d04c6788b7bda60853f'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Sabanalarga',
        'id': '5e206d04c6788b7bda608540'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Sabaneta',
        'id': '5e206d04c6788b7bda608541'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Salgar',
        'id': '5e206d04c6788b7bda608542'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Albán',
        'id': '5e206d04c6788b7bda608543'
    },
    {
        'DEPARTAMENTO': 'Vaupés',
        'MUNICIPIO': 'Yavaraté',
        'id': '5e206d04c6788b7bda608544'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Francisco',
        'id': '5e206d04c6788b7bda608545'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Jerónimo',
        'id': '5e206d04c6788b7bda608546'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Montelíbano',
        'id': '5e206d05c6788b7bda608547'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Puerto Asís',
        'id': '5e206d05c6788b7bda608548'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Luis',
        'id': '5e206d05c6788b7bda608549'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Pedro',
        'id': '5e206d05c6788b7bda60854a'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Corozal',
        'id': '5e206d05c6788b7bda60854b'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Rafael',
        'id': '5e206d05c6788b7bda60854c'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Roque',
        'id': '5e206d05c6788b7bda60854d'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Vicente',
        'id': '5e206d05c6788b7bda60854e'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Santa Bárbara',
        'id': '5e206d06c6788b7bda60854f'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Buesaco',
        'id': '5e206d06c6788b7bda608550'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Santo Domingo',
        'id': '5e206d06c6788b7bda608551'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'El Santuario',
        'id': '5e206d06c6788b7bda608552'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Segovia',
        'id': '5e206d06c6788b7bda608553'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Sopetrán',
        'id': '5e206d06c6788b7bda608554'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Támesis',
        'id': '5e206d06c6788b7bda608555'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Tarazá',
        'id': '5e206d06c6788b7bda608556'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Tarso',
        'id': '5e206d07c6788b7bda608557'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Titiribí',
        'id': '5e206d07c6788b7bda608558'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Toledo',
        'id': '5e206d07c6788b7bda608559'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Turbo',
        'id': '5e206d07c6788b7bda60855a'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Uramita',
        'id': '5e206d07c6788b7bda60855b'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Urrao',
        'id': '5e206d07c6788b7bda60855c'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Valdivia',
        'id': '5e206d07c6788b7bda60855d'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Valparaíso',
        'id': '5e206d07c6788b7bda60855e'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Vegachí',
        'id': '5e206d08c6788b7bda60855f'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Venecia',
        'id': '5e206d08c6788b7bda608560'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Maní',
        'id': '5e206d08c6788b7bda608561'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Yalí',
        'id': '5e206d08c6788b7bda608562'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Yarumal',
        'id': '5e206d08c6788b7bda608563'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Yolombó',
        'id': '5e206d08c6788b7bda608564'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Yondó',
        'id': '5e206d08c6788b7bda608565'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Zaragoza',
        'id': '5e206d08c6788b7bda608566'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Barranquilla',
        'id': '5e206d08c6788b7bda608567'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Baranoa',
        'id': '5e206d09c6788b7bda608568'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'El Peñón',
        'id': '5e206d09c6788b7bda608569'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Candelaria',
        'id': '5e206d09c6788b7bda60856a'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Galapa',
        'id': '5e206d09c6788b7bda60856b'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Tuluá',
        'id': '5e206d09c6788b7bda60856c'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Casabianca',
        'id': '5e206d09c6788b7bda60856d'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Luruaco',
        'id': '5e206d09c6788b7bda60856e'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Malambo',
        'id': '5e206d09c6788b7bda60856f'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Manatí',
        'id': '5e206d0ac6788b7bda608570'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Anolaima',
        'id': '5e206d0ac6788b7bda608571'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Piojó',
        'id': '5e206d0ac6788b7bda608572'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Polonuevo',
        'id': '5e206d0ac6788b7bda608573'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Chía',
        'id': '5e206d0ac6788b7bda608574'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'San Andrés de Tumaco',
        'id': '5e206d0ac6788b7bda608575'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Sabanagrande',
        'id': '5e206d0ac6788b7bda608576'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Sabanalarga',
        'id': '5e206d0ac6788b7bda608577'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Santa Lucía',
        'id': '5e206d0bc6788b7bda608578'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Santo Tomás',
        'id': '5e206d0bc6788b7bda608579'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Soledad',
        'id': '5e206d0bc6788b7bda60857a'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Suan',
        'id': '5e206d0bc6788b7bda60857b'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Tubará',
        'id': '5e206d0bc6788b7bda60857c'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Usiacurí',
        'id': '5e206d0bc6788b7bda60857d'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Milán',
        'id': '5e206d0bc6788b7bda60857e'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Capitanejo',
        'id': '5e206d0bc6788b7bda60857f'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Achí',
        'id': '5e206d0cc6788b7bda608580'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Anzoátegui',
        'id': '5e206d0cc6788b7bda608581'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Arenal',
        'id': '5e206d0cc6788b7bda608582'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Arjona',
        'id': '5e206d0cc6788b7bda608583'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Arroyohondo',
        'id': '5e206d0cc6788b7bda608584'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Florida',
        'id': '5e206d0cc6788b7bda608585'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Calamar',
        'id': '5e206d0cc6788b7bda608586'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Cantagallo',
        'id': '5e206d0cc6788b7bda608587'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Cicuco',
        'id': '5e206d0cc6788b7bda608588'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Córdoba',
        'id': '5e206d0dc6788b7bda608589'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Clemencia',
        'id': '5e206d0dc6788b7bda60858a'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Repelón',
        'id': '5e206d0dc6788b7bda60858b'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'El Guamo',
        'id': '5e206d0dc6788b7bda60858c'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Frontino',
        'id': '5e206d0dc6788b7bda60858d'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Magangué',
        'id': '5e206d0dc6788b7bda60858e'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Mahates',
        'id': '5e206d0dc6788b7bda60858f'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Margarita',
        'id': '5e206d0dc6788b7bda608590'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Montecristo',
        'id': '5e206d0ec6788b7bda608591'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Mompós',
        'id': '5e206d0ec6788b7bda608592'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Morales',
        'id': '5e206d0ec6788b7bda608593'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Norosí',
        'id': '5e206d0ec6788b7bda608594'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Pinillos',
        'id': '5e206d0ec6788b7bda608595'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Regidor',
        'id': '5e206d0ec6788b7bda608596'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Río Viejo',
        'id': '5e206d0ec6788b7bda608597'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Estanislao',
        'id': '5e206d0ec6788b7bda608598'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Fernando',
        'id': '5e206d0fc6788b7bda608599'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'El Peñón',
        'id': '5e206d0fc6788b7bda60859a'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Pamplonita',
        'id': '5e206d0fc6788b7bda60859b'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Juan Nepomuceno',
        'id': '5e206d0fc6788b7bda60859c'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'Miriti Paraná',
        'id': '5e206d0fc6788b7bda60859d'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Támara',
        'id': '5e206d0fc6788b7bda60859e'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Santa Catalina',
        'id': '5e206d0fc6788b7bda60859f'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Santa Rosa',
        'id': '5e206d0fc6788b7bda6085a0'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tibasosa',
        'id': '5e206d10c6788b7bda6085a1'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Simití',
        'id': '5e206d10c6788b7bda6085a2'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Soplaviento',
        'id': '5e206d10c6788b7bda6085a3'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Talaigua Nuevo',
        'id': '5e206d10c6788b7bda6085a4'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Tiquisio',
        'id': '5e206d10c6788b7bda6085a5'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Turbaco',
        'id': '5e206d10c6788b7bda6085a6'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Turbaná',
        'id': '5e206d10c6788b7bda6085a7'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Villanueva',
        'id': '5e206d10c6788b7bda6085a8'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Páez',
        'id': '5e206d10c6788b7bda6085a9'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tunja',
        'id': '5e206d11c6788b7bda6085aa'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Almeida',
        'id': '5e206d11c6788b7bda6085ab'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Aquitania',
        'id': '5e206d11c6788b7bda6085ac'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Arcabuco',
        'id': '5e206d11c6788b7bda6085ad'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Ibagué',
        'id': '5e206d11c6788b7bda6085ae'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Berbeo',
        'id': '5e206d11c6788b7bda6085af'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Betéitiva',
        'id': '5e206d11c6788b7bda6085b0'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Boavita',
        'id': '5e206d11c6788b7bda6085b1'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Boyacá',
        'id': '5e206d12c6788b7bda6085b2'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Briceño',
        'id': '5e206d12c6788b7bda6085b3'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Buena Vista',
        'id': '5e206d12c6788b7bda6085b4'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Busbanzá',
        'id': '5e206d12c6788b7bda6085b5'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Caldas',
        'id': '5e206d12c6788b7bda6085b6'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Campohermoso',
        'id': '5e206d12c6788b7bda6085b7'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Cerinza',
        'id': '5e206d12c6788b7bda6085b8'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chinavita',
        'id': '5e206d12c6788b7bda6085b9'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chiquinquirá',
        'id': '5e206d13c6788b7bda6085ba'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chiscas',
        'id': '5e206d13c6788b7bda6085bb'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chita',
        'id': '5e206d13c6788b7bda6085bc'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chitaraque',
        'id': '5e206d13c6788b7bda6085bd'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chivatá',
        'id': '5e206d13c6788b7bda6085be'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Cómbita',
        'id': '5e206d13c6788b7bda6085bf'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Coper',
        'id': '5e206d13c6788b7bda6085c0'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Corrales',
        'id': '5e206d13c6788b7bda6085c1'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Covarachía',
        'id': '5e206d13c6788b7bda6085c2'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Cubará',
        'id': '5e206d14c6788b7bda6085c3'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Cucaita',
        'id': '5e206d14c6788b7bda6085c4'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Cuítiva',
        'id': '5e206d14c6788b7bda6085c5'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chíquiza',
        'id': '5e206d14c6788b7bda6085c6'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Chivor',
        'id': '5e206d14c6788b7bda6085c7'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Duitama',
        'id': '5e206d14c6788b7bda6085c8'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'El Cocuy',
        'id': '5e206d14c6788b7bda6085c9'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'El Espino',
        'id': '5e206d14c6788b7bda6085ca'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Firavitoba',
        'id': '5e206d15c6788b7bda6085cb'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Floresta',
        'id': '5e206d15c6788b7bda6085cc'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Gachantivá',
        'id': '5e206d15c6788b7bda6085cd'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Gameza',
        'id': '5e206d15c6788b7bda6085ce'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Garagoa',
        'id': '5e206d15c6788b7bda6085cf'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Guacamayas',
        'id': '5e206d15c6788b7bda6085d0'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Guateque',
        'id': '5e206d15c6788b7bda6085d1'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Guayatá',
        'id': '5e206d15c6788b7bda6085d2'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Güicán',
        'id': '5e206d16c6788b7bda6085d3'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Iza',
        'id': '5e206d16c6788b7bda6085d4'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Jenesano',
        'id': '5e206d16c6788b7bda6085d5'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Jericó',
        'id': '5e206d16c6788b7bda6085d6'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Labranzagrande',
        'id': '5e206d16c6788b7bda6085d7'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'La Capilla',
        'id': '5e206d16c6788b7bda6085d8'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'La Victoria',
        'id': '5e206d16c6788b7bda6085d9'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Puerto Colombia',
        'id': '5e206d16c6788b7bda6085da'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Belén',
        'id': '5e206d17c6788b7bda6085db'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Macanal',
        'id': '5e206d17c6788b7bda6085dc'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Maripí',
        'id': '5e206d17c6788b7bda6085dd'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Miraflores',
        'id': '5e206d17c6788b7bda6085de'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Mongua',
        'id': '5e206d17c6788b7bda6085df'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Monguí',
        'id': '5e206d17c6788b7bda6085e0'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Moniquirá',
        'id': '5e206d17c6788b7bda6085e1'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Muzo',
        'id': '5e206d17c6788b7bda6085e2'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Nobsa',
        'id': '5e206d17c6788b7bda6085e3'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Nuevo Colón',
        'id': '5e206d18c6788b7bda6085e4'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Oicatá',
        'id': '5e206d18c6788b7bda6085e5'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Otanche',
        'id': '5e206d18c6788b7bda6085e6'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Pachavita',
        'id': '5e206d18c6788b7bda6085e7'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Páez',
        'id': '5e206d18c6788b7bda6085e8'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Paipa',
        'id': '5e206d18c6788b7bda6085e9'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Pajarito',
        'id': '5e206d18c6788b7bda6085ea'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Panqueba',
        'id': '5e206d18c6788b7bda6085eb'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Pauna',
        'id': '5e206d19c6788b7bda6085ec'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Paya',
        'id': '5e206d19c6788b7bda6085ed'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Sopó',
        'id': '5e206d19c6788b7bda6085ee'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Pesca',
        'id': '5e206d19c6788b7bda6085ef'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Pisba',
        'id': '5e206d19c6788b7bda6085f0'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Puerto Boyacá',
        'id': '5e206d19c6788b7bda6085f1'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Quípama',
        'id': '5e206d19c6788b7bda6085f2'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Ramiriquí',
        'id': '5e206d19c6788b7bda6085f3'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Ráquira',
        'id': '5e206d19c6788b7bda6085f4'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Rondón',
        'id': '5e206d1ac6788b7bda6085f5'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Saboyá',
        'id': '5e206d1ac6788b7bda6085f6'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sáchica',
        'id': '5e206d1ac6788b7bda6085f7'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Samacá',
        'id': '5e206d1ac6788b7bda6085f8'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'San Eduardo',
        'id': '5e206d1ac6788b7bda6085f9'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Carmen del Darien',
        'id': '5e206d1ac6788b7bda6085fa'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Gama',
        'id': '5e206d1ac6788b7bda6085fb'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'San Mateo',
        'id': '5e206d1ac6788b7bda6085fc'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Sasaima',
        'id': '5e206d1bc6788b7bda6085fd'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Chachagüí',
        'id': '5e206d1bc6788b7bda6085fe'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Santana',
        'id': '5e206d1bc6788b7bda6085ff'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Santa María',
        'id': '5e206d1bc6788b7bda608600'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Cúcuta',
        'id': '5e206d1bc6788b7bda608601'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Santa Sofía',
        'id': '5e206d1bc6788b7bda608602'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sativanorte',
        'id': '5e206d1bc6788b7bda608603'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sativasur',
        'id': '5e206d1bc6788b7bda608604'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Siachoque',
        'id': '5e206d1cc6788b7bda608605'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Soatá',
        'id': '5e206d1cc6788b7bda608606'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Socotá',
        'id': '5e206d1cc6788b7bda608607'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Socha',
        'id': '5e206d1cc6788b7bda608608'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sogamoso',
        'id': '5e206d1cc6788b7bda608609'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Somondoco',
        'id': '5e206d1cc6788b7bda60860a'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sora',
        'id': '5e206d1cc6788b7bda60860b'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sotaquirá',
        'id': '5e206d1cc6788b7bda60860c'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Soracá',
        'id': '5e206d1cc6788b7bda60860d'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Susacón',
        'id': '5e206d1dc6788b7bda60860e'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sutamarchán',
        'id': '5e206d1dc6788b7bda60860f'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Sutatenza',
        'id': '5e206d1dc6788b7bda608610'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tasco',
        'id': '5e206d1dc6788b7bda608611'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tenza',
        'id': '5e206d1dc6788b7bda608612'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tibaná',
        'id': '5e206d1dc6788b7bda608613'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tinjacá',
        'id': '5e206d1dc6788b7bda608614'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tipacoque',
        'id': '5e206d1dc6788b7bda608615'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Toca',
        'id': '5e206d1ec6788b7bda608616'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Cartagena',
        'id': '5e206d1ec6788b7bda608617'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tópaga',
        'id': '5e206d1ec6788b7bda608618'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tota',
        'id': '5e206d1ec6788b7bda608619'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Turmequé',
        'id': '5e206d1ec6788b7bda60861a'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Granada',
        'id': '5e206d1ec6788b7bda60861b'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tutazá',
        'id': '5e206d1ec6788b7bda60861c'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Umbita',
        'id': '5e206d1ec6788b7bda60861d'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Ventaquemada',
        'id': '5e206d1ec6788b7bda60861e'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Viracachá',
        'id': '5e206d1fc6788b7bda60861f'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Zetaquira',
        'id': '5e206d1fc6788b7bda608620'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Manizales',
        'id': '5e206d1fc6788b7bda608621'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Aguadas',
        'id': '5e206d1fc6788b7bda608622'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Anserma',
        'id': '5e206d1fc6788b7bda608623'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Aranzazu',
        'id': '5e206d1fc6788b7bda608624'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Belalcázar',
        'id': '5e206d1fc6788b7bda608625'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Chinchiná',
        'id': '5e206d1fc6788b7bda608626'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Filadelfia',
        'id': '5e206d20c6788b7bda608627'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'La Dorada',
        'id': '5e206d20c6788b7bda608628'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'La Merced',
        'id': '5e206d20c6788b7bda608629'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Manzanares',
        'id': '5e206d20c6788b7bda60862a'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Marmato',
        'id': '5e206d20c6788b7bda60862b'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Marulanda',
        'id': '5e206d20c6788b7bda60862c'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Neira',
        'id': '5e206d20c6788b7bda60862d'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Norcasia',
        'id': '5e206d20c6788b7bda60862e'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Pácora',
        'id': '5e206d21c6788b7bda60862f'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Palestina',
        'id': '5e206d21c6788b7bda608630'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Pensilvania',
        'id': '5e206d21c6788b7bda608631'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Riosucio',
        'id': '5e206d21c6788b7bda608632'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Risaralda',
        'id': '5e206d21c6788b7bda608633'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Salamina',
        'id': '5e206d21c6788b7bda608634'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Samaná',
        'id': '5e206d21c6788b7bda608635'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'San José',
        'id': '5e206d21c6788b7bda608636'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Supía',
        'id': '5e206d21c6788b7bda608637'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Victoria',
        'id': '5e206d22c6788b7bda608638'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Villamaría',
        'id': '5e206d22c6788b7bda608639'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Viterbo',
        'id': '5e206d22c6788b7bda60863a'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Florencia',
        'id': '5e206d22c6788b7bda60863b'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Albania',
        'id': '5e206d22c6788b7bda60863c'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Santa Bárbara de Pinto',
        'id': '5e206d22c6788b7bda60863d'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'María la Baja',
        'id': '5e206d22c6788b7bda60863e'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Curillo',
        'id': '5e206d22c6788b7bda60863f'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'El Doncello',
        'id': '5e206d23c6788b7bda608640'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'El Paujil',
        'id': '5e206d23c6788b7bda608641'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Morelia',
        'id': '5e206d23c6788b7bda608642'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Puerto Rico',
        'id': '5e206d23c6788b7bda608643'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'La Montañita',
        'id': '5e206d23c6788b7bda608644'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'San Vicente del Caguán',
        'id': '5e206d23c6788b7bda608645'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Solano',
        'id': '5e206d23c6788b7bda608646'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Solita',
        'id': '5e206d23c6788b7bda608647'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Valparaíso',
        'id': '5e206d24c6788b7bda608648'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Popayán',
        'id': '5e206d24c6788b7bda608649'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Almaguer',
        'id': '5e206d24c6788b7bda60864a'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Argelia',
        'id': '5e206d24c6788b7bda60864b'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Balboa',
        'id': '5e206d24c6788b7bda60864c'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Bolívar',
        'id': '5e206d24c6788b7bda60864d'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Buenos Aires',
        'id': '5e206d24c6788b7bda60864e'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Cajibío',
        'id': '5e206d24c6788b7bda60864f'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Caldono',
        'id': '5e206d24c6788b7bda608650'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Caloto',
        'id': '5e206d25c6788b7bda608651'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Corinto',
        'id': '5e206d25c6788b7bda608652'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'El Tambo',
        'id': '5e206d25c6788b7bda608653'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Florencia',
        'id': '5e206d25c6788b7bda608654'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Guachené',
        'id': '5e206d25c6788b7bda608655'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Guapi',
        'id': '5e206d25c6788b7bda608656'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Inzá',
        'id': '5e206d25c6788b7bda608657'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Jambaló',
        'id': '5e206d25c6788b7bda608658'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'La Sierra',
        'id': '5e206d26c6788b7bda608659'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'La Vega',
        'id': '5e206d26c6788b7bda60865a'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'López',
        'id': '5e206d26c6788b7bda60865b'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Mercaderes',
        'id': '5e206d26c6788b7bda60865c'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Miranda',
        'id': '5e206d26c6788b7bda60865d'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Morales',
        'id': '5e206d26c6788b7bda60865e'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Padilla',
        'id': '5e206d26c6788b7bda60865f'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Patía',
        'id': '5e206d26c6788b7bda608660'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Piamonte',
        'id': '5e206d26c6788b7bda608661'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Piendamó',
        'id': '5e206d27c6788b7bda608662'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Puerto Tejada',
        'id': '5e206d27c6788b7bda608663'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Puracé',
        'id': '5e206d27c6788b7bda608664'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Rosas',
        'id': '5e206d27c6788b7bda608665'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'El Peñón',
        'id': '5e206d27c6788b7bda608666'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Jardín',
        'id': '5e206d27c6788b7bda608667'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Santa Rosa',
        'id': '5e206d27c6788b7bda608668'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Silvia',
        'id': '5e206d27c6788b7bda608669'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Sotara',
        'id': '5e206d28c6788b7bda60866a'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Suárez',
        'id': '5e206d28c6788b7bda60866b'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Sucre',
        'id': '5e206d28c6788b7bda60866c'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Timbío',
        'id': '5e206d28c6788b7bda60866d'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Timbiquí',
        'id': '5e206d28c6788b7bda60866e'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Toribio',
        'id': '5e206d28c6788b7bda60866f'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Totoró',
        'id': '5e206d28c6788b7bda608670'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Villa Rica',
        'id': '5e206d28c6788b7bda608671'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Valledupar',
        'id': '5e206d28c6788b7bda608672'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Aguachica',
        'id': '5e206d29c6788b7bda608673'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Agustín Codazzi',
        'id': '5e206d29c6788b7bda608674'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Astrea',
        'id': '5e206d29c6788b7bda608675'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Becerril',
        'id': '5e206d29c6788b7bda608676'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Bosconia',
        'id': '5e206d29c6788b7bda608677'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Chimichagua',
        'id': '5e206d29c6788b7bda608678'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Chiriguaná',
        'id': '5e206d29c6788b7bda608679'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Curumaní',
        'id': '5e206d29c6788b7bda60867a'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'El Copey',
        'id': '5e206d2ac6788b7bda60867b'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'El Paso',
        'id': '5e206d2ac6788b7bda60867c'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Gamarra',
        'id': '5e206d2ac6788b7bda60867d'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'González',
        'id': '5e206d2ac6788b7bda60867e'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'La Gloria',
        'id': '5e206d2ac6788b7bda60867f'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Jamundí',
        'id': '5e206d2ac6788b7bda608680'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Manaure',
        'id': '5e206d2ac6788b7bda608681'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Pailitas',
        'id': '5e206d2ac6788b7bda608682'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Pelaya',
        'id': '5e206d2bc6788b7bda608683'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Pueblo Bello',
        'id': '5e206d2bc6788b7bda608684'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Tadó',
        'id': '5e206d2bc6788b7bda608685'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'La Paz',
        'id': '5e206d2bc6788b7bda608686'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'San Alberto',
        'id': '5e206d2bc6788b7bda608687'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'San Diego',
        'id': '5e206d2bc6788b7bda608688'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'San Martín',
        'id': '5e206d2bc6788b7bda608689'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Tamalameque',
        'id': '5e206d2bc6788b7bda60868a'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Montería',
        'id': '5e206d2bc6788b7bda60868b'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Ayapel',
        'id': '5e206d2cc6788b7bda60868c'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Buenavista',
        'id': '5e206d2cc6788b7bda60868d'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Canalete',
        'id': '5e206d2cc6788b7bda60868e'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Cereté',
        'id': '5e206d2cc6788b7bda60868f'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Chimá',
        'id': '5e206d2cc6788b7bda608690'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Chinú',
        'id': '5e206d2cc6788b7bda608691'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Orocué',
        'id': '5e206d2cc6788b7bda608692'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Cotorra',
        'id': '5e206d2cc6788b7bda608693'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Líbano',
        'id': '5e206d2dc6788b7bda608694'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Lorica',
        'id': '5e206d2dc6788b7bda608695'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Los Córdobas',
        'id': '5e206d2dc6788b7bda608696'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Momil',
        'id': '5e206d2dc6788b7bda608697'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Moñitos',
        'id': '5e206d2dc6788b7bda608698'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Planeta Rica',
        'id': '5e206d2dc6788b7bda608699'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Pueblo Nuevo',
        'id': '5e206d2dc6788b7bda60869a'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Puerto Escondido',
        'id': '5e206d2dc6788b7bda60869b'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Yacopí',
        'id': '5e206d2ec6788b7bda60869c'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Purísima',
        'id': '5e206d2ec6788b7bda60869d'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Sahagún',
        'id': '5e206d2ec6788b7bda60869e'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'San Andrés Sotavento',
        'id': '5e206d2ec6788b7bda60869f'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'San Antero',
        'id': '5e206d2ec6788b7bda6086a0'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Calarcá',
        'id': '5e206d2ec6788b7bda6086a1'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Sonsón',
        'id': '5e206d2ec6788b7bda6086a2'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'El Carmen',
        'id': '5e206d2ec6788b7bda6086a3'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'San Pelayo',
        'id': '5e206d2ec6788b7bda6086a4'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Tierralta',
        'id': '5e206d2fc6788b7bda6086a5'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Tuchín',
        'id': '5e206d2fc6788b7bda6086a6'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Valencia',
        'id': '5e206d2fc6788b7bda6086a7'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Lérida',
        'id': '5e206d2fc6788b7bda6086a8'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Anapoima',
        'id': '5e206d2fc6788b7bda6086a9'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Arbeláez',
        'id': '5e206d2fc6788b7bda6086aa'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Beltrán',
        'id': '5e206d2fc6788b7bda6086ab'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Bituima',
        'id': '5e206d2fc6788b7bda6086ac'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Bojacá',
        'id': '5e206d30c6788b7bda6086ad'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Cabrera',
        'id': '5e206d30c6788b7bda6086ae'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Cachipay',
        'id': '5e206d30c6788b7bda6086af'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Cajicá',
        'id': '5e206d30c6788b7bda6086b0'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Caparrapí',
        'id': '5e206d30c6788b7bda6086b1'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Caqueza',
        'id': '5e206d30c6788b7bda6086b2'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'La Apartada',
        'id': '5e206d30c6788b7bda6086b3'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Chaguaní',
        'id': '5e206d30c6788b7bda6086b4'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Chipaque',
        'id': '5e206d31c6788b7bda6086b5'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Choachí',
        'id': '5e206d31c6788b7bda6086b6'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Chocontá',
        'id': '5e206d31c6788b7bda6086b7'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Cogua',
        'id': '5e206d31c6788b7bda6086b8'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Cota',
        'id': '5e206d31c6788b7bda6086b9'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Cucunubá',
        'id': '5e206d31c6788b7bda6086ba'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'El Colegio',
        'id': '5e206d31c6788b7bda6086bb'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'El Rosal',
        'id': '5e206d31c6788b7bda6086bc'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Fomeque',
        'id': '5e206d31c6788b7bda6086bd'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Fosca',
        'id': '5e206d32c6788b7bda6086be'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Funza',
        'id': '5e206d32c6788b7bda6086bf'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Fúquene',
        'id': '5e206d32c6788b7bda6086c0'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Gachala',
        'id': '5e206d32c6788b7bda6086c1'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Gachancipá',
        'id': '5e206d32c6788b7bda6086c2'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Gachetá',
        'id': '5e206d32c6788b7bda6086c3'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Cristóbal',
        'id': '5e206d32c6788b7bda6086c4'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Girardot',
        'id': '5e206d32c6788b7bda6086c5'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Granada',
        'id': '5e206d33c6788b7bda6086c6'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Guachetá',
        'id': '5e206d33c6788b7bda6086c7'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Guaduas',
        'id': '5e206d33c6788b7bda6086c8'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Guasca',
        'id': '5e206d33c6788b7bda6086c9'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Guataquí',
        'id': '5e206d33c6788b7bda6086ca'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Guatavita',
        'id': '5e206d33c6788b7bda6086cb'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Fusagasugá',
        'id': '5e206d33c6788b7bda6086cc'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Guayabetal',
        'id': '5e206d33c6788b7bda6086cd'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Gutiérrez',
        'id': '5e206d33c6788b7bda6086ce'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Jerusalén',
        'id': '5e206d34c6788b7bda6086cf'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Junín',
        'id': '5e206d34c6788b7bda6086d0'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'La Calera',
        'id': '5e206d34c6788b7bda6086d1'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'La Mesa',
        'id': '5e206d34c6788b7bda6086d2'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'La Palma',
        'id': '5e206d34c6788b7bda6086d3'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'La Peña',
        'id': '5e206d34c6788b7bda6086d4'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'La Vega',
        'id': '5e206d34c6788b7bda6086d5'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Lenguazaque',
        'id': '5e206d34c6788b7bda6086d6'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Macheta',
        'id': '5e206d35c6788b7bda6086d7'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Madrid',
        'id': '5e206d35c6788b7bda6086d8'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Manta',
        'id': '5e206d35c6788b7bda6086d9'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Medina',
        'id': '5e206d35c6788b7bda6086da'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Mosquera',
        'id': '5e206d35c6788b7bda6086db'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Nariño',
        'id': '5e206d35c6788b7bda6086dc'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Nemocón',
        'id': '5e206d35c6788b7bda6086dd'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Nilo',
        'id': '5e206d35c6788b7bda6086de'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Nimaima',
        'id': '5e206d35c6788b7bda6086df'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Nocaima',
        'id': '5e206d36c6788b7bda6086e0'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Venecia',
        'id': '5e206d36c6788b7bda6086e1'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Pacho',
        'id': '5e206d36c6788b7bda6086e2'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Paime',
        'id': '5e206d36c6788b7bda6086e3'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Pandi',
        'id': '5e206d36c6788b7bda6086e4'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Paratebueno',
        'id': '5e206d36c6788b7bda6086e5'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Pasca',
        'id': '5e206d36c6788b7bda6086e6'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Puerto Salgar',
        'id': '5e206d36c6788b7bda6086e7'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Pulí',
        'id': '5e206d37c6788b7bda6086e8'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Quebradanegra',
        'id': '5e206d37c6788b7bda6086e9'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Quetame',
        'id': '5e206d37c6788b7bda6086ea'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Quipile',
        'id': '5e206d37c6788b7bda6086eb'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Apulo',
        'id': '5e206d37c6788b7bda6086ec'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Ricaurte',
        'id': '5e206d37c6788b7bda6086ed'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Zambrano',
        'id': '5e206d37c6788b7bda6086ee'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'San Bernardo',
        'id': '5e206d37c6788b7bda6086ef'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'San Cayetano',
        'id': '5e206d38c6788b7bda6086f0'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'San Francisco',
        'id': '5e206d38c6788b7bda6086f1'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'La Uvita',
        'id': '5e206d38c6788b7bda6086f2'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Zipaquirá',
        'id': '5e206d38c6788b7bda6086f3'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Sesquilé',
        'id': '5e206d38c6788b7bda6086f4'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Sibaté',
        'id': '5e206d38c6788b7bda6086f5'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Silvania',
        'id': '5e206d38c6788b7bda6086f6'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Simijaca',
        'id': '5e206d38c6788b7bda6086f7'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Soacha',
        'id': '5e206d38c6788b7bda6086f8'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Subachoque',
        'id': '5e206d39c6788b7bda6086f9'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Suesca',
        'id': '5e206d39c6788b7bda6086fa'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Supatá',
        'id': '5e206d39c6788b7bda6086fb'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Susa',
        'id': '5e206d39c6788b7bda6086fc'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Sutatausa',
        'id': '5e206d39c6788b7bda6086fd'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tabio',
        'id': '5e206d39c6788b7bda6086fe'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Génova',
        'id': '5e206d39c6788b7bda6086ff'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tausa',
        'id': '5e206d39c6788b7bda608700'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tena',
        'id': '5e206d3ac6788b7bda608701'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tenjo',
        'id': '5e206d3ac6788b7bda608702'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tibacuy',
        'id': '5e206d3ac6788b7bda608703'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tibirita',
        'id': '5e206d3ac6788b7bda608704'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tocaima',
        'id': '5e206d3ac6788b7bda608705'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Tocancipá',
        'id': '5e206d3ac6788b7bda608706'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Topaipí',
        'id': '5e206d3ac6788b7bda608707'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Ubalá',
        'id': '5e206d3ac6788b7bda608708'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Ubaque',
        'id': '5e206d3ac6788b7bda608709'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Suárez',
        'id': '5e206d3bc6788b7bda60870a'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Une',
        'id': '5e206d3bc6788b7bda60870b'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Útica',
        'id': '5e206d3bc6788b7bda60870c'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Castilla la Nueva',
        'id': '5e206d3bc6788b7bda60870d'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Vianí',
        'id': '5e206d3bc6788b7bda60870e'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Villagómez',
        'id': '5e206d3bc6788b7bda60870f'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Villapinzón',
        'id': '5e206d3bc6788b7bda608710'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Villeta',
        'id': '5e206d3bc6788b7bda608711'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Viotá',
        'id': '5e206d3cc6788b7bda608712'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Zipacón',
        'id': '5e206d3cc6788b7bda608713'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Quibdó',
        'id': '5e206d3cc6788b7bda608714'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Acandí',
        'id': '5e206d3cc6788b7bda608715'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Alto Baudo',
        'id': '5e206d3cc6788b7bda608716'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Atrato',
        'id': '5e206d3cc6788b7bda608717'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Bagadó',
        'id': '5e206d3cc6788b7bda608718'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Bahía Solano',
        'id': '5e206d3cc6788b7bda608719'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Bajo Baudó',
        'id': '5e206d3dc6788b7bda60871a'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Belén',
        'id': '5e206d3dc6788b7bda60871b'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Bojaya',
        'id': '5e206d3dc6788b7bda60871c'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Unión Panamericana',
        'id': '5e206d3dc6788b7bda60871d'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Pueblo Viejo',
        'id': '5e206d3dc6788b7bda60871e'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Cértegui',
        'id': '5e206d3dc6788b7bda60871f'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Condoto',
        'id': '5e206d3dc6788b7bda608720'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Villagarzón',
        'id': '5e206d3dc6788b7bda608721'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Facatativá',
        'id': '5e206d3dc6788b7bda608722'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Juradó',
        'id': '5e206d3ec6788b7bda608723'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Lloró',
        'id': '5e206d3ec6788b7bda608724'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Medio Atrato',
        'id': '5e206d3ec6788b7bda608725'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Medio Baudó',
        'id': '5e206d3ec6788b7bda608726'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Medio San Juan',
        'id': '5e206d3ec6788b7bda608727'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Nóvita',
        'id': '5e206d3ec6788b7bda608728'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Nuquí',
        'id': '5e206d3ec6788b7bda608729'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Río Iro',
        'id': '5e206d3ec6788b7bda60872a'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Río Quito',
        'id': '5e206d3fc6788b7bda60872b'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Riosucio',
        'id': '5e206d3fc6788b7bda60872c'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Puerto Libertador',
        'id': '5e206d3fc6788b7bda60872d'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Sipí',
        'id': '5e206d3fc6788b7bda60872e'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Unguía',
        'id': '5e206d3fc6788b7bda60872f'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Neiva',
        'id': '5e206d3fc6788b7bda608730'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Acevedo',
        'id': '5e206d3fc6788b7bda608731'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Agrado',
        'id': '5e206d3fc6788b7bda608732'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Aipe',
        'id': '5e206d3fc6788b7bda608733'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Algeciras',
        'id': '5e206d40c6788b7bda608734'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Altamira',
        'id': '5e206d40c6788b7bda608735'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Baraya',
        'id': '5e206d40c6788b7bda608736'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Campoalegre',
        'id': '5e206d40c6788b7bda608737'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Colombia',
        'id': '5e206d40c6788b7bda608738'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Elías',
        'id': '5e206d40c6788b7bda608739'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Garzón',
        'id': '5e206d40c6788b7bda60873a'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Gigante',
        'id': '5e206d40c6788b7bda60873b'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Guadalupe',
        'id': '5e206d41c6788b7bda60873c'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Hobo',
        'id': '5e206d41c6788b7bda60873d'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Iquira',
        'id': '5e206d41c6788b7bda60873e'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Isnos',
        'id': '5e206d41c6788b7bda60873f'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'La Argentina',
        'id': '5e206d41c6788b7bda608740'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'La Plata',
        'id': '5e206d41c6788b7bda608741'
    },
    {
        'DEPARTAMENTO': 'Caldas',
        'MUNICIPIO': 'Marquetalia',
        'id': '5e206d41c6788b7bda608742'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Nátaga',
        'id': '5e206d41c6788b7bda608743'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Oporapa',
        'id': '5e206d42c6788b7bda608744'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Paicol',
        'id': '5e206d42c6788b7bda608745'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Palermo',
        'id': '5e206d42c6788b7bda608746'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Palestina',
        'id': '5e206d42c6788b7bda608747'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Pital',
        'id': '5e206d42c6788b7bda608748'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Pitalito',
        'id': '5e206d42c6788b7bda608749'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Rivera',
        'id': '5e206d42c6788b7bda60874a'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Saladoblanco',
        'id': '5e206d42c6788b7bda60874b'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Arboleda',
        'id': '5e206d43c6788b7bda60874c'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Santa María',
        'id': '5e206d43c6788b7bda60874d'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Suaza',
        'id': '5e206d43c6788b7bda60874e'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Tarqui',
        'id': '5e206d43c6788b7bda60874f'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Tesalia',
        'id': '5e206d43c6788b7bda608750'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Tello',
        'id': '5e206d43c6788b7bda608751'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Teruel',
        'id': '5e206d43c6788b7bda608752'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Timaná',
        'id': '5e206d43c6788b7bda608753'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Villavieja',
        'id': '5e206d43c6788b7bda608754'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'Yaguará',
        'id': '5e206d44c6788b7bda608755'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Riohacha',
        'id': '5e206d44c6788b7bda608756'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Albania',
        'id': '5e206d44c6788b7bda608757'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Barrancas',
        'id': '5e206d44c6788b7bda608758'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Dibula',
        'id': '5e206d44c6788b7bda608759'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Distracción',
        'id': '5e206d44c6788b7bda60875a'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'El Molino',
        'id': '5e206d44c6788b7bda60875b'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Fonseca',
        'id': '5e206d44c6788b7bda60875c'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Hatonuevo',
        'id': '5e206d45c6788b7bda60875d'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Maicao',
        'id': '5e206d45c6788b7bda60875e'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Manaure',
        'id': '5e206d45c6788b7bda60875f'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Uribia',
        'id': '5e206d45c6788b7bda608760'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Urumita',
        'id': '5e206d45c6788b7bda608761'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'Villanueva',
        'id': '5e206d45c6788b7bda608762'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Santa Marta',
        'id': '5e206d45c6788b7bda608763'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Algarrobo',
        'id': '5e206d45c6788b7bda608764'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Aracataca',
        'id': '5e206d45c6788b7bda608765'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Ariguaní',
        'id': '5e206d46c6788b7bda608766'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Cerro San Antonio',
        'id': '5e206d46c6788b7bda608767'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Chivolo',
        'id': '5e206d46c6788b7bda608768'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Concordia',
        'id': '5e206d46c6788b7bda608769'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'El Banco',
        'id': '5e206d46c6788b7bda60876a'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'El Piñon',
        'id': '5e206d46c6788b7bda60876b'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'El Retén',
        'id': '5e206d46c6788b7bda60876c'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Fundación',
        'id': '5e206d46c6788b7bda60876d'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Guamal',
        'id': '5e206d47c6788b7bda60876e'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Nueva Granada',
        'id': '5e206d47c6788b7bda60876f'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Pedraza',
        'id': '5e206d47c6788b7bda608770'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Pivijay',
        'id': '5e206d47c6788b7bda608771'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Plato',
        'id': '5e206d47c6788b7bda608772'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Remolino',
        'id': '5e206d47c6788b7bda608773'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Salamina',
        'id': '5e206d47c6788b7bda608774'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'San Zenón',
        'id': '5e206d47c6788b7bda608775'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Santa Ana',
        'id': '5e206d47c6788b7bda608776'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Sitionuevo',
        'id': '5e206d48c6788b7bda608777'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Tenerife',
        'id': '5e206d48c6788b7bda608778'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Zapayán',
        'id': '5e206d48c6788b7bda608779'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Zona Bananera',
        'id': '5e206d48c6788b7bda60877a'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Villavicencio',
        'id': '5e206d48c6788b7bda60877b'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Acacias',
        'id': '5e206d48c6788b7bda60877c'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Cabuyaro',
        'id': '5e206d48c6788b7bda60877d'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Cubarral',
        'id': '5e206d48c6788b7bda60877e'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Cumaral',
        'id': '5e206d49c6788b7bda60877f'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'El Calvario',
        'id': '5e206d49c6788b7bda608780'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'El Castillo',
        'id': '5e206d49c6788b7bda608781'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'El Dorado',
        'id': '5e206d49c6788b7bda608782'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Buenaventura',
        'id': '5e206d49c6788b7bda608783'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Granada',
        'id': '5e206d49c6788b7bda608784'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Guamal',
        'id': '5e206d49c6788b7bda608785'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Mapiripán',
        'id': '5e206d49c6788b7bda608786'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Mesetas',
        'id': '5e206d4ac6788b7bda608787'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'La Macarena',
        'id': '5e206d4ac6788b7bda608788'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Uribe',
        'id': '5e206d4ac6788b7bda608789'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Lejanías',
        'id': '5e206d4ac6788b7bda60878a'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Puerto Concordia',
        'id': '5e206d4ac6788b7bda60878b'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Puerto Gaitán',
        'id': '5e206d4ac6788b7bda60878c'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Puerto López',
        'id': '5e206d4ac6788b7bda60878d'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Puerto Lleras',
        'id': '5e206d4ac6788b7bda60878e'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Puerto Rico',
        'id': '5e206d4ac6788b7bda60878f'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Restrepo',
        'id': '5e206d4bc6788b7bda608790'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Ciénaga',
        'id': '5e206d4bc6788b7bda608791'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Ponedera',
        'id': '5e206d4bc6788b7bda608792'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'San Juanito',
        'id': '5e206d4bc6788b7bda608793'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'San Martín',
        'id': '5e206d4bc6788b7bda608794'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Vista Hermosa',
        'id': '5e206d4bc6788b7bda608795'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Pasto',
        'id': '5e206d4bc6788b7bda608796'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Albán',
        'id': '5e206d4bc6788b7bda608797'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Aldana',
        'id': '5e206d4cc6788b7bda608798'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Ancuyá',
        'id': '5e206d4cc6788b7bda608799'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tununguá',
        'id': '5e206d4cc6788b7bda60879a'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Barbacoas',
        'id': '5e206d4cc6788b7bda60879b'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Motavita',
        'id': '5e206d4cc6788b7bda60879c'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'San Bernardo del Viento',
        'id': '5e206d4cc6788b7bda60879d'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Colón',
        'id': '5e206d4cc6788b7bda60879e'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Consaca',
        'id': '5e206d4cc6788b7bda60879f'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Contadero',
        'id': '5e206d4cc6788b7bda6087a0'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Córdoba',
        'id': '5e206d4dc6788b7bda6087a1'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Cuaspud',
        'id': '5e206d4dc6788b7bda6087a2'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Cumbal',
        'id': '5e206d4dc6788b7bda6087a3'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Cumbitara',
        'id': '5e206d4dc6788b7bda6087a4'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'El Charco',
        'id': '5e206d4dc6788b7bda6087a5'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'El Peñol',
        'id': '5e206d4dc6788b7bda6087a6'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'El Rosario',
        'id': '5e206d4dc6788b7bda6087a7'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Istmina',
        'id': '5e206d4dc6788b7bda6087a8'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'El Tambo',
        'id': '5e206d4ec6788b7bda6087a9'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Funes',
        'id': '5e206d4ec6788b7bda6087aa'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Guachucal',
        'id': '5e206d4ec6788b7bda6087ab'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Guaitarilla',
        'id': '5e206d4ec6788b7bda6087ac'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Gualmatán',
        'id': '5e206d4ec6788b7bda6087ad'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Iles',
        'id': '5e206d4ec6788b7bda6087ae'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Imués',
        'id': '5e206d4ec6788b7bda6087af'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Ipiales',
        'id': '5e206d4ec6788b7bda6087b0'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'La Cruz',
        'id': '5e206d4fc6788b7bda6087b1'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'La Florida',
        'id': '5e206d4fc6788b7bda6087b2'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'La Llanada',
        'id': '5e206d4fc6788b7bda6087b3'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'La Tola',
        'id': '5e206d4fc6788b7bda6087b4'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'La Unión',
        'id': '5e206d4fc6788b7bda6087b5'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Leiva',
        'id': '5e206d4fc6788b7bda6087b6'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Linares',
        'id': '5e206d4fc6788b7bda6087b7'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Los Andes',
        'id': '5e206d4fc6788b7bda6087b8'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Magüí',
        'id': '5e206d4fc6788b7bda6087b9'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Mallama',
        'id': '5e206d50c6788b7bda6087ba'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Mosquera',
        'id': '5e206d50c6788b7bda6087bb'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Nariño',
        'id': '5e206d50c6788b7bda6087bc'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Olaya Herrera',
        'id': '5e206d50c6788b7bda6087bd'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Ospina',
        'id': '5e206d50c6788b7bda6087be'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Francisco Pizarro',
        'id': '5e206d50c6788b7bda6087bf'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Policarpa',
        'id': '5e206d50c6788b7bda6087c0'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Potosí',
        'id': '5e206d50c6788b7bda6087c1'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Providencia',
        'id': '5e206d51c6788b7bda6087c2'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Puerres',
        'id': '5e206d51c6788b7bda6087c3'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Pupiales',
        'id': '5e206d51c6788b7bda6087c4'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Ricaurte',
        'id': '5e206d51c6788b7bda6087c5'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Roberto Payán',
        'id': '5e206d51c6788b7bda6087c6'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Samaniego',
        'id': '5e206d51c6788b7bda6087c7'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Sandoná',
        'id': '5e206d51c6788b7bda6087c8'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'San Bernardo',
        'id': '5e206d51c6788b7bda6087c9'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'San Lorenzo',
        'id': '5e206d51c6788b7bda6087ca'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'San Pablo',
        'id': '5e206d52c6788b7bda6087cb'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Belmira',
        'id': '5e206d52c6788b7bda6087cc'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Ciénega',
        'id': '5e206d52c6788b7bda6087cd'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Santa Bárbara',
        'id': '5e206d52c6788b7bda6087ce'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Sapuyes',
        'id': '5e206d52c6788b7bda6087cf'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Taminango',
        'id': '5e206d52c6788b7bda6087d0'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Tangua',
        'id': '5e206d52c6788b7bda6087d1'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Santacruz',
        'id': '5e206d52c6788b7bda6087d2'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Túquerres',
        'id': '5e206d53c6788b7bda6087d3'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'Yacuanquer',
        'id': '5e206d53c6788b7bda6087d4'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Puerto Wilches',
        'id': '5e206d53c6788b7bda6087d5'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Puerto Parra',
        'id': '5e206d53c6788b7bda6087d6'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Armenia',
        'id': '5e206d53c6788b7bda6087d7'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Buenavista',
        'id': '5e206d53c6788b7bda6087d8'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Circasia',
        'id': '5e206d53c6788b7bda6087d9'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Córdoba',
        'id': '5e206d53c6788b7bda6087da'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Filandia',
        'id': '5e206d54c6788b7bda6087db'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'La Tebaida',
        'id': '5e206d54c6788b7bda6087dc'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Montenegro',
        'id': '5e206d54c6788b7bda6087dd'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Pijao',
        'id': '5e206d54c6788b7bda6087de'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Quimbaya',
        'id': '5e206d54c6788b7bda6087df'
    },
    {
        'DEPARTAMENTO': 'Quindío',
        'MUNICIPIO': 'Salento',
        'id': '5e206d54c6788b7bda6087e0'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Pereira',
        'id': '5e206d54c6788b7bda6087e1'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Apía',
        'id': '5e206d54c6788b7bda6087e2'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Balboa',
        'id': '5e206d54c6788b7bda6087e3'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Dosquebradas',
        'id': '5e206d55c6788b7bda6087e4'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Guática',
        'id': '5e206d55c6788b7bda6087e5'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'La Celia',
        'id': '5e206d55c6788b7bda6087e6'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'La Virginia',
        'id': '5e206d55c6788b7bda6087e7'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Marsella',
        'id': '5e206d55c6788b7bda6087e8'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Mistrató',
        'id': '5e206d55c6788b7bda6087e9'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Pueblo Rico',
        'id': '5e206d55c6788b7bda6087ea'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Quinchía',
        'id': '5e206d55c6788b7bda6087eb'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Santuario',
        'id': '5e206d56c6788b7bda6087ec'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Bucaramanga',
        'id': '5e206d56c6788b7bda6087ed'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Aguada',
        'id': '5e206d56c6788b7bda6087ee'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Albania',
        'id': '5e206d56c6788b7bda6087ef'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Aratoca',
        'id': '5e206d56c6788b7bda6087f0'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Barbosa',
        'id': '5e206d56c6788b7bda6087f1'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Barichara',
        'id': '5e206d56c6788b7bda6087f2'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Barrancabermeja',
        'id': '5e206d56c6788b7bda6087f3'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Betulia',
        'id': '5e206d57c6788b7bda6087f4'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Bolívar',
        'id': '5e206d57c6788b7bda6087f5'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Cabrera',
        'id': '5e206d57c6788b7bda6087f6'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'California',
        'id': '5e206d57c6788b7bda6087f7'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Carcasí',
        'id': '5e206d57c6788b7bda6087f8'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Cepitá',
        'id': '5e206d57c6788b7bda6087f9'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Cerrito',
        'id': '5e206d57c6788b7bda6087fa'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Charalá',
        'id': '5e206d57c6788b7bda6087fb'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Charta',
        'id': '5e206d57c6788b7bda6087fc'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Chipatá',
        'id': '5e206d58c6788b7bda6087fd'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Cimitarra',
        'id': '5e206d58c6788b7bda6087fe'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Concepción',
        'id': '5e206d58c6788b7bda6087ff'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Confines',
        'id': '5e206d58c6788b7bda608800'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Contratación',
        'id': '5e206d58c6788b7bda608801'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Coromoro',
        'id': '5e206d58c6788b7bda608802'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Curití',
        'id': '5e206d58c6788b7bda608803'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'El Guacamayo',
        'id': '5e206d58c6788b7bda608804'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'El Playón',
        'id': '5e206d59c6788b7bda608805'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Encino',
        'id': '5e206d59c6788b7bda608806'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Enciso',
        'id': '5e206d59c6788b7bda608807'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Florián',
        'id': '5e206d59c6788b7bda608808'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Floridablanca',
        'id': '5e206d59c6788b7bda608809'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Galán',
        'id': '5e206d59c6788b7bda60880a'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Gambita',
        'id': '5e206d59c6788b7bda60880b'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Girón',
        'id': '5e206d59c6788b7bda60880c'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Guaca',
        'id': '5e206d59c6788b7bda60880d'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Guadalupe',
        'id': '5e206d5ac6788b7bda60880e'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Guapotá',
        'id': '5e206d5ac6788b7bda60880f'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Guavatá',
        'id': '5e206d5ac6788b7bda608810'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Güepsa',
        'id': '5e206d5ac6788b7bda608811'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Jesús María',
        'id': '5e206d5ac6788b7bda608812'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Jordán',
        'id': '5e206d5ac6788b7bda608813'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'La Belleza',
        'id': '5e206d5ac6788b7bda608814'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Landázuri',
        'id': '5e206d5ac6788b7bda608815'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'La Paz',
        'id': '5e206d5bc6788b7bda608816'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Lebríja',
        'id': '5e206d5bc6788b7bda608817'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Los Santos',
        'id': '5e206d5bc6788b7bda608818'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Macaravita',
        'id': '5e206d5bc6788b7bda608819'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Málaga',
        'id': '5e206d5bc6788b7bda60881a'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Matanza',
        'id': '5e206d5bc6788b7bda60881b'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Mogotes',
        'id': '5e206d5bc6788b7bda60881c'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Molagavita',
        'id': '5e206d5bc6788b7bda60881d'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Ocamonte',
        'id': '5e206d5cc6788b7bda60881e'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Oiba',
        'id': '5e206d5cc6788b7bda60881f'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Onzaga',
        'id': '5e206d5cc6788b7bda608820'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Palmar',
        'id': '5e206d5cc6788b7bda608821'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Páramo',
        'id': '5e206d5cc6788b7bda608822'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Piedecuesta',
        'id': '5e206d5cc6788b7bda608823'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Pinchote',
        'id': '5e206d5cc6788b7bda608824'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Puente Nacional',
        'id': '5e206d5cc6788b7bda608825'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Rionegro',
        'id': '5e206d5cc6788b7bda608826'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'San Andrés',
        'id': '5e206d5dc6788b7bda608827'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'San Gil',
        'id': '5e206d5dc6788b7bda608828'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'San Joaquín',
        'id': '5e206d5dc6788b7bda608829'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'San Miguel',
        'id': '5e206d5dc6788b7bda60882a'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Santa Bárbara',
        'id': '5e206d5dc6788b7bda60882b'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Simacota',
        'id': '5e206d5dc6788b7bda60882c'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Socorro',
        'id': '5e206d5dc6788b7bda60882d'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Suaita',
        'id': '5e206d5dc6788b7bda60882e'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Sucre',
        'id': '5e206d5ec6788b7bda60882f'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Suratá',
        'id': '5e206d5ec6788b7bda608830'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Tona',
        'id': '5e206d5ec6788b7bda608831'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Vélez',
        'id': '5e206d5ec6788b7bda608832'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Vetas',
        'id': '5e206d5ec6788b7bda608833'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Villanueva',
        'id': '5e206d5ec6788b7bda608834'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Zapatoca',
        'id': '5e206d5ec6788b7bda608835'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Sincelejo',
        'id': '5e206d5ec6788b7bda608836'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Buenavista',
        'id': '5e206d5fc6788b7bda608837'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Caimito',
        'id': '5e206d5fc6788b7bda608838'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Coloso',
        'id': '5e206d5fc6788b7bda608839'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Coveñas',
        'id': '5e206d5fc6788b7bda60883a'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Chalán',
        'id': '5e206d5fc6788b7bda60883b'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'El Roble',
        'id': '5e206d5fc6788b7bda60883c'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Galeras',
        'id': '5e206d5fc6788b7bda60883d'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Guaranda',
        'id': '5e206d5fc6788b7bda60883e'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'La Unión',
        'id': '5e206d5fc6788b7bda60883f'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Los Palmitos',
        'id': '5e206d60c6788b7bda608840'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Majagual',
        'id': '5e206d60c6788b7bda608841'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Morroa',
        'id': '5e206d60c6788b7bda608842'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Ovejas',
        'id': '5e206d60c6788b7bda608843'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Palmito',
        'id': '5e206d60c6788b7bda608844'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'San Benito Abad',
        'id': '5e206d60c6788b7bda608845'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'San Marcos',
        'id': '5e206d60c6788b7bda608846'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'San Onofre',
        'id': '5e206d60c6788b7bda608847'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'San Pedro',
        'id': '5e206d61c6788b7bda608848'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Sucre',
        'id': '5e206d61c6788b7bda608849'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Tolú Viejo',
        'id': '5e206d61c6788b7bda60884a'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Alpujarra',
        'id': '5e206d61c6788b7bda60884b'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Alvarado',
        'id': '5e206d61c6788b7bda60884c'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Ambalema',
        'id': '5e206d61c6788b7bda60884d'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Armero',
        'id': '5e206d61c6788b7bda60884e'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Ataco',
        'id': '5e206d61c6788b7bda60884f'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Cajamarca',
        'id': '5e206d62c6788b7bda608850'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Chaparral',
        'id': '5e206d62c6788b7bda608851'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Coello',
        'id': '5e206d62c6788b7bda608852'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Coyaima',
        'id': '5e206d62c6788b7bda608853'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Cunday',
        'id': '5e206d62c6788b7bda608854'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Dolores',
        'id': '5e206d62c6788b7bda608855'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Espinal',
        'id': '5e206d62c6788b7bda608856'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Falan',
        'id': '5e206d62c6788b7bda608857'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Flandes',
        'id': '5e206d62c6788b7bda608858'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Fresno',
        'id': '5e206d63c6788b7bda608859'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Guamo',
        'id': '5e206d63c6788b7bda60885a'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Herveo',
        'id': '5e206d63c6788b7bda60885b'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Honda',
        'id': '5e206d63c6788b7bda60885c'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Icononzo',
        'id': '5e206d63c6788b7bda60885d'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Mariquita',
        'id': '5e206d63c6788b7bda60885e'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Melgar',
        'id': '5e206d63c6788b7bda60885f'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Murillo',
        'id': '5e206d63c6788b7bda608860'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Natagaima',
        'id': '5e206d64c6788b7bda608861'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Ortega',
        'id': '5e206d64c6788b7bda608862'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Palocabildo',
        'id': '5e206d64c6788b7bda608863'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Piedras',
        'id': '5e206d64c6788b7bda608864'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Planadas',
        'id': '5e206d64c6788b7bda608865'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Prado',
        'id': '5e206d64c6788b7bda608866'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Purificación',
        'id': '5e206d64c6788b7bda608867'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Rio Blanco',
        'id': '5e206d64c6788b7bda608868'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Roncesvalles',
        'id': '5e206d65c6788b7bda608869'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Rovira',
        'id': '5e206d65c6788b7bda60886a'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Saldaña',
        'id': '5e206d65c6788b7bda60886b'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Santa Isabel',
        'id': '5e206d65c6788b7bda60886c'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Venadillo',
        'id': '5e206d65c6788b7bda60886d'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Villahermosa',
        'id': '5e206d65c6788b7bda60886e'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Villarrica',
        'id': '5e206d65c6788b7bda60886f'
    },
    {
        'DEPARTAMENTO': 'Arauca',
        'MUNICIPIO': 'Arauquita',
        'id': '5e206d65c6788b7bda608870'
    },
    {
        'DEPARTAMENTO': 'Arauca',
        'MUNICIPIO': 'Cravo Norte',
        'id': '5e206d66c6788b7bda608871'
    },
    {
        'DEPARTAMENTO': 'Arauca',
        'MUNICIPIO': 'Fortul',
        'id': '5e206d66c6788b7bda608872'
    },
    {
        'DEPARTAMENTO': 'Arauca',
        'MUNICIPIO': 'Puerto Rondón',
        'id': '5e206d66c6788b7bda608873'
    },
    {
        'DEPARTAMENTO': 'Arauca',
        'MUNICIPIO': 'Saravena',
        'id': '5e206d66c6788b7bda608874'
    },
    {
        'DEPARTAMENTO': 'Arauca',
        'MUNICIPIO': 'Tame',
        'id': '5e206d66c6788b7bda608875'
    },
    {
        'DEPARTAMENTO': 'Arauca',
        'MUNICIPIO': 'Arauca',
        'id': '5e206d66c6788b7bda608876'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Yopal',
        'id': '5e206d66c6788b7bda608877'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Aguazul',
        'id': '5e206d66c6788b7bda608878'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Chámeza',
        'id': '5e206d67c6788b7bda608879'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Hato Corozal',
        'id': '5e206d67c6788b7bda60887a'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'La Salina',
        'id': '5e206d67c6788b7bda60887b'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Monterrey',
        'id': '5e206d67c6788b7bda60887c'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Pore',
        'id': '5e206d67c6788b7bda60887d'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Recetor',
        'id': '5e206d67c6788b7bda60887e'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Sabanalarga',
        'id': '5e206d67c6788b7bda60887f'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Sácama',
        'id': '5e206d67c6788b7bda608880'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Tauramena',
        'id': '5e206d67c6788b7bda608881'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Trinidad',
        'id': '5e206d68c6788b7bda608882'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Villanueva',
        'id': '5e206d68c6788b7bda608883'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Mocoa',
        'id': '5e206d68c6788b7bda608884'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Colón',
        'id': '5e206d68c6788b7bda608885'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Orito',
        'id': '5e206d68c6788b7bda608886'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Puerto Caicedo',
        'id': '5e206d68c6788b7bda608887'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Puerto Guzmán',
        'id': '5e206d68c6788b7bda608888'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Leguízamo',
        'id': '5e206d68c6788b7bda608889'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Sibundoy',
        'id': '5e206d69c6788b7bda60888a'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'San Francisco',
        'id': '5e206d69c6788b7bda60888b'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'San Miguel',
        'id': '5e206d69c6788b7bda60888c'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Santiago',
        'id': '5e206d69c6788b7bda60888d'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'Leticia',
        'id': '5e206d69c6788b7bda60888e'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'El Encanto',
        'id': '5e206d69c6788b7bda60888f'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'La Chorrera',
        'id': '5e206d69c6788b7bda608890'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'La Pedrera',
        'id': '5e206d69c6788b7bda608891'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'La Victoria',
        'id': '5e206d6ac6788b7bda608892'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'Puerto Arica',
        'id': '5e206d6ac6788b7bda608893'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'Puerto Nariño',
        'id': '5e206d6ac6788b7bda608894'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'Puerto Santander',
        'id': '5e206d6ac6788b7bda608895'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'Tarapacá',
        'id': '5e206d6ac6788b7bda608896'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'Inírida',
        'id': '5e206d6ac6788b7bda608897'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'Barranco Minas',
        'id': '5e206d6ac6788b7bda608898'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'Mapiripana',
        'id': '5e206d6ac6788b7bda608899'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'San Felipe',
        'id': '5e206d6bc6788b7bda60889a'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'Puerto Colombia',
        'id': '5e206d6bc6788b7bda60889b'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'La Guadalupe',
        'id': '5e206d6bc6788b7bda60889c'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'Cacahual',
        'id': '5e206d6bc6788b7bda60889d'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'Pana Pana',
        'id': '5e206d6bc6788b7bda60889e'
    },
    {
        'DEPARTAMENTO': 'Guainía',
        'MUNICIPIO': 'Morichal',
        'id': '5e206d6bc6788b7bda60889f'
    },
    {
        'DEPARTAMENTO': 'Vaupés',
        'MUNICIPIO': 'Mitú',
        'id': '5e206d6bc6788b7bda6088a0'
    },
    {
        'DEPARTAMENTO': 'Vaupés',
        'MUNICIPIO': 'Caruru',
        'id': '5e206d6bc6788b7bda6088a1'
    },
    {
        'DEPARTAMENTO': 'Vaupés',
        'MUNICIPIO': 'Pacoa',
        'id': '5e206d6bc6788b7bda6088a2'
    },
    {
        'DEPARTAMENTO': 'Vaupés',
        'MUNICIPIO': 'Taraira',
        'id': '5e206d6cc6788b7bda6088a3'
    },
    {
        'DEPARTAMENTO': 'Vaupés',
        'MUNICIPIO': 'Papunaua',
        'id': '5e206d6cc6788b7bda6088a4'
    },
    {
        'DEPARTAMENTO': 'Vichada',
        'MUNICIPIO': 'Puerto Carreño',
        'id': '5e206d6cc6788b7bda6088a5'
    },
    {
        'DEPARTAMENTO': 'Vichada',
        'MUNICIPIO': 'La Primavera',
        'id': '5e206d6cc6788b7bda6088a6'
    },
    {
        'DEPARTAMENTO': 'Vichada',
        'MUNICIPIO': 'Santa Rosalía',
        'id': '5e206d6cc6788b7bda6088a7'
    },
    {
        'DEPARTAMENTO': 'Vichada',
        'MUNICIPIO': 'Cumaribo',
        'id': '5e206d6cc6788b7bda6088a8'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'San José del Fragua',
        'id': '5e206d6cc6788b7bda6088a9'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Barranca de Upía',
        'id': '5e206d6cc6788b7bda6088aa'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Palmas del Socorro',
        'id': '5e206d6dc6788b7bda6088ab'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'San Juan de Río Seco',
        'id': '5e206d6dc6788b7bda6088ac'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Juan de Acosta',
        'id': '5e206d6dc6788b7bda6088ad'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'Fuente de Oro',
        'id': '5e206d6dc6788b7bda6088ae'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'San Luis de Gaceno',
        'id': '5e206d6dc6788b7bda6088af'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'El Litoral del San Juan',
        'id': '5e206d6dc6788b7bda6088b0'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Villa de San Diego de Ubate',
        'id': '5e206d6dc6788b7bda6088b1'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Barranco de Loba',
        'id': '5e206d6dc6788b7bda6088b2'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Togüí',
        'id': '5e206d6ec6788b7bda6088b3'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Santa Rosa del Sur',
        'id': '5e206d6ec6788b7bda6088b4'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'El Cantón del San Pablo',
        'id': '5e206d6ec6788b7bda6088b5'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Villa de Leyva',
        'id': '5e206d6ec6788b7bda6088b6'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'San Sebastián de Buenavista',
        'id': '5e206d6ec6788b7bda6088b7'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Paz de Río',
        'id': '5e206d6ec6788b7bda6088b8'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Hatillo de Loba',
        'id': '5e206d6ec6788b7bda6088b9'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Sabanas de San Angel',
        'id': '5e206d6ec6788b7bda6088ba'
    },
    {
        'DEPARTAMENTO': 'Guaviare',
        'MUNICIPIO': 'Calamar',
        'id': '5e206d6ec6788b7bda6088bb'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'Río de Oro',
        'id': '5e206d6fc6788b7bda6088bc'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Pedro de Uraba',
        'id': '5e206d6fc6788b7bda6088bd'
    },
    {
        'DEPARTAMENTO': 'Guaviare',
        'MUNICIPIO': 'San José del Guaviare',
        'id': '5e206d6fc6788b7bda6088be'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Santa Rosa de Viterbo',
        'id': '5e206d6fc6788b7bda6088bf'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'Santander de Quilichao',
        'id': '5e206d6fc6788b7bda6088c0'
    },
    {
        'DEPARTAMENTO': 'Guaviare',
        'MUNICIPIO': 'Miraflores',
        'id': '5e206d6fc6788b7bda6088c1'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Santafé de Antioquia',
        'id': '5e206d6fc6788b7bda6088c2'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'San Carlos de Guaroa',
        'id': '5e206d6fc6788b7bda6088c3'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Palmar de Varela',
        'id': '5e206d70c6788b7bda6088c4'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Santa Rosa de Osos',
        'id': '5e206d70c6788b7bda6088c5'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Andrés de Cuerquía',
        'id': '5e206d70c6788b7bda6088c6'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Valle de San Juan',
        'id': '5e206d70c6788b7bda6088c7'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'San Vicente de Chucurí',
        'id': '5e206d70c6788b7bda6088c8'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'San José de Miranda',
        'id': '5e206d70c6788b7bda6088c9'
    },
    {
        'DEPARTAMENTO': 'Archipiélago de San Andrés, Providencia y Santa Catalina',
        'MUNICIPIO': 'Providencia',
        'id': '5e206d70c6788b7bda6088ca'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Santa Rosa de Cabal',
        'id': '5e206d70c6788b7bda6088cb'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Guayabal de Siquima',
        'id': '5e206d71c6788b7bda6088cc'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Belén de Los Andaquies',
        'id': '5e206d71c6788b7bda6088cd'
    },
    {
        'DEPARTAMENTO': 'Casanare',
        'MUNICIPIO': 'Paz de Ariporo',
        'id': '5e206d71c6788b7bda6088ce'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Santa Helena del Opón',
        'id': '5e206d71c6788b7bda6088cf'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'San Pablo de Borbur',
        'id': '5e206d71c6788b7bda6088d0'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'La Jagua del Pilar',
        'id': '5e206d71c6788b7bda6088d1'
    },
    {
        'DEPARTAMENTO': 'Cesar',
        'MUNICIPIO': 'La Jagua de Ibirico',
        'id': '5e206d71c6788b7bda6088d2'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'San Luis de Sincé',
        'id': '5e206d71c6788b7bda6088d3'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'San Luis de Gaceno',
        'id': '5e206d72c6788b7bda6088d4'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'El Carmen de Bolívar',
        'id': '5e206d72c6788b7bda6088d5'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'El Carmen de Atrato',
        'id': '5e206d72c6788b7bda6088d6'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'San Juan de Betulia',
        'id': '5e206d72c6788b7bda6088d7'
    },
    {
        'DEPARTAMENTO': 'Magdalena',
        'MUNICIPIO': 'Pijiño del Carmen',
        'id': '5e206d72c6788b7bda6088d8'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'Vigía del Fuerte',
        'id': '5e206d72c6788b7bda6088d9'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Martín de Loba',
        'id': '5e206d72c6788b7bda6088da'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'Altos del Rosario',
        'id': '5e206d72c6788b7bda6088db'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'Carmen de Apicala',
        'id': '5e206d72c6788b7bda6088dc'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'San Antonio del Tequendama',
        'id': '5e206d73c6788b7bda6088dd'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Sabana de Torres',
        'id': '5e206d73c6788b7bda6088de'
    },
    {
        'DEPARTAMENTO': 'Guaviare',
        'MUNICIPIO': 'El Retorno',
        'id': '5e206d73c6788b7bda6088df'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'San José de Uré',
        'id': '5e206d73c6788b7bda6088e0'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'San Pedro de Cartago',
        'id': '5e206d73c6788b7bda6088e1'
    },
    {
        'DEPARTAMENTO': 'Atlántico',
        'MUNICIPIO': 'Campo de La Cruz',
        'id': '5e206d73c6788b7bda6088e2'
    },
    {
        'DEPARTAMENTO': 'Meta',
        'MUNICIPIO': 'San Juan de Arama',
        'id': '5e206d73c6788b7bda6088e3'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San José de La Montaña',
        'id': '5e206d73c6788b7bda6088e4'
    },
    {
        'DEPARTAMENTO': 'Caquetá',
        'MUNICIPIO': 'Cartagena del Chairá',
        'id': '5e206d74c6788b7bda6088e5'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'San José del Palmar',
        'id': '5e206d74c6788b7bda6088e6'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Agua de Dios',
        'id': '5e206d74c6788b7bda6088e7'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Jacinto del Cauca',
        'id': '5e206d74c6788b7bda6088e8'
    },
    {
        'DEPARTAMENTO': 'Huila',
        'MUNICIPIO': 'San Agustín',
        'id': '5e206d74c6788b7bda6088e9'
    },
    {
        'DEPARTAMENTO': 'Nariño',
        'MUNICIPIO': 'El Tablón de Gómez',
        'id': '5e206d74c6788b7bda6088ea'
    },
    {
        'DEPARTAMENTO': 'Archipiélago de San Andrés, Providencia y Santa Catalina',
        'MUNICIPIO': 'San Andrés',
        'id': '5e206d74c6788b7bda6088eb'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'San José de Pare',
        'id': '5e206d74c6788b7bda6088ec'
    },
    {
        'DEPARTAMENTO': 'Putumayo',
        'MUNICIPIO': 'Valle de Guamez',
        'id': '5e206d74c6788b7bda6088ed'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Pablo de Borbur',
        'id': '5e206d75c6788b7bda6088ee'
    },
    {
        'DEPARTAMENTO': 'Sucre',
        'MUNICIPIO': 'Santiago de Tolú',
        'id': '5e206d75c6788b7bda6088ef'
    },
    {
        'DEPARTAMENTO': 'Bogotá D.C.',
        'MUNICIPIO': 'Bogotá D.C.',
        'id': '5e206d75c6788b7bda6088f0'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Carmen de Carupa',
        'id': '5e206d75c6788b7bda6088f1'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'Ciénaga de Oro',
        'id': '5e206d75c6788b7bda6088f2'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Juan de Urabá',
        'id': '5e206d75c6788b7bda6088f3'
    },
    {
        'DEPARTAMENTO': 'La Guajira',
        'MUNICIPIO': 'San Juan del Cesar',
        'id': '5e206d75c6788b7bda6088f4'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'El Carmen de Chucurí',
        'id': '5e206d75c6788b7bda6088f5'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'El Carmen de Viboral',
        'id': '5e206d76c6788b7bda6088f6'
    },
    {
        'DEPARTAMENTO': 'Risaralda',
        'MUNICIPIO': 'Belén de Umbría',
        'id': '5e206d76c6788b7bda6088f7'
    },
    {
        'DEPARTAMENTO': 'Chocó',
        'MUNICIPIO': 'Belén de Bajira',
        'id': '5e206d76c6788b7bda6088f8'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Valle de San José',
        'id': '5e206d76c6788b7bda6088f9'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'San Luis',
        'id': '5e206d76c6788b7bda6088fa'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'San Miguel de Sema',
        'id': '5e206d76c6788b7bda6088fb'
    },
    {
        'DEPARTAMENTO': 'Tolima',
        'MUNICIPIO': 'San Antonio',
        'id': '5e206d76c6788b7bda6088fc'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'San Benito',
        'id': '5e206d76c6788b7bda6088fd'
    },
    {
        'DEPARTAMENTO': 'Cundinamarca',
        'MUNICIPIO': 'Vergara',
        'id': '5e206d76c6788b7bda6088fe'
    },
    {
        'DEPARTAMENTO': 'Córdoba',
        'MUNICIPIO': 'San Carlos',
        'id': '5e206d77c6788b7bda6088ff'
    },
    {
        'DEPARTAMENTO': 'Amazonas',
        'MUNICIPIO': 'Puerto Alegría',
        'id': '5e206d77c6788b7bda608900'
    },
    {
        'DEPARTAMENTO': 'Santander',
        'MUNICIPIO': 'Hato',
        'id': '5e206d77c6788b7bda608901'
    },
    {
        'DEPARTAMENTO': 'Bolívar',
        'MUNICIPIO': 'San Jacinto',
        'id': '5e206d77c6788b7bda608902'
    },
    {
        'DEPARTAMENTO': 'Cauca',
        'MUNICIPIO': 'San Sebastián',
        'id': '5e206d77c6788b7bda608903'
    },
    {
        'DEPARTAMENTO': 'Antioquia',
        'MUNICIPIO': 'San Carlos',
        'id': '5e206d77c6788b7bda608904'
    },
    {
        'DEPARTAMENTO': 'Boyacá',
        'MUNICIPIO': 'Tuta',
        'id': '5e206d77c6788b7bda608905'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Silos',
        'id': '5e206d77c6788b7bda608906'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Cácota',
        'id': '5e206d78c6788b7bda608907'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'El Dovio',
        'id': '5e206d78c6788b7bda608908'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Toledo',
        'id': '5e206d78c6788b7bda608909'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Roldanillo',
        'id': '5e206d78c6788b7bda60890a'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Mutiscua',
        'id': '5e206d78c6788b7bda60890b'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Argelia',
        'id': '5e206d78c6788b7bda60890c'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'El Zulia',
        'id': '5e206d78c6788b7bda60890d'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Salazar',
        'id': '5e206d78c6788b7bda60890e'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Sevilla',
        'id': '5e206d78c6788b7bda60890f'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Zarzal',
        'id': '5e206d79c6788b7bda608910'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Cucutilla',
        'id': '5e206d79c6788b7bda608911'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'El Cerrito',
        'id': '5e206d79c6788b7bda608912'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Cartago',
        'id': '5e206d79c6788b7bda608913'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Caicedonia',
        'id': '5e206d79c6788b7bda608914'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Puerto Santander',
        'id': '5e206d79c6788b7bda608915'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Gramalote',
        'id': '5e206d79c6788b7bda608916'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'El Cairo',
        'id': '5e206d79c6788b7bda608917'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'El Tarra',
        'id': '5e206d7ac6788b7bda608918'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'La Unión',
        'id': '5e206d7ac6788b7bda608919'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Restrepo',
        'id': '5e206d7ac6788b7bda60891a'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Teorama',
        'id': '5e206d7ac6788b7bda60891b'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Dagua',
        'id': '5e206d7ac6788b7bda60891c'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Arboledas',
        'id': '5e206d7ac6788b7bda60891d'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Guacarí',
        'id': '5e206d7ac6788b7bda60891e'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Lourdes',
        'id': '5e206d7ac6788b7bda60891f'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Ansermanuevo',
        'id': '5e206d7bc6788b7bda608920'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Bochalema',
        'id': '5e206d7bc6788b7bda608921'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Bugalagrande',
        'id': '5e206d7bc6788b7bda608922'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Convención',
        'id': '5e206d7bc6788b7bda608923'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Hacarí',
        'id': '5e206d7bc6788b7bda608924'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'La Victoria',
        'id': '5e206d7bc6788b7bda608925'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Herrán',
        'id': '5e206d7bc6788b7bda608926'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Ginebra',
        'id': '5e206d7bc6788b7bda608927'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Yumbo',
        'id': '5e206d7bc6788b7bda608928'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Obando',
        'id': '5e206d7cc6788b7bda608929'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Tibú',
        'id': '5e206d7cc6788b7bda60892a'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'San Cayetano',
        'id': '5e206d7cc6788b7bda60892b'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'San Calixto',
        'id': '5e206d7cc6788b7bda60892c'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Bolívar',
        'id': '5e206d7cc6788b7bda60892d'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'La Playa',
        'id': '5e206d7cc6788b7bda60892e'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Cali',
        'id': '5e206d7dc6788b7bda60892f'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'San Pedro',
        'id': '5e206d7dc6788b7bda608930'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Guadalajara de Buga',
        'id': '5e206d7dc6788b7bda608931'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Chinácota',
        'id': '5e206d7dc6788b7bda608932'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Ragonvalia',
        'id': '5e206d7dc6788b7bda608933'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'La Esperanza',
        'id': '5e206d7ec6788b7bda608934'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Villa del Rosario',
        'id': '5e206d7ec6788b7bda608935'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Chitagá',
        'id': '5e206d7ec6788b7bda608936'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Calima',
        'id': '5e206d7ec6788b7bda608937'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Sardinata',
        'id': '5e206d7ec6788b7bda608938'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Andalucía',
        'id': '5e206d7ec6788b7bda608939'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Pradera',
        'id': '5e206d7fc6788b7bda60893a'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Abrego',
        'id': '5e206d7fc6788b7bda60893b'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Los Patios',
        'id': '5e206d7fc6788b7bda60893c'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Ocaña',
        'id': '5e206d7fc6788b7bda60893d'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Bucarasica',
        'id': '5e206d7fc6788b7bda60893e'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Yotoco',
        'id': '5e206d80c6788b7bda60893f'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Palmira',
        'id': '5e206d80c6788b7bda608940'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Riofrío',
        'id': '5e206d80c6788b7bda608941'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Santiago',
        'id': '5e206d80c6788b7bda608942'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Alcalá',
        'id': '5e206d80c6788b7bda608943'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Versalles',
        'id': '5e206d81c6788b7bda608944'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Labateca',
        'id': '5e206d81c6788b7bda608945'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Cachirá',
        'id': '5e206d81c6788b7bda608946'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Villa Caro',
        'id': '5e206d81c6788b7bda608947'
    },
    {
        'DEPARTAMENTO': 'Norte de Santander',
        'MUNICIPIO': 'Durania',
        'id': '5e206d81c6788b7bda608948'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'El Águila',
        'id': '5e206d82c6788b7bda608949'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Toro',
        'id': '5e206d82c6788b7bda60894a'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Candelaria',
        'id': '5e206d82c6788b7bda60894b'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'La Cumbre',
        'id': '5e206d82c6788b7bda60894c'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Ulloa',
        'id': '5e206d82c6788b7bda60894d'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Trujillo',
        'id': '5e206d83c6788b7bda60894e'
    },
    {
        'DEPARTAMENTO': 'Valle del Cauca',
        'MUNICIPIO': 'Vijes',
        'id': '5e206d83c6788b7bda60894f'
    }
];
  departamentos = [
    'Antioquia',
    'Santander',
    'Sucre',
    'Casanare',
    'Norte de Santander',
    'Cundinamarca',
    'Vaupés',
    'Córdoba',
    'Putumayo',
    'Nariño',
    'Atlántico',
    'Bolívar',
    'Valle del Cauca',
    'Tolima',
    'Caquetá',
    'Amazonas',
    'Boyacá',
    'Cauca',
    'Chocó',
    'Caldas',
    'Magdalena',
    'Cesar',
    'Quindío',
    'Meta',
    'Huila',
    'La Guajira',
    'Risaralda',
    'Arauca',
    'Guainía',
    'Vichada',
    'Guaviare',
    'Archipiélago de San Andrés, Providencia y Santa Catalina',
    'Bogotá D.C.'
  ];
  newMunicipios = [];
  isColombia = true;
  isOtro = true;
  isNoColombia = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private TipoafiliadoSer: TipoafiliadoService,
    private http: HttpClient,
    private CategoriaSer: CategoriaService,
    private ClasificacionSer: ClasificacionService,
    private TipodocumentoSer: TipodocumentoService,
    private SeccionalSer: SeccionalService,
    private GrupoafinidadSer: GrupoafinidadService,
    private CiudadSer: CiudadService,
    private PaisSer: PaisService,
    private EstudioSer: EstudioService,
    private fb: FormBuilder,
    private DocumentoSer: DocumentoService,
    private Afiliado_ser: AfiliadoService,
    private TipodocumentosoporteSer: TipodocumentosoporteService,
    public toastr: ToastrManager) {
    this.createForm();
    this.createStudies();
    this.createAfinidad();
    this.createDocumento();
    this.createBeneficiario();
  }

  createForm() {
    this.angForm = this.fb.group({
      tipoafiliado: [null],
      representantelegal: [null],
      categoria: ['', Validators.required],
      codigocliente: ['', Validators.required],
      nombres: ['', Validators.required],
      apellidos: [null],
      clasificacion: ['', Validators.required],
      tipodocumento: ['', Validators.required],
      documento: ['', Validators.required],
      ciiu: [null],
      seccional: ['', Validators.required],
      pais: ['', Validators.required],
      ciudad: ['', Validators.required],
      direccion: ['', Validators.required],
      telefono: ['', Validators.required],
      email: ['', Validators.required],
      sitioweb: [null],
      informaciondirectorio: ['', Validators.required],
      fechanacimiento: [null],
      fechavinculacion: [null],
      abeasdata: [null],
      lavadoactivos: [null],
      municipio: [null],
      departamento: [null],


    });
  }
  createStudies() {
    this.form = this.fb.group({
      universidad: ['', Validators.required],
      titulo: ['', Validators.required],
      nivel: ['', Validators.required],
    });

  }
  createBeneficiario() {
    this.formBeneficiario = this.fb.group({
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      cargo: ['', Validators.required],
      correo: ['', Validators.required],
      telefono: ['', Validators.required],
    });
  }
  createAfinidad() {
    this.formAfinidad = this.fb.group({
      subafinidad: ['', Validators.required],
      afinidad: ['', Validators.required],
    });


  }
  createDocumento() {
    this.formDocumento = this.fb.group({
      subafinidad: ['', Validators.required],
      afinidad: ['', Validators.required],
    });


  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
  }
  showFile() {
    this.toastr.successToastr('El archivo se subio correctamente.', 'Subido!');
  }
  showError() {
    this.toastr.errorToastr('El registro no pudo ser guardado', 'Error');
  }
  uploadFile() {
    this.toastr.customToastr(

      `<div class="center">
      <h3 class="center" style='color:black;'>
      Subiendo
      <img class="center" width="40px" height="40px"
      src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///
      wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/
      hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+
      QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnA
      dOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQ
      JCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGB
      MRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujI
      jK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQo
      AAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDg
      sGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIp
      z8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradyleso
      jEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WG
      c3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDI
      I7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
      </h3>

    </div>`,
      null,
      {
        enableHTML: true,
        position: 'top-center',
        toastTimeout: 2000
      }
    );
  }
  addAfiliado(tipoafiliado,
    categoria,
    codigocliente,
    nombres,
    apellidos,
    clasificacion,
    tipodocumento,
    documento,
    ciiu,
    seccional,
    // subafinidad,
    pais,
    ciudad,
    direccion,
    telefono,
    email,
    sitioweb,
    informaciondirectorio,
    fechanacimiento,
    fechavinculacion,
    representantelegal) {

    this.Afiliado_ser.addAfiliado(tipoafiliado,
      categoria,
      codigocliente,
      nombres,
      apellidos,
      clasificacion,
      tipodocumento,
      documento,
      ciiu,
      seccional,
      this.afinidadesId,
      pais || 'Colombia',
      ciudad  || this.newciudad || this.angForm.value['ciudad'],
      direccion,
      telefono,
      email,
      sitioweb,
      informaciondirectorio,
      fechanacimiento,
      fechavinculacion,
      this.estudios,
      this.documentosId,
      representantelegal,
      this.beneficiarios)
      .subscribe((data) => {
      if (data['status'] === 'created' ) {
        this.angForm.reset();
        this.estudios = [];
        this.documentosId = [];
        this.estudiosAll = [];
        this.documentos = [];
        this.afinidades = [];
        this.afinidadesId = [];
        this.beneficiarios = [];
       // this.router.navigate([`/afiliado/detail/${data['id']}`]);
        this.showSuccess();
      } else {
        this.showError();
      }
    });

  }

  ngOnInit() {


    this.form = this.fb.group({
      items: this.fb.array([this.createItem()])
    });
    this.formAfinidad = this.fb.group({
      items: this.fb.array([this.createItemAfinidad()])
    });
    this.formDocumento = this.fb.group({
      items: this.fb.array([this.createItemDocumento()])
    });
    this.formBeneficiario = this.fb.group({
      items: this.fb.array([this.createItemBeneficiario()])
    });
    this.TipoafiliadoSer
      .getTipoafiliado()
      .subscribe((data: Tipoafiliado[]) => {
        this.Tipoafiliado = data;
      });
    this.CategoriaSer
      .getCategoria()
      .subscribe((data: Categoria[]) => {
        this.Categoria = data;
      });
    this.ClasificacionSer
      .getClasificacion()
      .subscribe((data: Clasificacion[]) => {
        this.Clasificacion = data;
      });
    this.TipodocumentoSer
      .getTipodocumento()
      .subscribe((data: Tipodocumento[]) => {
        this.Tipodocumento = data;
      });
    this.SeccionalSer
      .getSeccional()
      .subscribe((data: Seccional[]) => {
        this.Seccional = data;
      });
    this.GrupoafinidadSer
      .getGrupoafinidad()
      .subscribe((data: Grupoafinidad[]) => {
        this.Grupoafinidad = data;

      });
    this.CiudadSer
      .getCiudad()
      .subscribe((data: Ciudad[]) => {
        this.Ciudad = data;
      });
    this.PaisSer
      .getPais()
      .subscribe((data: Pais[]) => {
        this.Pais = data;

      });
    this.TipodocumentosoporteSer
      .getTipodocumentosoporte()
      .subscribe((data: Tipodocumentosoporte[]) => {
        this.Tipodocumentosoporte = data;

      });
    this.IsApellidos = true;
    this.IsCiiu = true;
    this.IsFechanacimiento = true;
    this.IsNombre = true;
    this.IsEmpresa = true;
    this.ValidateStudies = true;
    this.ValidateAfinidad = true;
    this.ValidateDocumento = true;
    this.ValidateBeneficiario = true;
  }
  changeCountry(count) {
    this.Subafinidad = this.Grupoafinidad.find(con => con.nombre === count).subafinidad;

  }
  changePais(count) {

    if (count === 'Colombia') {

      this.isColombia = false;
      this.isNoColombia = true;
    } else {
      if (count === 'Otro') {
        this.isColombia = true;
        this.isNoColombia = true;
        this.isOtro = false;

      }
      this.Ciudades = this.Pais.find(con => con.nombre === count).ciudad;
      this.isColombia = true;
      this.isNoColombia = false;
    }


  }
  changeDepartamento(count) {
    this.newMunicipios = this.municipios.filter(municipio => municipio.DEPARTAMENTO === count);

    this.idmunicipio = '5dd45baba37661000885033e';
  }
  changetipo(tipo) {
    if (tipo === '5d1f888bb0faed0009d6b4d5') {
      this.tipodocsop = this.Tipodocumentosoporte.filter(docsoporte => docsoporte.tipo === 'NATURAL');
      this.elnombre = 'Nombres';
      this.IsApellidos = false;
      this.IsCiiu = true;
      this.IsFechanacimiento = false;
      this.IsNombre = false;
      this.IsEmpresa = true;
    }
    if (tipo === '5d1f88c8b0faed0009d6b4d6') {
      this.tipodocsop = this.Tipodocumentosoporte.filter(docsoporte => docsoporte.tipo === 'JURIDICO');
      this.elnombre = 'Razon Social';
      this.IsApellidos = true;
      this.IsCiiu = false;
      this.IsFechanacimiento = true;
      this.IsNombre = true;
      this.IsEmpresa = false;

    }

  }
  changeclasificacion(clasificacion) {
    if (clasificacion === '5d6436efae1da30009344e5d' ||
      clasificacion === '5d6436f5ae1da30009344e5e' ||
      clasificacion === '5d6436fcae1da30009344e5f') {
      this.pretipo = '5d1f888bb0faed0009d6b4d5';
      this.IsApellidos = false;
      this.IsCiiu = true;
      this.IsFechanacimiento = false;
      this.IsNombre = false;
      this.IsEmpresa = true;
    }
    if (clasificacion === '5d643703ae1da30009344e60' ||
      clasificacion === '5d64370dae1da30009344e61' ||
      clasificacion === '5d643718ae1da30009344e62' ||
      clasificacion === '5d64372bae1da30009344e63' ||
      clasificacion === '5d64373cae1da30009344e64' ||
      clasificacion === '5d64376eae1da30009344e65' ||
      clasificacion === '5d643778ae1da30009344e66' ||
      clasificacion === '5d643781ae1da30009344e67' ||
      clasificacion === '5d643788ae1da30009344e68' ||
      clasificacion === '5d6437a4ae1da30009344e69' ||
      clasificacion === '5d6437b8ae1da30009344e6a' ||
      clasificacion === '5d6437bfae1da30009344e6b' ||
      clasificacion === '5d6437c5ae1da30009344e6c' ||
      clasificacion === '5d6437d9ae1da30009344e6d' ||
      clasificacion === '5d6437dfae1da30009344e6e' ||
      clasificacion === '5d6437e4ae1da30009344e6f') {
      this.pretipo = '5d1f88c8b0faed0009d6b4d6';
      this.IsApellidos = true;
      this.IsCiiu = false;
      this.IsFechanacimiento = true;
      this.IsNombre = true;
      this.IsEmpresa = false;

    }

  }
  createItem() {
    return this.fb.group({
      universidad: [],
      titulo: [],
      nivel: []
    });
  }
  createItemAfinidad() {
    return this.fb.group({
      afinidad: [],
      subafinidad: []
    });
  }
  createItemDocumento() {
    return this.fb.group({
      tipodocumentosoporte: [],
      urldocumento: []
    });
  }
  createItemBeneficiario() {
    return this.fb.group({
      nombres: [],
      apellidos: [],
      cargo: [],
      correo: [],
      telefono: []
    });
  }
  addNext() {
    const len = this.form.value.items.length - 1;
    if (this.form.value.items[len].universidad && this.form.value.items[len].nivel && this.form.value.items[len].titulo) {
      this.ValidateStudies = true;
      //   (this.form.controls['items'] as FormArray).push(this.createItem())

      this.EstudioSer.addEstudio(this.form.value.items[len].nivel,
        this.form.value.items[len].titulo,
        this.form.value.items[len].universidad)
        .subscribe((data) => {
          this.respuesta = data;
          this.estudios.push(this.respuesta._id);
          this.estudiosAll.push(this.respuesta);

        });
      this.form.reset();

    } else {
      this.ValidateStudies = false;
    }


  }
  addNextBeneficiario() {
    const len = this.formBeneficiario.value.items.length - 1;
    if (this.formBeneficiario.value.items[len].nombres &&
      this.formBeneficiario.value.items[len].apellidos &&
      this.formBeneficiario.value.items[len].cargo &&
      this.formBeneficiario.value.items[len].correo &&
      this.formBeneficiario.value.items[len].correo) {
      this.ValidateBeneficiario = true;
      //   (this.form.controls['items'] as FormArray).push(this.createItem())

      this.beneficiarios.push(
        {
          nombres: this.formBeneficiario.value.items[len].nombres,
          apellidos: this.formBeneficiario.value.items[len].apellidos,
          cargo: this.formBeneficiario.value.items[len].cargo,
          correo: this.formBeneficiario.value.items[len].correo,
          telefono: this.formBeneficiario.value.items[len].telefono

        }
      );
      this.formBeneficiario.reset();

    } else {
      this.ValidateBeneficiario = false;
    }


  }
  addNextAfinidad() {
    const len = this.formAfinidad.value.items.length - 1;
    if (this.formAfinidad.value.items[len].afinidad && this.formAfinidad.value.items[len].subafinidad) {
      this.ValidateAfinidad = true;

      this.afinidades.push(this.formAfinidad.value.items[len].subafinidad);
      this.afinidadesId.push(this.formAfinidad.value.items[len].subafinidad._id);
      this.formAfinidad.reset();

    } else {
      this.ValidateAfinidad = false;
    }


  }
  addNextDocumento() {
    const len = this.formDocumento.value.items.length - 1;
    if (this.formDocumento.value.items[len].tipodocumentosoporte && this.formDocumento.value.items[len].urldocumento) {
      this.ValidateAfinidad = true;

      this.documentos.push(this.formDocumento.value.items[len].tipodocumentosoporte);
      // this.documentosId.push(this.formDocumento.value.items[len].subafinidad._id);
      this.formDocumento.reset();

    } else {
      this.ValidateAfinidad = false;
    }


  }
  deleteAfinidad(index) {
    if (index > -1) {
      this.afinidades.splice(index, 1);
      this.afinidadesId.splice(index, 1);
    }

  }
  deleteBeneficiario(index) {
    if (index > -1) {
      this.beneficiarios.splice(index, 1);
    }

  }
  deleteEstudio(index) {
    if (index > -1) {
      this.estudios.splice(index, 1);
      this.estudiosAll.splice(index, 1);
    }

  }
  deleteDocumento(index) {
    if (index > -1) {
      this.documentos.splice(index, 1);
      this.documentosId.splice(index, 1);
    }

  }
  addDocumento(
    tipodocumentosoporte,
    urldocumento) {
    this.DocumentoSer.addDocumento(
      tipodocumentosoporte,
      urldocumento);

  }
  nuevaCiudad() {
   const ciudad =  this.angForm.value['ciudad'];
   this.CiudadSer
   .addCiudad(ciudad, ciudad, ciudad )
   .subscribe((data) => {

    this.newciudad = data['id'];

   });
  }
  postMethod(files: FileList) {
    this.uploadFile();
    this.fileToUpload = files.item(0);
    const formData = new FormData();
    formData.append(`singleFile`, this.fileToUpload);

    this.http.post(`${environment.apiUrl}/upload`, formData).subscribe((val) => {
      this.archivo = val;
      this.route.params.subscribe(params => {

        this.DocumentoSer.addDocumento(this.formDocumento.value.items[0].tipodocumentosoporte._id, this.archivo.file)
          .subscribe((data) => {
            this.archivo2 = data;
            this.documentosId.push(this.archivo2.id);

          });

      });
      this.documentos.push({ url: this.archivo.file, nombre: this.formDocumento.value.items[0].tipodocumentosoporte.nombre });

      this.showFile();
      this.formDocumento.reset();
    });
    return false;
  }
}
