import { Component, OnInit } from '@angular/core';
import Tipocontacto from '../../models/Tipocontacto';
import { TipocontactoService } from '../../services/tipocontacto.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipocontacto-get',
  templateUrl: './tipocontacto-get.component.html',
  styleUrls: ['./tipocontacto-get.component.css']
})

export class TipocontactoGetComponent implements OnInit {

  tipocontacto: Tipocontacto[];

  constructor( private bs: TipocontactoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getTipocontacto()
      .subscribe((data: Tipocontacto[]) => {
        this.tipocontacto = data;
    });
  }

  deleteTipocontacto(id) {
    this.bs.deleteTipocontacto(id).subscribe(res => {

      this.bs
      .getTipocontacto()
      .subscribe((data: Tipocontacto[]) => {
        this.tipocontacto = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

