import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipocontactoService } from '../../services/tipocontacto.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipocontacto-edit',
  templateUrl: './tipocontacto-edit.component.html',
  styleUrls: ['./tipocontacto-edit.component.css']
})
export class TipocontactoEditComponent implements OnInit {
     angForm: FormGroup;
  tipocontacto: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: TipocontactoService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editTipocontacto(params['id']).subscribe(res => {
        this.tipocontacto = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateTipocontacto(nombre,
descripcion ) {
   this.route.params.subscribe(params => {
      this.bs.updateTipocontacto(nombre,
descripcion  , params['id']);
      this.showInfo();
      this.router.navigate(['tipocontacto']);
   });
}
}
