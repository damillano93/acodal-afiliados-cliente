import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipocontactoService } from '../../services/tipocontacto.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-tipocontacto-add',
  templateUrl: './tipocontacto-add.component.html',
  styleUrls: ['./tipocontacto-add.component.css']
})
export class TipocontactoAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Tipocontacto_ser: TipocontactoService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addTipocontacto(nombre,
descripcion ) {
    this.Tipocontacto_ser.addTipocontacto(nombre,
descripcion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
