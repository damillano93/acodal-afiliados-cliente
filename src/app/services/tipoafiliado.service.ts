import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class TipoafiliadoService {

  uri = `${environment.apiUrl}/tipoafiliado`;

  constructor(private http: HttpClient) { }
  addTipoafiliado(nombre ,
descripcion ,
estado ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
estado: estado,
abreviacion: abreviacion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getTipoafiliado() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editTipoafiliado(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateTipoafiliado(nombre ,
descripcion ,
estado ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
estado: estado,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteTipoafiliado(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
