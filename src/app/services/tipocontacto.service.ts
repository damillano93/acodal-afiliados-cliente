import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class TipocontactoService {

  uri = `${environment.apiUrl}/tipocontacto`;

  constructor(private http: HttpClient) { }
  addTipocontacto(nombre ,
descripcion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getTipocontacto() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editTipocontacto(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateTipocontacto(nombre ,
descripcion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteTipocontacto(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
