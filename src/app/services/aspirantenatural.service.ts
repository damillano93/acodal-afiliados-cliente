import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class AspirantenaturalService {

  uri = `${environment.apiUrl}/aspirantenatural`;

  constructor(private http: HttpClient) { }
  addAspirantenatural(fecha ,
nombres ,
apellidos ,
tipodocumento ,
documento ,
fechanacimiento ,
ciudad ,
direccion ,
telefono ,
celular ,
email ,
profesion ,
anogrado ,
matriculaprofesional ,
universidad ,
seccional ,
subafinidad,
abeasdata ,
lavadoactivos ) {
    const obj = {
      fecha: fecha,
nombres: nombres,
apellidos: apellidos,
tipodocumento: tipodocumento,
documento: documento,
fechanacimiento: fechanacimiento,
ciudad: ciudad,
direccion: direccion,
telefono: telefono,
celular: celular,
email: email,
profesion: profesion,
anogrado: anogrado,
matriculaprofesional: matriculaprofesional,
universidad: universidad,
seccional: seccional,
subafinidad: subafinidad,
abeasdata: abeasdata,
lavadoactivos: lavadoactivos

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getAspirantenatural() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editAspirantenatural(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateAspirantenatural(fecha ,
nombres ,
apellidos ,
tipodocumento ,
documento ,
fechanacimiento ,
ciudad ,
direccion ,
telefono ,
celular ,
email ,
profesion ,
anogrado ,
matriculaprofesional ,
universidad ,
seccional ,
abeasdata ,
lavadoactivos , id) {

    const obj = {
      fecha: fecha,
nombres: nombres,
apellidos: apellidos,
tipodocumento: tipodocumento,
documento: documento,
fechanacimiento: fechanacimiento,
ciudad: ciudad,
direccion: direccion,
telefono: telefono,
celular: celular,
email: email,
profesion: profesion,
anogrado: anogrado,
matriculaprofesional: matriculaprofesional,
universidad: universidad,
seccional: seccional,
abeasdata: abeasdata,
lavadoactivos: lavadoactivos

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteAspirantenatural(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
