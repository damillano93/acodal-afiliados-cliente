import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class ClasificacionService {

  uri = `${environment.apiUrl}/clasificacion`;

  constructor(private http: HttpClient) { }
  addClasificacion(nombre ,
descripcion ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getClasificacion() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editClasificacion(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateClasificacion(nombre ,
descripcion ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteClasificacion(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
