import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  uri = `${environment.apiUrl}/contacto`;

  constructor(private http: HttpClient) { }
  addContacto(afiliado ,
tipocontacto ,
valor ) {
    const obj = {
      afiliado: afiliado,
tipocontacto: tipocontacto,
valor: valor

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getContacto() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editContacto(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateContacto(afiliado ,
tipocontacto ,
valor , id) {

    const obj = {
      afiliado: afiliado,
tipocontacto: tipocontacto,
valor: valor

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteContacto(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
