import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class CiudadService {

  uri = `${environment.apiUrl}/ciudad`;

  constructor(private http: HttpClient) { }
  addCiudad(nombre ,
descripcion ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

    };
   return  this.http.post(`${this.uri}`, obj);

  }
  getCiudad() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editCiudad(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateCiudad(nombre ,
descripcion ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteCiudad(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
