import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class TipoempresaService {

  uri = `${environment.apiUrl}/tipoempresa`;

  constructor(private http: HttpClient) { }
  addTipoempresa(nombre ,
descripcion ,
estado ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
estado: estado,
abreviacion: abreviacion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getTipoempresa() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editTipoempresa(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateTipoempresa(nombre ,
descripcion ,
estado ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
estado: estado,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteTipoempresa(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
