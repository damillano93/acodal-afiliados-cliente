import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class TipodocumentoService {

  uri = `${environment.apiUrl}/tipodocumento`;

  constructor(private http: HttpClient) { }
  addTipodocumento(nombre ,
descripcion ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getTipodocumento() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editTipodocumento(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateTipodocumento(nombre ,
descripcion ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteTipodocumento(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
