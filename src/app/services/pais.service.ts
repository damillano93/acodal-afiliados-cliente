import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class PaisService {

  uri = `${environment.apiUrl}/pais`;

  constructor(private http: HttpClient) { }
  addPais(nombre ,
ciudad ) {
    const obj = {
      nombre: nombre,
ciudad: ciudad
    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getPais() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editPais(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updatePais(nombre ,
ciudad , id) {

    const obj = {
      nombre: nombre,
      ciudad: ciudad

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deletePais(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
