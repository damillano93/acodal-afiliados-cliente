import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class TipodocumentosoporteService {

  uri = `${environment.apiUrl}/tipodocumentosoporte`;

  constructor(private http: HttpClient) { }
  addTipodocumentosoporte(nombre ,
tipo ) {
    const obj = {
      nombre: nombre,
tipo: tipo

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getTipodocumentosoporte() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editTipodocumentosoporte(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateTipodocumentosoporte(nombre ,
tipo , id) {

    const obj = {
      nombre: nombre,
tipo: tipo

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteTipodocumentosoporte(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
