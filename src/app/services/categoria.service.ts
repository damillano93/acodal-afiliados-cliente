import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  uri = `${environment.apiUrl}/categoria`;

  constructor(private http: HttpClient) { }
  addCategoria(nombre ,
descripcion ,
estado ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
estado: estado,
abreviacion: abreviacion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getCategoria() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editCategoria(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateCategoria(nombre ,
descripcion ,
estado ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
estado: estado,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteCategoria(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
