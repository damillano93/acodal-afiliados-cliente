import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class GrupoafinidadService {

  uri = `${environment.apiUrl}/grupoafinidad`;

  constructor(private http: HttpClient) { }
  addGrupoafinidad(nombre ,
descripcion ,
abreviacion ,
subafinidad ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion,
subafinidad: subafinidad

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getGrupoafinidad() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editGrupoafinidad(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateGrupoafinidad(nombre ,
descripcion ,
abreviacion ,
subafinidad , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion,
subafinidad: subafinidad

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteGrupoafinidad(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
