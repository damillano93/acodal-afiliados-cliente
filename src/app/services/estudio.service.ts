import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class EstudioService {

  uri = `${environment.apiUrl}/estudio`;
  res;
  constructor(private http: HttpClient) { }

  addEstudio(nivel ,
titulo ,
universidad ) {
    const obj = {
      nivel: nivel,
titulo: titulo,
universidad: universidad

    };
    return this.http.post(`${this.uri}`, obj);


  }
  getEstudio() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editEstudio(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateEstudio(nivel ,
titulo ,
universidad , id) {

    const obj = {
      nivel: nivel,
titulo: titulo,
universidad: universidad

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteEstudio(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
