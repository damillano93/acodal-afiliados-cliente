import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class AfiliadoService {

  uri = `${environment.apiUrl}/afiliado`;
  logurl = `${environment.apiUrl}/log`;
  user = JSON.parse(localStorage.getItem('currentUser'));

  constructor(private http: HttpClient) { }
  addLog(us, action) {
    const date = new Date();
    const obj = {
      user: us,
      fecha: date,
      accion: action

    };
    this.http.post(`${this.logurl}`, obj)
      .subscribe(res => console.log('Done'));
  }
  getLog() {
    return this
      .http
      .get(`${this.logurl}`);
  }
  addAfiliado(tipoafiliado,
    categoria,
    codigocliente,
    nombres,
    apellidos,
    clasificacion,
    tipodocumento,
    documento,
    ciiu,
    seccional,
    subafinidad,
    pais,
    ciudad,
    direccion,
    telefono,
    email,
    sitioweb,
    informaciondirectorio,
    fechanacimiento,
    fechavinculacion,
    estudio,
    docsoporte,
    representantelegal,
    beneficiarios) {
    const obj = {
      tipoafiliado: tipoafiliado,
      categoria: categoria,
      codigocliente: codigocliente,
      nombres: nombres,
      apellidos: apellidos,
      clasificacion: clasificacion,
      tipodocumento: tipodocumento,
      documento: documento,
      ciiu: ciiu,
      seccional: seccional,
      subafinidad: subafinidad,
      pais: pais,
      ciudad: ciudad,
      direccion: direccion,
      telefono: telefono,
      email: email,
      sitioweb: sitioweb,
      informaciondirectorio: informaciondirectorio,
      fechanacimiento: fechanacimiento,
      fechavinculacion: fechavinculacion,
      estudio: estudio,
      docsoporte: docsoporte,
      representantelegal: representantelegal,
      beneficiarios: beneficiarios,
      user: (JSON.parse(localStorage.getItem('currentUser'))).userId,
    };
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': (JSON.parse(localStorage.getItem('currentUser'))).token
    });
    const options = { headers: headers };
    this.addLog((JSON.parse(localStorage.getItem('currentUser'))).userId, 'POST afiliado');
    return this.http.post(`${this.uri}`, obj, options);

  }
  getAfiliado() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': (JSON.parse(localStorage.getItem('currentUser'))).token
    });
    const options = { headers: headers };
    this.addLog((JSON.parse(localStorage.getItem('currentUser'))).userId, 'GET afiliado');
    return this
      .http
      .get(`${this.uri}`, options);
  }
  editAfiliado(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': (JSON.parse(localStorage.getItem('currentUser'))).token
    });
    const options = { headers: headers };

    return this
      .http
      .get(`${this.uri}/${id}`, options);
  }
  updateAfiliado(tipoafiliado,
    categoria,
    codigocliente,
    nombres,
    apellidos,
    clasificacion,
    tipodocumento,
    documento,
    ciiu,
    seccional,
    subafinidad,
    pais,
    ciudad,
    direccion,
    telefono,
    email,
    sitioweb,
    informaciondirectorio,
    fechanacimiento,
    fechavinculacion,
    estudio,
    docsoporte,
    representantelegal,
    beneficiarios,
    estado,
    id) {

    const obj = {
      tipoafiliado: tipoafiliado,
      categoria: categoria,
      codigocliente: codigocliente,
      nombres: nombres,
      apellidos: apellidos,
      clasificacion: clasificacion,
      tipodocumento: tipodocumento,
      documento: documento,
      ciiu: ciiu,
      seccional: seccional,
      subafinidad: subafinidad,
      pais: pais,
      ciudad: ciudad,
      direccion: direccion,
      telefono: telefono,
      email: email,
      sitioweb: sitioweb,
      informaciondirectorio: informaciondirectorio,
      fechanacimiento: fechanacimiento,
      fechavinculacion: fechavinculacion,
      estudio: estudio,
      docsoporte: docsoporte,
      user: (JSON.parse(localStorage.getItem('currentUser'))).userId,
      representantelegal: representantelegal,
      beneficiarios: beneficiarios,
      estado: estado

    };
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': (JSON.parse(localStorage.getItem('currentUser'))).token
    });
    const options = { headers: headers };
    this.addLog((JSON.parse(localStorage.getItem('currentUser'))).userId, 'PUT afiliado');
    this
      .http
      .put(`${this.uri}/${id}`, obj, options)
      .subscribe(res => console.log('Done'));
  }
  deleteAfiliado(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-access-token': (JSON.parse(localStorage.getItem('currentUser'))).token
    });
    const options = { headers: headers };
    this.addLog((JSON.parse(localStorage.getItem('currentUser'))).userId, 'DELETE afiliado');
    return this
      .http
      .delete(`${this.uri}/${id}`, options);
  }
}
