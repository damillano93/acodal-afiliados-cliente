import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  uri = `${environment.apiUrl}/factura`;

  constructor(private http: HttpClient) { }
  addFactura({ afiliado,
    numero,
    v_pagado,
    fecha_pago,
    fecha,
    documento,
   estado }) {
    const obj = {
      afiliado,
      numero,
      v_pagado,
      fecha_pago,
      rc: Date.now(),
      fecha,
      documento,
      estado,

    };
    return this.http.post(`${this.uri}`, obj);

  }
  getFactura() {
    return this
      .http
      .get(`${this.uri}`);
  }
  getFacturaByAfiliado(id) {
    return this
      .http
      .get(`${this.uri}/afiliado/${id}`);
  }
  getFacturaById(id) {
    return this
      .http
      .get(`${this.uri}/${id}`);
  }
  editFactura(id) {
    return this
      .http
      .get(`${this.uri}/${id}`);
  }
  updateFactura({
    id,
    afiliado,
    numero,
    v_pagado,
    fecha_pago,
    rc,
    fecha,
    documento,
    estado }) {

    const obj = {
      id,
      afiliado,
      numero,
      v_pagado,
      fecha_pago,
      rc,
      fecha,
      documento,
      estado
    };
    return this
      .http
      .put(`${this.uri}/${id}`, obj);
  }
  deleteFactura(id) {
    return this
      .http
      .delete(`${this.uri}/${id}`);
  }

  uploadDocument(formData) {
   return this.http.post(`${environment.apiUrl}/upload`, formData);

  }
}
