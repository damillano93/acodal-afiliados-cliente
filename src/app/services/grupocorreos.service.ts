import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class GrupocorreosService {

  uri = `${environment.apiUrl}/grupocorreos`;

  constructor(private http: HttpClient) { }
  addGrupocorreos(nombre ,
descripcion ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getGrupocorreos() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editGrupocorreos(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateGrupocorreos(nombre ,
descripcion ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteGrupocorreos(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
