import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class AspirantejuridicoService {

  uri = `${environment.apiUrl}/aspirantejuridico`;

  constructor(private http: HttpClient) { }
  addAspirantejuridico(fecha ,
razonsocial ,
CIUU ,
Nit ,
direccion ,
telefono ,
email ,
ciudad ,
servicios ,
nombregerente ,
emailgerente ,
celulargerente ,
paginaweb ,
seccional ,
tipoempresa ,
subafinidad,
abeasdata ,
lavadoactivos ) {
    const obj = {
      fecha: fecha,
razonsocial: razonsocial,
CIUU: CIUU,
Nit: Nit,
direccion: direccion,
telefono: telefono,
email: email,
ciudad: ciudad,
servicios: servicios,
nombregerente: nombregerente,
emailgerente: emailgerente,
celulargerente: celulargerente,
paginaweb: paginaweb,
seccional: seccional,
tipoempresa: tipoempresa,
subafinidad: subafinidad,
abeasdata: abeasdata,
lavadoactivos: lavadoactivos

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getAspirantejuridico() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editAspirantejuridico(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateAspirantejuridico(fecha ,
razonsocial ,
CIUU ,
Nit ,
direccion ,
telefono ,
email ,
ciudad ,
servicios ,
nombregerente ,
emailgerente ,
celulargerente ,
paginaweb ,
seccional ,
tipoempresa ,
abeasdata ,
lavadoactivos , id) {

    const obj = {
      fecha: fecha,
razonsocial: razonsocial,
CIUU: CIUU,
Nit: Nit,
direccion: direccion,
telefono: telefono,
email: email,
ciudad: ciudad,
servicios: servicios,
nombregerente: nombregerente,
emailgerente: emailgerente,
celulargerente: celulargerente,
paginaweb: paginaweb,
seccional: seccional,
tipoempresa: tipoempresa,
abeasdata: abeasdata,
lavadoactivos: lavadoactivos

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteAspirantejuridico(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
