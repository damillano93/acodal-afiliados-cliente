import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/User';
import { environment } from '../env/env';
@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }
    uri = `${environment.apiUrl}/users`;

    getAll() {
        return this.http.get<User[]>(`/users`);
    }

    getById(id: number) {
        return this.http.get(`/users/` + id);
    }
    forgot(email: string) {
        return this.http.get(`${this.uri}/usuario/email/` + email);
    }

    register(user: User) {
        return this.http.post(`/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`/users/` + id);
    }

    addUser(user: User ) {
            const obj = {
                username: user.username,
                password: user.password,
                firstname: user.firstname,
                lastname: user.lastname,
                seccional: user.seccional,
                cargo: user.cargo,
                permisos: user.permisos

            };
      return      this.http.post(`${this.uri}/register`, obj);

          }
          getUser() {
            return this
                   .http
                   .get(`${this.uri}/usuario`);
          }
          editUser(id) {
            return this
                    .http
                    .get(`${this.uri}/usuario/${id}`);
            }
          updateUser(user , id) {

            const obj = {
                username: user.username,
                password: user.password,
                firstname: user.firstname,
                lastname: user.lastname,
                seccional: user.seccional,
                cargo: user.cargo,
                permisos: user.permisos

            };
            this
              .http
              .put(`${this.uri}/usuario/${id}`, obj)
              .subscribe(res => console.log('Done'));
          }
          restorePassword(password , id) {
            const obj = {
              password: password
            };
            return this
              .http
              .put(`${this.uri}/usuario/password/${id}`, obj);
          }
         deleteUser(id) {
            return this
                      .http
                      .delete(`${this.uri}/usuario/${id}`);
          }

}
