import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class DocumentoService {

  uri = `${environment.apiUrl}/documento`;

  constructor(private http: HttpClient) { }
  addDocumento(
tipodocumentosoporte ,
urldocumento ) {
    const obj = {

tipodocumentosoporte: tipodocumentosoporte,
urldocumento: urldocumento

    };
    return this.http.post(`${this.uri}`, obj);

  }
  getDocumento() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editDocumento(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateDocumento(afiliado ,
tipodocumentosoporte ,
urldocumento , id) {

    const obj = {
      afiliado: afiliado,
tipodocumentosoporte: tipodocumentosoporte,
urldocumento: urldocumento

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteDocumento(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
