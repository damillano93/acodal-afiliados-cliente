import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class SeccionalService {

  uri = `${environment.apiUrl}/seccional`;

  constructor(private http: HttpClient) { }
  addSeccional(nombre ,
descripcion ,
direccion ,
ciudad ,
cubrimiento ,
estado ,
telefono ,
email ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion,
ciudad: ciudad,
cubrimiento: cubrimiento,
estado: estado,
telefono: telefono,
email: email

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getSeccional() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editSeccional(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateSeccional(nombre ,
descripcion ,
direccion ,
ciudad ,
cubrimiento ,
estado ,
telefono ,
email , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion,
ciudad: ciudad,
cubrimiento: cubrimiento,
estado: estado,
telefono: telefono,
email: email

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteSeccional(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
