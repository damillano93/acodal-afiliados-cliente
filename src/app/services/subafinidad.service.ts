import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../env/env';
@Injectable({
  providedIn: 'root'
})
export class SubafinidadService {

  uri = `${environment.apiUrl}/subafinidad`;

  constructor(private http: HttpClient) { }
  addSubafinidad(nombre ,
descripcion ,
abreviacion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

    };
    return this.http.post(`${this.uri}`, obj);

  }
  getSubafinidad() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editSubafinidad(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateSubafinidad(nombre ,
descripcion ,
abreviacion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
abreviacion: abreviacion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteSubafinidad(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
