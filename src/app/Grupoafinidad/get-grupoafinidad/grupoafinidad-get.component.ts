import { Component, OnInit } from '@angular/core';
import Grupoafinidad from '../../models/Grupoafinidad';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-grupoafinidad-get',
  templateUrl: './grupoafinidad-get.component.html',
  styleUrls: ['./grupoafinidad-get.component.css']
})

export class GrupoafinidadGetComponent implements OnInit {

  grupoafinidad: Grupoafinidad[];

  constructor( private bs: GrupoafinidadService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getGrupoafinidad()
      .subscribe((data: Grupoafinidad[]) => {
        this.grupoafinidad = data;
    });
  }

  deleteGrupoafinidad(id) {
    this.bs.deleteGrupoafinidad(id).subscribe(res => {

      this.bs
      .getGrupoafinidad()
      .subscribe((data: Grupoafinidad[]) => {
        this.grupoafinidad = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

