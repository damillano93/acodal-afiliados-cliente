import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Subafinidad from '../../models/Subafinidad';

import { GrupoafinidadService } from '../../services/grupoafinidad.service';
import { SubafinidadService } from '../../services/subafinidad.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-grupoafinidad-add',
  templateUrl: './grupoafinidad-add.component.html',
  styleUrls: ['./grupoafinidad-add.component.css']
})
export class GrupoafinidadAddComponent implements OnInit {
   Subafinidad: Subafinidad[];
   subafinidades = [];
   subafinidadesId = [];
   angForm: FormGroup;
   angFormSubafinidad: FormGroup;
  constructor(
private SubafinidadSer: SubafinidadService,

   private fb: FormBuilder,
   private Grupoafinidad_ser: GrupoafinidadService,
   public toastr: ToastrManager) {
    this.createForm();
    this.createFormSubafinidad();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  createFormSubafinidad() {
    this.angFormSubafinidad = this.fb.group({
     nombreSub: ['', Validators.required ],
descripcionSub: ['', Validators.required ],
abreviacionSub: ['', Validators.required ]

    });
  }

  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
addSubafinidad(nombre,
  descripcion,
  abreviacion ) {
      this.SubafinidadSer.addSubafinidad(nombre,
  descripcion,
  abreviacion )  .subscribe((data) => {

    this.subafinidadesId.push(data['id']);

    this.subafinidades.push({
      _id: data['id'],
      nombre,
      descripcion,
      abreviacion
    });

    this.angFormSubafinidad.reset();

  });

  }
  addGrupoafinidad(nombre,
descripcion,
abreviacion ) {
    this.Grupoafinidad_ser.addGrupoafinidad(nombre,
descripcion,
abreviacion,
this.subafinidadesId );
    this.angForm.reset();
    this.subafinidades = [];
    this.subafinidadesId = [];
    this.showSuccess();
  }

  ngOnInit() {
    this.SubafinidadSer
.getSubafinidad()
.subscribe((data: Subafinidad[]) => {
this.Subafinidad = data;
});
  }
  deleteAfinidad(index) {
    if (index > -1) {
      this.subafinidades.splice(index, 1);
      this.subafinidadesId.splice(index, 1);
    }

  }
}
