import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Subafinidad from '../../models/Subafinidad';

import { GrupoafinidadService } from '../../services/grupoafinidad.service';
import { SubafinidadService } from '../../services/subafinidad.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-grupoafinidad-edit',
  templateUrl: './grupoafinidad-edit.component.html',
  styleUrls: ['./grupoafinidad-edit.component.css']
})
export class GrupoafinidadEditComponent implements OnInit {
  Subafinidad: Subafinidad[];
  subafinidades = [];
  subafinidadesId = [];
   angForm: FormGroup;
   angFormSubafinidad: FormGroup;
  grupoafinidad: any = {};

  constructor(private route: ActivatedRoute,
     private SubafinidadSer: SubafinidadService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: GrupoafinidadService,
    private fb: FormBuilder) {
      this.createForm();
      this.createFormSubafinidad();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }
    createFormSubafinidad() {
      this.angFormSubafinidad = this.fb.group({
       nombreSub: ['', Validators.required ],
  descripcionSub: ['', Validators.required ],
  abreviacionSub: ['', Validators.required ]

      });
    }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editGrupoafinidad(params['id']).subscribe(res => {

        this.grupoafinidad = res;
        this.subafinidades = this.grupoafinidad.subafinidad;
        for (let sub = 0; sub <  this.grupoafinidad.subafinidad.length; sub ++ ) {
          this.subafinidadesId[sub] = this.grupoafinidad.subafinidad[sub]._id;
        }

      });
    });
     this.SubafinidadSer
.getSubafinidad()
.subscribe((data: Subafinidad[]) => {
this.Subafinidad = data;
});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
addSubafinidad(nombre,
  descripcion,
  abreviacion ) {
      this.SubafinidadSer.addSubafinidad(nombre,
  descripcion,
  abreviacion )  .subscribe((data) => {

    this.subafinidadesId.push(data['id']);

    this.subafinidades.push({
      _id: data['id'],
      nombre,
      descripcion,
      abreviacion
    });

    this.angFormSubafinidad.reset();

  });

  }
  updateGrupoafinidad(nombre,
descripcion,
abreviacion,
subafinidad ) {
   this.route.params.subscribe(params => {
      this.bs.updateGrupoafinidad(nombre,
descripcion,
abreviacion,
this.subafinidadesId  , params['id']);
      this.showInfo();
      this.router.navigate(['grupoafinidad']);
   });
}
deleteAfinidad(index) {
  if (index > -1) {
    this.subafinidades.splice(index, 1);
    this.subafinidadesId.splice(index, 1);
  }

}

}
