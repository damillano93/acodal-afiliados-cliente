import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipodocumentoService } from '../../services/tipodocumento.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-tipodocumento-add',
  templateUrl: './tipodocumento-add.component.html',
  styleUrls: ['./tipodocumento-add.component.css']
})
export class TipodocumentoAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Tipodocumento_ser: TipodocumentoService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addTipodocumento(nombre,
descripcion,
abreviacion ) {
    this.Tipodocumento_ser.addTipodocumento(nombre,
descripcion,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
