import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipodocumentoService } from '../../services/tipodocumento.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipodocumento-edit',
  templateUrl: './tipodocumento-edit.component.html',
  styleUrls: ['./tipodocumento-edit.component.css']
})
export class TipodocumentoEditComponent implements OnInit {
     angForm: FormGroup;
  tipodocumento: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: TipodocumentoService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editTipodocumento(params['id']).subscribe(res => {
        this.tipodocumento = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateTipodocumento(nombre,
descripcion,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateTipodocumento(nombre,
descripcion,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['tipodocumento']);
   });
}
}
