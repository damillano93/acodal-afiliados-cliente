import { Component, OnInit } from '@angular/core';
import Tipodocumento from '../../models/Tipodocumento';
import { TipodocumentoService } from '../../services/tipodocumento.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipodocumento-get',
  templateUrl: './tipodocumento-get.component.html',
  styleUrls: ['./tipodocumento-get.component.css']
})

export class TipodocumentoGetComponent implements OnInit {

  tipodocumento: Tipodocumento[];

  constructor( private bs: TipodocumentoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getTipodocumento()
      .subscribe((data: Tipodocumento[]) => {
        this.tipodocumento = data;
    });
  }

  deleteTipodocumento(id) {
    this.bs.deleteTipodocumento(id).subscribe(res => {

      this.bs
      .getTipodocumento()
      .subscribe((data: Tipodocumento[]) => {
        this.tipodocumento = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

