import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { ClasificacionService } from '../../services/clasificacion.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-clasificacion-add',
  templateUrl: './clasificacion-add.component.html',
  styleUrls: ['./clasificacion-add.component.css']
})
export class ClasificacionAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Clasificacion_ser: ClasificacionService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addClasificacion(nombre,
descripcion,
abreviacion ) {
    this.Clasificacion_ser.addClasificacion(nombre,
descripcion,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
