import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { ClasificacionService } from '../../services/clasificacion.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-clasificacion-edit',
  templateUrl: './clasificacion-edit.component.html',
  styleUrls: ['./clasificacion-edit.component.css']
})
export class ClasificacionEditComponent implements OnInit {
     angForm: FormGroup;
  clasificacion: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: ClasificacionService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editClasificacion(params['id']).subscribe(res => {
        this.clasificacion = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateClasificacion(nombre,
descripcion,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateClasificacion(nombre,
descripcion,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['clasificacion']);
   });
}
}
