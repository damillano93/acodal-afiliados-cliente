import { Component, OnInit } from '@angular/core';
import Clasificacion from '../../models/Clasificacion';
import { ClasificacionService } from '../../services/clasificacion.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-clasificacion-get',
  templateUrl: './clasificacion-get.component.html',
  styleUrls: ['./clasificacion-get.component.css']
})

export class ClasificacionGetComponent implements OnInit {

  clasificacion: Clasificacion[];

  constructor( private bs: ClasificacionService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getClasificacion()
      .subscribe((data: Clasificacion[]) => {
        this.clasificacion = data;
    });
  }

  deleteClasificacion(id) {
    this.bs.deleteClasificacion(id).subscribe(res => {

      this.bs
      .getClasificacion()
      .subscribe((data: Clasificacion[]) => {
        this.clasificacion = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

