import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { UserafiliadoComponent } from './userafiliado/userafiliado.component';
import { SeccionalAddComponent } from './Seccional/add-seccional/seccional-add.component';
import { SeccionalEditComponent } from './Seccional/edit-seccional/seccional-edit.component';
import { SeccionalGetComponent } from './Seccional/get-seccional/seccional-get.component';
import { CategoriaAddComponent } from './Categoria/add-categoria/categoria-add.component';
import { CategoriaEditComponent } from './Categoria/edit-categoria/categoria-edit.component';
import { CategoriaGetComponent } from './Categoria/get-categoria/categoria-get.component';
import { TipoafiliadoAddComponent } from './Tipoafiliado/add-tipoafiliado/tipoafiliado-add.component';
import { TipoafiliadoEditComponent } from './Tipoafiliado/edit-tipoafiliado/tipoafiliado-edit.component';
import { TipoafiliadoGetComponent } from './Tipoafiliado/get-tipoafiliado/tipoafiliado-get.component';
import { TipoempresaAddComponent } from './Tipoempresa/add-tipoempresa/tipoempresa-add.component';
import { TipoempresaEditComponent } from './Tipoempresa/edit-tipoempresa/tipoempresa-edit.component';
import { TipoempresaGetComponent } from './Tipoempresa/get-tipoempresa/tipoempresa-get.component';
import { AspirantejuridicoAddComponent } from './Aspirantejuridico/add-aspirantejuridico/aspirantejuridico-add.component';
import { AspirantejuridicoEditComponent } from './Aspirantejuridico/edit-aspirantejuridico/aspirantejuridico-edit.component';
import { AspirantejuridicoGetComponent } from './Aspirantejuridico/get-aspirantejuridico/aspirantejuridico-get.component';
import { TipocontactoAddComponent } from './Tipocontacto/add-tipocontacto/tipocontacto-add.component';
import { TipocontactoEditComponent } from './Tipocontacto/edit-tipocontacto/tipocontacto-edit.component';
import { TipocontactoGetComponent } from './Tipocontacto/get-tipocontacto/tipocontacto-get.component';
import { ContactoAddComponent } from './Contacto/add-contacto/contacto-add.component';
import { ContactoEditComponent } from './Contacto/edit-contacto/contacto-edit.component';
import { ContactoGetComponent } from './Contacto/get-contacto/contacto-get.component';
import { CiudadAddComponent } from './Ciudad/add-ciudad/ciudad-add.component';
import { CiudadEditComponent } from './Ciudad/edit-ciudad/ciudad-edit.component';
import { CiudadGetComponent } from './Ciudad/get-ciudad/ciudad-get.component';
import { FacturaAddComponent } from './Factura/add-factura/factura-add.component';
import { FacturaEditComponent } from './Factura/edit-factura/factura-edit.component';
import { FacturaGetComponent } from './Factura/get-factura/factura-get.component';
import { FacturaDetailComponent } from './Factura/detail-factura/factura-detail.component';
import { UserGetComponent } from './register/get-user/user-get.component';
import { UserEditComponent } from './register/edit-user/user-edit.component';
import { GrupocorreosAddComponent } from './Grupocorreos/add-grupocorreos/grupocorreos-add.component';
import { GrupocorreosEditComponent } from './Grupocorreos/edit-grupocorreos/grupocorreos-edit.component';
import { GrupocorreosGetComponent } from './Grupocorreos/get-grupocorreos/grupocorreos-get.component';
import { TipodocumentoAddComponent } from './Tipodocumento/add-tipodocumento/tipodocumento-add.component';
import { TipodocumentoEditComponent } from './Tipodocumento/edit-tipodocumento/tipodocumento-edit.component';
import { TipodocumentoGetComponent } from './Tipodocumento/get-tipodocumento/tipodocumento-get.component';
import { TipodocumentosoporteAddComponent } from './Tipodocumentosoporte/add-tipodocumentosoporte/tipodocumentosoporte-add.component';
import { TipodocumentosoporteEditComponent } from './Tipodocumentosoporte/edit-tipodocumentosoporte/tipodocumentosoporte-edit.component';
import { TipodocumentosoporteGetComponent } from './Tipodocumentosoporte/get-tipodocumentosoporte/tipodocumentosoporte-get.component';
import { DocumentoAddComponent } from './Documento/add-documento/documento-add.component';
import { DocumentoEditComponent } from './Documento/edit-documento/documento-edit.component';
import { DocumentoGetComponent } from './Documento/get-documento/documento-get.component';
import { AspirantenaturalAddComponent } from './Aspirantenatural/add-aspirantenatural/aspirantenatural-add.component';
import { AspirantenaturalEditComponent } from './Aspirantenatural/edit-aspirantenatural/aspirantenatural-edit.component';
import { AspirantenaturalGetComponent } from './Aspirantenatural/get-aspirantenatural/aspirantenatural-get.component';
import { AfiliadoAddComponent } from './Afiliado/add-afiliado/afiliado-add.component';
import { AfiliadoEditComponent } from './Afiliado/edit-afiliado/afiliado-edit.component';
import { AfiliadoGetComponent } from './Afiliado/get-afiliado/afiliado-get.component';
import { AfiliadoGetFilterComponent } from './Afiliado/get-afiliado-filter/afiliado-get-filter.component';
import { AfiliadoDetailComponent } from './Afiliado/detail-afiliado/afiliado-detail.component';
import { GrupoafinidadAddComponent } from './Grupoafinidad/add-grupoafinidad/grupoafinidad-add.component';
import { GrupoafinidadEditComponent } from './Grupoafinidad/edit-grupoafinidad/grupoafinidad-edit.component';
import { GrupoafinidadGetComponent } from './Grupoafinidad/get-grupoafinidad/grupoafinidad-get.component';
import { SubafinidadAddComponent } from './Subafinidad/add-subafinidad/subafinidad-add.component';
import { SubafinidadEditComponent } from './Subafinidad/edit-subafinidad/subafinidad-edit.component';
import { SubafinidadGetComponent } from './Subafinidad/get-subafinidad/subafinidad-get.component';
import { ClasificacionAddComponent } from './Clasificacion/add-clasificacion/clasificacion-add.component';
import { ClasificacionEditComponent } from './Clasificacion/edit-clasificacion/clasificacion-edit.component';
import { ClasificacionGetComponent } from './Clasificacion/get-clasificacion/clasificacion-get.component';
import { LogGetComponent} from './Log/log-get.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ToastrModule } from 'ng6-toastr-notifications';
import { SidebarjsModule } from 'ng-sidebarjs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SeccionalService } from './services/seccional.service';
import { CategoriaService } from './services/categoria.service';
import { TipoafiliadoService } from './services/tipoafiliado.service';
import { TipoempresaService } from './services/tipoempresa.service';
import { AspirantejuridicoService } from './services/aspirantejuridico.service';
import { TipocontactoService } from './services/tipocontacto.service';
import { ContactoService } from './services/contacto.service';
import { CiudadService } from './services/ciudad.service';
import { FacturaService } from './services/factura.service';
import { GrupoafinidadService } from './services/grupoafinidad.service';
import { GrupocorreosService } from './services/grupocorreos.service';
import { TipodocumentoService } from './services/tipodocumento.service';
import { TipodocumentosoporteService } from './services/tipodocumentosoporte.service';
import { DocumentoService } from './services/documento.service';
import { AspirantenaturalService } from './services/aspirantenatural.service';
import { AfiliadoService } from './services/afiliado.service';

import { SubafinidadService } from './services/subafinidad.service';
import { UploadService } from './services/upload.service';
import { ClasificacionService } from './services/clasificacion.service';

// used to create fake backend
// import { fakeBackendProvider } from './_helpers';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService } from './services/alert.service';
import {AuthenticationService } from './services/authentication.service';
import {UserService } from './services/user.service';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { ForgotComponent } from './reset/forgot';
import { RestoreComponent } from './reset/restore';
// mport { FacturaComponent } from './Factura';
@NgModule({
  declarations: [
    AlertComponent,
    LoginComponent,
    RegisterComponent,
    ForgotComponent,
    RestoreComponent,
   // FacturaComponent,
     HomeComponent,
     UserComponent,
     UserafiliadoComponent,
     AdminComponent,
    AppComponent,
SeccionalAddComponent,
SeccionalGetComponent,
SeccionalEditComponent
,
CategoriaAddComponent,
CategoriaGetComponent,
CategoriaEditComponent
,
TipoafiliadoAddComponent,
TipoafiliadoGetComponent,
TipoafiliadoEditComponent
,
ClasificacionAddComponent,
ClasificacionGetComponent,
ClasificacionEditComponent
,
TipoempresaAddComponent,
TipoempresaGetComponent,
TipoempresaEditComponent
,
AspirantejuridicoAddComponent,
AspirantejuridicoGetComponent,
AspirantejuridicoEditComponent
,
TipocontactoAddComponent,
TipocontactoGetComponent,
TipocontactoEditComponent
,
ContactoAddComponent,
ContactoGetComponent,
ContactoEditComponent
,
CiudadAddComponent,
CiudadGetComponent,
CiudadEditComponent
,
FacturaAddComponent,
FacturaGetComponent,
FacturaEditComponent,
FacturaDetailComponent
,
UserGetComponent,
UserEditComponent
,
GrupocorreosAddComponent,
GrupocorreosGetComponent,
GrupocorreosEditComponent
,
TipodocumentoAddComponent,
TipodocumentoGetComponent,
TipodocumentoEditComponent
,
TipodocumentosoporteAddComponent,
TipodocumentosoporteGetComponent,
TipodocumentosoporteEditComponent
,
DocumentoAddComponent,
DocumentoGetComponent,
DocumentoEditComponent
,
AspirantenaturalAddComponent,
AspirantenaturalGetComponent,
AspirantenaturalEditComponent
,
AfiliadoAddComponent,
AfiliadoGetComponent,
AfiliadoEditComponent,
AfiliadoDetailComponent,
AfiliadoGetFilterComponent
,
GrupoafinidadAddComponent,
GrupoafinidadGetComponent,
GrupoafinidadEditComponent
,
SubafinidadAddComponent,
SubafinidadGetComponent,
SubafinidadEditComponent,
LogGetComponent
  ],
  imports: [

    NgbModule,
    SidebarjsModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    AngularMultiSelectModule,
    ToastrModule.forRoot()
  ],
  providers: [
    SeccionalService,
CategoriaService,
TipoafiliadoService,
TipoempresaService,
AspirantejuridicoService,
TipocontactoService,
ContactoService,
CiudadService,
FacturaService,
GrupoafinidadService,
SubafinidadService,
GrupocorreosService,
TipodocumentoService,
TipodocumentosoporteService,
DocumentoService,
AspirantenaturalService,
AfiliadoService,
UploadService,
ClasificacionService,
AuthGuard,
AlertService,
AuthenticationService,
UserService,
{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

// provider used to create fake backend
// fakeBackendProvider

    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
