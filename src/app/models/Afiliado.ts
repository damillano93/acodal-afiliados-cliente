
export default class Afiliado {
  // tipoafiliado:Tipoafiliado[];
// categoria:Categoria[];
codigocliente: String;
nombres: String;
apellidos: String;
// clasificacion:Clasificacion[];
// tipodocumento:Tipodocumento[];
documento: String;
ciiu: String;
// seccional:Seccional[];
// subafinidad:Subafinidad[];
pais: String;
// ciudad:Ciudad[];
direccion: String;
telefono: String;
email: String;
sitioweb: String;
informaciondirectorio: String;
fechanacimiento: String;
 }
