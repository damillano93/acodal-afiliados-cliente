import { Component, OnInit } from '@angular/core';
import Tipodocumentosoporte from '../../models/Tipodocumentosoporte';
import { TipodocumentosoporteService } from '../../services/tipodocumentosoporte.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipodocumentosoporte-get',
  templateUrl: './tipodocumentosoporte-get.component.html',
  styleUrls: ['./tipodocumentosoporte-get.component.css']
})

export class TipodocumentosoporteGetComponent implements OnInit {

  tipodocumentosoporte: Tipodocumentosoporte[];

  constructor( private bs: TipodocumentosoporteService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getTipodocumentosoporte()
      .subscribe((data: Tipodocumentosoporte[]) => {
        this.tipodocumentosoporte = data;
    });
  }

  deleteTipodocumentosoporte(id) {
    this.bs.deleteTipodocumentosoporte(id).subscribe(res => {

      this.bs
      .getTipodocumentosoporte()
      .subscribe((data: Tipodocumentosoporte[]) => {
        this.tipodocumentosoporte = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

