import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipodocumentosoporteService } from '../../services/tipodocumentosoporte.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipodocumentosoporte-edit',
  templateUrl: './tipodocumentosoporte-edit.component.html',
  styleUrls: ['./tipodocumentosoporte-edit.component.css']
})
export class TipodocumentosoporteEditComponent implements OnInit {
     angForm: FormGroup;
  tipodocumentosoporte: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: TipodocumentosoporteService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
tipo: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editTipodocumentosoporte(params['id']).subscribe(res => {
        this.tipodocumentosoporte = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateTipodocumentosoporte(nombre,
tipo ) {
   this.route.params.subscribe(params => {
      this.bs.updateTipodocumentosoporte(nombre,
tipo  , params['id']);
      this.showInfo();
      this.router.navigate(['tipodocumentosoporte']);
   });
}
}
