import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipodocumentosoporteService } from '../../services/tipodocumentosoporte.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-tipodocumentosoporte-add',
  templateUrl: './tipodocumentosoporte-add.component.html',
  styleUrls: ['./tipodocumentosoporte-add.component.css']
})
export class TipodocumentosoporteAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Tipodocumentosoporte_ser: TipodocumentosoporteService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
tipo: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addTipodocumentosoporte(nombre,
tipo ) {
    this.Tipodocumentosoporte_ser.addTipodocumentosoporte(nombre,
tipo );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
