import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Seccional from '../../models/Seccional';
import { UserService } from '../../services/user.service';
@Component({templateUrl: 'restore.component.html'})
export class RestoreComponent implements OnInit {
    restoreForm: FormGroup;
    isCorrect: Boolean;
    isFalse: Boolean;
    nombre = '';
    error = '';
    loading = false;
    submitted = false;
    match = true;
    Seccional: Seccional[];
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService) { }


    ngOnInit() {
        this.isCorrect = true;
        this.isFalse = true;
        this.match = true;
        this.restoreForm = this.formBuilder.group({
            password: ['', Validators.required],
            password2: ''
        });

    }

    // convenience getter for easy access to form fields
    get f() { return this.restoreForm.controls; }

    onSubmit() {
        if (!this.match) {
        const password = this.restoreForm.value['password'];
        this.route.params.subscribe(params => {
        this.userService.restorePassword(password, params['id']).subscribe(res => {

            if ( res && res['message']) {
                this.isCorrect = false ;
                this.isFalse = true ;
                this.restoreForm.reset();
                this.match = true;
            } else {
                this.isCorrect = true ;
                this.isFalse = false ;
            }
          });
        });
    } else {
        this.isCorrect = true ;
        this.isFalse = false ;
    }
}
    changePass(pass) {

        if (pass !== this.restoreForm.value.password) {
          this.match = true;
        } else {
         this.match = false;
        }
     }
}

