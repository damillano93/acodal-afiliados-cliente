import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Seccional from '../../models/Seccional';
import { UserService } from '../../services/user.service';
@Component({templateUrl: 'forgot.component.html'})
export class ForgotComponent implements OnInit {
    forgotForm: FormGroup;
    isCorrect: Boolean;
    isFalse: Boolean;
    nombre = '';
    error = '';
    Seccional: Seccional[];
    constructor(
        private formBuilder: FormBuilder,
        private userService: UserService) { }

    ngOnInit() {
        this.isCorrect = true;
        this.isFalse = true;
        this.forgotForm = this.formBuilder.group({
            email: ['', Validators.required]
        });

    }

    // convenience getter for easy access to form fields
    get f() { return this.forgotForm.controls; }

    onSubmit() {
        const email = this.forgotForm.value['email'];
        this.userService.forgot(email).subscribe(res => {
            if (res && res['firstname']) {
                this.isCorrect = false ;
                this.isFalse = true ;
                this.nombre = res['firstname'];
                this.forgotForm.reset();
            } else {
                this.isCorrect = true ;
                this.isFalse = false ;
            }
          });
    }
}
