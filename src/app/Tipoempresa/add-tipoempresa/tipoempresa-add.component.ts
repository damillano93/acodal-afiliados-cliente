import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipoempresaService } from '../../services/tipoempresa.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-tipoempresa-add',
  templateUrl: './tipoempresa-add.component.html',
  styleUrls: ['./tipoempresa-add.component.css']
})
export class TipoempresaAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Tipoempresa_ser: TipoempresaService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addTipoempresa(nombre,
descripcion,
estado,
abreviacion ) {
    this.Tipoempresa_ser.addTipoempresa(nombre,
descripcion,
estado,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
