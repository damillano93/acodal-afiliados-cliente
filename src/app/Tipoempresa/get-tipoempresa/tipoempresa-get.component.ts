import { Component, OnInit } from '@angular/core';
import Tipoempresa from '../../models/Tipoempresa';
import { TipoempresaService } from '../../services/tipoempresa.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipoempresa-get',
  templateUrl: './tipoempresa-get.component.html',
  styleUrls: ['./tipoempresa-get.component.css']
})

export class TipoempresaGetComponent implements OnInit {

  tipoempresa: Tipoempresa[];

  constructor( private bs: TipoempresaService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getTipoempresa()
      .subscribe((data: Tipoempresa[]) => {
        this.tipoempresa = data;
    });
  }

  deleteTipoempresa(id) {
    this.bs.deleteTipoempresa(id).subscribe(res => {

      this.bs
      .getTipoempresa()
      .subscribe((data: Tipoempresa[]) => {
        this.tipoempresa = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

