import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipoempresaService } from '../../services/tipoempresa.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipoempresa-edit',
  templateUrl: './tipoempresa-edit.component.html',
  styleUrls: ['./tipoempresa-edit.component.css']
})
export class TipoempresaEditComponent implements OnInit {
     angForm: FormGroup;
  tipoempresa: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: TipoempresaService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editTipoempresa(params['id']).subscribe(res => {
        this.tipoempresa = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateTipoempresa(nombre,
descripcion,
estado,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateTipoempresa(nombre,
descripcion,
estado,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['tipoempresa']);
   });
}
}
