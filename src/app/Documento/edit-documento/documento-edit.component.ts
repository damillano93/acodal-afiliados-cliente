import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Afiliado from '../../models/Afiliado';
import Tipodocumentosoporte from '../../models/Tipodocumentosoporte';

import { DocumentoService } from '../../services/documento.service';
import { AfiliadoService } from '../../services/afiliado.service';
import { TipodocumentosoporteService } from '../../services/tipodocumentosoporte.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-documento-edit',
  templateUrl: './documento-edit.component.html',
  styleUrls: ['./documento-edit.component.css']
})
export class DocumentoEditComponent implements OnInit {
  Afiliado: Afiliado[];
Tipodocumentosoporte: Tipodocumentosoporte[];
   angForm: FormGroup;
  documento: any = {};

  constructor(private route: ActivatedRoute, private AfiliadoSer: AfiliadoService,
 private TipodocumentosoporteSer: TipodocumentosoporteService,

    private router: Router,
    public toastr: ToastrManager,
    private bs: DocumentoService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        afiliado: ['', Validators.required ],
tipodocumentosoporte: ['', Validators.required ],
urldocumento: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editDocumento(params['id']).subscribe(res => {
        this.documento = res;
      });
    });
     this.AfiliadoSer
.getAfiliado()
.subscribe((data: Afiliado[]) => {
this.Afiliado = data;
});
this.TipodocumentosoporteSer
.getTipodocumentosoporte()
.subscribe((data: Tipodocumentosoporte[]) => {
this.Tipodocumentosoporte = data;
});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateDocumento(afiliado,
tipodocumentosoporte,
urldocumento ) {
   this.route.params.subscribe(params => {
      this.bs.updateDocumento(afiliado,
tipodocumentosoporte,
urldocumento  , params['id']);
      this.showInfo();
      this.router.navigate(['documento']);
   });
}
}
