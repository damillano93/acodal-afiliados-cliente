import { Component, OnInit } from '@angular/core';
import Documento from '../../models/Documento';
import { DocumentoService } from '../../services/documento.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-documento-get',
  templateUrl: './documento-get.component.html',
  styleUrls: ['./documento-get.component.css']
})

export class DocumentoGetComponent implements OnInit {

  documento: Documento[];

  constructor( private bs: DocumentoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getDocumento()
      .subscribe((data: Documento[]) => {
        this.documento = data;
    });
  }

  deleteDocumento(id) {
    this.bs.deleteDocumento(id).subscribe(res => {

      this.bs
      .getDocumento()
      .subscribe((data: Documento[]) => {
        this.documento = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

