import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Afiliado from '../../models/Afiliado';
import Tipodocumentosoporte from '../../models/Tipodocumentosoporte';

import { DocumentoService } from '../../services/documento.service';
import { AfiliadoService } from '../../services/afiliado.service';
import { TipodocumentosoporteService } from '../../services/tipodocumentosoporte.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-documento-add',
  templateUrl: './documento-add.component.html',
  styleUrls: ['./documento-add.component.css']
})
export class DocumentoAddComponent implements OnInit {
   Afiliado: Afiliado[];
Tipodocumentosoporte: Tipodocumentosoporte[];
   angForm: FormGroup;
  constructor(
private AfiliadoSer: AfiliadoService,
private TipodocumentosoporteSer: TipodocumentosoporteService,

   private fb: FormBuilder,
   private Documento_ser: DocumentoService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       afiliado: ['', Validators.required ],
tipodocumentosoporte: ['', Validators.required ],
urldocumento: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addDocumento(
tipodocumentosoporte,
urldocumento ) {
    this.Documento_ser.addDocumento(
tipodocumentosoporte,
urldocumento );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.AfiliadoSer
.getAfiliado()
.subscribe((data: Afiliado[]) => {
this.Afiliado = data;
});
this.TipodocumentosoporteSer
.getTipodocumentosoporte()
.subscribe((data: Tipodocumentosoporte[]) => {
this.Tipodocumentosoporte = data;
});
  }

}
