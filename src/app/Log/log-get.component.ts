import { Component, OnInit } from '@angular/core';
import { AfiliadoService } from '../services/afiliado.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import Logg from '../models/Log';
@Component({
  selector: 'app-log-get',
  templateUrl: './log-get.component.html',
  styleUrls: ['./log-get.component.css']
})

export class LogGetComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  myTable: Boolean;
  // ciudad: Ciudad[];
  log: Logg[];
  constructor( private bs: AfiliadoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.myTable = false;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[1, 'asc']],
      responsive: true,
  /* below is the relevant part, e.g. translated to spanish */
  language: {
    processing: 'Procesando...',
    search: 'Buscar:',
    lengthMenu: 'Mostrar _MENU_ elementos',
    info: 'Mostrando desde _START_ al _END_ de _TOTAL_ elementos',
    infoEmpty: 'Mostrando ningún elemento.',
    infoFiltered: '(filtrado _MAX_ elementos total)',
    infoPostFix: '',
    loadingRecords: 'Cargando registros...',
    zeroRecords: 'No se encontraron registros',
    emptyTable: 'No hay datos disponibles en la tabla',
    'paginate': {
      'first': '<i class="fa fa-fast-backward" aria-hidden="true"></i>',
      'last': '<i class="fa fa-fast-forward" aria-hidden="true"></i>',
      'next': '<i class="fa fa-step-forward large" aria-hidden="true"></i>',
      'previous': '<i class="fa fa-step-backward" aria-hidden="true"></i>'
  },
    aria: {
      sortAscending: ': Activar para ordenar la tabla en orden ascendente',
      sortDescending: ': Activar para ordenar la tabla en orden descendente'
    }
  }
    };

    this.bs
      .getLog()
      .subscribe((data: Logg[]) => {
        this.log = data;

         this.myTable = true;
    });
  }



}

