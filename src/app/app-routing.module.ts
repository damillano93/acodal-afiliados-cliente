import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { UserafiliadoComponent } from './userafiliado/userafiliado.component';
import { SeccionalAddComponent } from './Seccional/add-seccional/seccional-add.component';
import { SeccionalEditComponent } from './Seccional/edit-seccional/seccional-edit.component';
import { SeccionalGetComponent } from './Seccional/get-seccional/seccional-get.component';
import { CategoriaAddComponent } from './Categoria/add-categoria/categoria-add.component';
import { CategoriaEditComponent } from './Categoria/edit-categoria/categoria-edit.component';
import { CategoriaGetComponent } from './Categoria/get-categoria/categoria-get.component';
import { TipoafiliadoAddComponent } from './Tipoafiliado/add-tipoafiliado/tipoafiliado-add.component';
import { TipoafiliadoEditComponent } from './Tipoafiliado/edit-tipoafiliado/tipoafiliado-edit.component';
import { TipoafiliadoGetComponent } from './Tipoafiliado/get-tipoafiliado/tipoafiliado-get.component';
import { TipoempresaAddComponent } from './Tipoempresa/add-tipoempresa/tipoempresa-add.component';
import { TipoempresaEditComponent } from './Tipoempresa/edit-tipoempresa/tipoempresa-edit.component';
import { TipoempresaGetComponent } from './Tipoempresa/get-tipoempresa/tipoempresa-get.component';
import { AspirantejuridicoAddComponent } from './Aspirantejuridico/add-aspirantejuridico/aspirantejuridico-add.component';
import { AspirantejuridicoEditComponent } from './Aspirantejuridico/edit-aspirantejuridico/aspirantejuridico-edit.component';
import { AspirantejuridicoGetComponent } from './Aspirantejuridico/get-aspirantejuridico/aspirantejuridico-get.component';
import { TipocontactoAddComponent } from './Tipocontacto/add-tipocontacto/tipocontacto-add.component';
import { TipocontactoEditComponent } from './Tipocontacto/edit-tipocontacto/tipocontacto-edit.component';
import { TipocontactoGetComponent } from './Tipocontacto/get-tipocontacto/tipocontacto-get.component';
import { ContactoAddComponent } from './Contacto/add-contacto/contacto-add.component';
import { ContactoEditComponent } from './Contacto/edit-contacto/contacto-edit.component';
import { ContactoGetComponent } from './Contacto/get-contacto/contacto-get.component';
import { CiudadAddComponent } from './Ciudad/add-ciudad/ciudad-add.component';
import { CiudadEditComponent } from './Ciudad/edit-ciudad/ciudad-edit.component';
import { CiudadGetComponent } from './Ciudad/get-ciudad/ciudad-get.component';
import { UserGetComponent } from './register/get-user/user-get.component';
import { UserEditComponent } from './register/edit-user/user-edit.component';
import { GrupocorreosAddComponent } from './Grupocorreos/add-grupocorreos/grupocorreos-add.component';
import { GrupocorreosEditComponent } from './Grupocorreos/edit-grupocorreos/grupocorreos-edit.component';
import { GrupocorreosGetComponent } from './Grupocorreos/get-grupocorreos/grupocorreos-get.component';
import { TipodocumentoAddComponent } from './Tipodocumento/add-tipodocumento/tipodocumento-add.component';
import { TipodocumentoEditComponent } from './Tipodocumento/edit-tipodocumento/tipodocumento-edit.component';
import { TipodocumentoGetComponent } from './Tipodocumento/get-tipodocumento/tipodocumento-get.component';
import { TipodocumentosoporteAddComponent } from './Tipodocumentosoporte/add-tipodocumentosoporte/tipodocumentosoporte-add.component';
import { TipodocumentosoporteEditComponent } from './Tipodocumentosoporte/edit-tipodocumentosoporte/tipodocumentosoporte-edit.component';
import { TipodocumentosoporteGetComponent } from './Tipodocumentosoporte/get-tipodocumentosoporte/tipodocumentosoporte-get.component';
import { DocumentoAddComponent } from './Documento/add-documento/documento-add.component';
import { DocumentoEditComponent } from './Documento/edit-documento/documento-edit.component';
import { DocumentoGetComponent } from './Documento/get-documento/documento-get.component';
import { AspirantenaturalAddComponent } from './Aspirantenatural/add-aspirantenatural/aspirantenatural-add.component';
import { AspirantenaturalEditComponent } from './Aspirantenatural/edit-aspirantenatural/aspirantenatural-edit.component';
import { AspirantenaturalGetComponent } from './Aspirantenatural/get-aspirantenatural/aspirantenatural-get.component';
import { AfiliadoAddComponent } from './Afiliado/add-afiliado/afiliado-add.component';
import { AfiliadoEditComponent } from './Afiliado/edit-afiliado/afiliado-edit.component';
import { AfiliadoGetComponent } from './Afiliado/get-afiliado/afiliado-get.component';
import { AfiliadoGetFilterComponent } from './Afiliado/get-afiliado-filter/afiliado-get-filter.component';
import { AfiliadoDetailComponent } from './Afiliado/detail-afiliado/afiliado-detail.component';
import { GrupoafinidadEditComponent } from './Grupoafinidad/edit-grupoafinidad/grupoafinidad-edit.component';
import { GrupoafinidadGetComponent } from './Grupoafinidad/get-grupoafinidad/grupoafinidad-get.component';
import { GrupoafinidadAddComponent } from './Grupoafinidad/add-grupoafinidad/grupoafinidad-add.component';
import { SubafinidadEditComponent } from './Subafinidad/edit-subafinidad/subafinidad-edit.component';
import { SubafinidadGetComponent } from './Subafinidad/get-subafinidad/subafinidad-get.component';
import { SubafinidadAddComponent } from './Subafinidad/add-subafinidad/subafinidad-add.component';
import { ClasificacionAddComponent } from './Clasificacion/add-clasificacion/clasificacion-add.component';
import { ClasificacionEditComponent } from './Clasificacion/edit-clasificacion/clasificacion-edit.component';
import { ClasificacionGetComponent } from './Clasificacion/get-clasificacion/clasificacion-get.component';
import {LogGetComponent} from './Log/log-get.component';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { ForgotComponent } from './reset/forgot';
import { RestoreComponent } from './reset/restore';
import { AuthGuard } from './_guards';
import { FacturaAddComponent } from './Factura/add-factura/factura-add.component';
import { FacturaEditComponent } from './Factura/edit-factura/factura-edit.component';
import { FacturaGetComponent } from './Factura/get-factura/factura-get.component';
import { FacturaDetailComponent } from './Factura/detail-factura/factura-detail.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard]  },
  { path: 'login', component: LoginComponent   },
  { path: 'register', component: RegisterComponent  },
  { path: 'forgot', component: ForgotComponent  },
  { path: 'restore/:id', component: RestoreComponent  },
 // { path: 'factura', component: FacturaComponent  },

{
  path: 'admin',
  component: AdminComponent
, canActivate: [AuthGuard]  },
{
  path: 'user',
  component: UserComponent
, canActivate: [AuthGuard]  },
{
  path: 'userafiliado',
  component: UserafiliadoComponent
, canActivate: [AuthGuard]  },
{
  path: 'log',
  component: LogGetComponent
, canActivate: [AuthGuard]  },
  {
  path: 'seccional/create',
  component: SeccionalAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'seccional/edit/:id',
  component: SeccionalEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'seccional',
  component: SeccionalGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'categoria/create',
  component: CategoriaAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'categoria/edit/:id',
  component: CategoriaEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'categoria',
  component: CategoriaGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipoafiliado/create',
  component: TipoafiliadoAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipoafiliado/edit/:id',
  component: TipoafiliadoEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipoafiliado',
  component: TipoafiliadoGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipoempresa/create',
  component: TipoempresaAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipoempresa/edit/:id',
  component: TipoempresaEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipoempresa',
  component: TipoempresaGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'aspirantejuridico/create',
  component: AspirantejuridicoAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'aspirantejuridico/edit/:id',
  component: AspirantejuridicoEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'aspirantejuridico',
  component: AspirantejuridicoGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipocontacto/create',
  component: TipocontactoAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipocontacto/edit/:id',
  component: TipocontactoEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipocontacto',
  component: TipocontactoGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'contacto/create',
  component: ContactoAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'contacto/edit/:id',
  component: ContactoEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'contacto',
  component: ContactoGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'ciudad/create',
  component: CiudadAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'ciudad/edit/:id',
  component: CiudadEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'ciudad',
  component: CiudadGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'factura/create',
  component: FacturaAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'factura/edit/:id',
  component: FacturaEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'factura/detail/:id',
  component: FacturaDetailComponent
, canActivate: [AuthGuard]  },
{
  path: 'factura',
  component: FacturaGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'grupocorreos/create',
  component: GrupocorreosAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'grupocorreos/edit/:id',
  component: GrupocorreosEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'grupocorreos',
  component: GrupocorreosGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipodocumento/create',
  component: TipodocumentoAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipodocumento/edit/:id',
  component: TipodocumentoEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipodocumento',
  component: TipodocumentoGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipodocumentosoporte/create',
  component: TipodocumentosoporteAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipodocumentosoporte/edit/:id',
  component: TipodocumentosoporteEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'tipodocumentosoporte',
  component: TipodocumentosoporteGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'documento/create',
  component: DocumentoAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'documento/edit/:id',
  component: DocumentoEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'documento',
  component: DocumentoGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'aspirantenatural/create',
  component: AspirantenaturalAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'aspirantenatural/edit/:id',
  component: AspirantenaturalEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'aspirantenatural',
  component: AspirantenaturalGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'afiliado/create',
  component: AfiliadoAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'afiliado/edit/:id',
  component: AfiliadoEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'afiliado',
  component: AfiliadoGetComponent
  , canActivate: [AuthGuard]
}
,
{
  path: 'afiliadofiltro',
  component: AfiliadoGetFilterComponent
  , canActivate: [AuthGuard]
}
,
{
  path: 'afiliado/detail/:id',
  component: AfiliadoDetailComponent
, canActivate: [AuthGuard]  },
{
  path: 'grupoafinidad/edit/:id',
  component: GrupoafinidadEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'grupoafinidad',
  component: GrupoafinidadGetComponent
}
,
{
  path: 'grupoafinidad/create',
  component: GrupoafinidadAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'subafinidad/edit/:id',
  component: SubafinidadEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'subafinidad',
  component: SubafinidadGetComponent
}
,
{
  path: 'subafinidad/create',
  component: SubafinidadAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'clasificacion/create',
  component: ClasificacionAddComponent
, canActivate: [AuthGuard]  },
{
  path: 'clasificacion/edit/:id',
  component: ClasificacionEditComponent
, canActivate: [AuthGuard]  },
{
  path: 'clasificacion',
  component: ClasificacionGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'usuarios',
  component: UserGetComponent
, canActivate: [AuthGuard]  },
{
  path: 'usuarios/edit/:id',
  component: UserEditComponent
, canActivate: [AuthGuard]  },

      // otherwise redirect to home
      { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
