import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FacturaService } from '../../services/factura.service';
import { AfiliadoService } from '../../services/afiliado.service';
import { SeccionalService } from '../../services/seccional.service';
import { TipoafiliadoService } from '../../services/tipoafiliado.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import Afiliado from '../../models/Afiliado';
@Component({
  selector: 'app-factura-edit',
  templateUrl: './factura-edit.component.html',
  styleUrls: ['./factura-edit.component.css']
})
export class FacturaEditComponent implements OnInit {
  angForm: FormGroup;
  documentForm: FormGroup;
  afiliados: Afiliado[];
  afiliadosFilter: Afiliado[];
  seccionales: Object;
  tiposAfiliado: Object;
  tipoAfiliado: String;
  urlDocumento: String[];
  todosDocumento = [];
  seccional: String;
  fileToUpload: File = null;
  factura: Object;
  idFactura: String;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private Factura_ser: FacturaService,
    private AfiliadoSer: AfiliadoService,
    private TipoafiliadoSer: TipoafiliadoService,
    private SeccionalSer: SeccionalService,
    public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      seccional: ['', Validators.required],
      afiliado: ['', Validators.required],
      numero: ['', Validators.required],
      v_pagado: ['', Validators.required],
      fecha_pago: ['', Validators.required],
      estado: ['', Validators.required]

    });
    this.documentForm =  this.fb.group({
      file: ['', Validators.required],
      detalle: ['', Validators.required],
    });
  }
  ngOnInit() {
    this.getAfiliados();
    this.getSeccionales();
    this.getTiposAfiliado();
    this.seccional = '';
    this.tipoAfiliado = '';
    this.urlDocumento = [];
    this.afiliadosFilter = this.afiliados;
  }
  getAfiliados() {
    this.AfiliadoSer.getAfiliado()
      .subscribe((data: Afiliado[]) => {
        this.afiliados = data;
        this.afiliadosFilter = this.afiliados;
        this.route.params.subscribe(params => {
          this.idFactura = params['id'];
          this.Factura_ser.editFactura(params['id']).subscribe(res => {
            this.factura = res;
            this.urlDocumento = this.factura['documento'];
            this.todosDocumento = JSON.parse('[' + this.urlDocumento + ']');


          });
        });
      });
  }
  getSeccionales() {
    this.SeccionalSer.getSeccional()
      .subscribe((data) => {
        this.seccionales = data;
      });
  }
  getTiposAfiliado() {
    this.TipoafiliadoSer.getTipoafiliado()
      .subscribe((data) => {
       this.tiposAfiliado = data;
      });

  }
  changeSeccional(data) {
    this.afiliadosFilter = this.afiliados.filter(afiliado => afiliado['seccional'][0]._id === data);
  }
  editFactura(value) {

      value['documento'] = this.urlDocumento || [];
      value['id'] = this.idFactura || '';

     this.Factura_ser.updateFactura(value)
     .subscribe((data) => {
     if (data['status'] === 'updated') {
       this.showSuccess();
       this.angForm.reset();
       this.urlDocumento = [];
       this.router.navigate(['factura']);
     } else {
       this.showError();
     }
     });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue actualizado.', 'Creado!');
  }
  showError() {
    this.toastr.errorToastr('Hubo un error en la actualizacion de la factura intente de nuevo', 'Error');
  }
  postMethod(files: FileList) {
    let {detalle} =  this.documentForm.value;
    if (!detalle) {
      detalle = 'nulo';
    }
    this.uploadFile();
    this.fileToUpload = files.item(0);
    const formData = new FormData();
    formData.append(`singleFile`, this.fileToUpload);
   this.Factura_ser.uploadDocument(formData)
   .subscribe((data) => {
    if (data['file']) {
      this.urlDocumento.push(JSON.stringify({file: data['file'], detalle}));
      this.todosDocumento.push( {file: data['file'], detalle});
      this.showSuccess();
     // this.angForm.reset()
    } else {
      this.urlDocumento = [];
      this.todosDocumento = [];
      this.showError();
    }



   });

    return false;
  }
  uploadFile() {
    this.toastr.customToastr(

      `<div class="center">
      <h3 class="center" style='color:black;'>
      Subiendo
      <img class="center" width="40px" height="40px"
      src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///
      wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/
      hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+
      QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnA
      dOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQ
      JCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGB
      MRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujI
      jK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQo
      AAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDg
      sGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIp
      z8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradyleso
      jEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WG
      c3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDI
      I7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
      </h3>

    </div>`,
      null,
      {
        enableHTML: true,
        position: 'top-center',
        toastTimeout: 2000
      }
    );
  }
  deleteDocumento(index) {
    if (index > -1) {
      this.urlDocumento.splice(index, 1);
      this.todosDocumento.splice(index, 1);
  }
}
}

