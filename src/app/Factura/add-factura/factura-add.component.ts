import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { FacturaService } from '../../services/factura.service';
import { AfiliadoService } from '../../services/afiliado.service';
import { SeccionalService } from '../../services/seccional.service';
import { TipoafiliadoService } from '../../services/tipoafiliado.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import Afiliado from '../../models/Afiliado';
@Component({
  selector: 'app-factura-add',
  templateUrl: './factura-add.component.html',
  styleUrls: ['./factura-add.component.css']
})
export class FacturaAddComponent implements OnInit {
  angForm: FormGroup;
  documentForm: FormGroup;
  afiliados: Afiliado[];
  afiliadosFilter: Afiliado[];
  seccionales: Object;
  tiposAfiliado: Object;
  tipoAfiliado: String;
  urlDocumento: String[];
  todosDocumento = [];
  seccional: String;
  fileToUpload: File = null;
  constructor(

    private fb: FormBuilder,
    private Factura_ser: FacturaService,
    private AfiliadoSer: AfiliadoService,
    private TipoafiliadoSer: TipoafiliadoService,
    private SeccionalSer: SeccionalService,
    public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      seccional: ['', Validators.required],
      afiliado: ['', Validators.required],
      numero: ['', Validators.required],
      v_pagado: ['', Validators.required],
      fecha_pago: ['', Validators.required],
      estado: ['', Validators.required]

    });
    this.documentForm =  this.fb.group({
      file: ['', Validators.required],
      detalle: ['', Validators.required],
    });
  }
  ngOnInit() {
    this.getAfiliados();
    this.getSeccionales();
    this.getTiposAfiliado();
    this.seccional = '';
    this.tipoAfiliado = '';
    this.urlDocumento = [];
  }
  getAfiliados() {
    this.AfiliadoSer.getAfiliado()
      .subscribe((data: Afiliado[]) => {
        this.afiliados = data;
      });
  }
  getSeccionales() {
    this.SeccionalSer.getSeccional()
      .subscribe((data) => {
        this.seccionales = data;
      });
  }
  getTiposAfiliado() {
    this.TipoafiliadoSer.getTipoafiliado()
      .subscribe((data) => {
       this.tiposAfiliado = data;
      });

  }
  changeSeccional(data) {
    this.afiliadosFilter = this.afiliados.filter(afiliado => afiliado['seccional'][0]._id === data);
  }
  addFactura(value ) {
      value['documento'] = this.urlDocumento || [];
     this.Factura_ser.addFactura(value)
     .subscribe((data) => {
     if (data['status'] === 'created') {
       this.showSuccess();
       this.angForm.reset();
       this.documentForm.reset();
       this.urlDocumento = [];
       this.todosDocumento = [];
     } else {
       this.showError();
     }
     });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
  }
  showError() {
    this.toastr.errorToastr('Hubo un error en la creacion de la factura intente de nuevo', 'Error');
  }

  postMethod(files: FileList) {
    let {detalle} =  this.documentForm.value;
    if (!detalle) {
      detalle = 'nulo';
    }
    this.uploadFile();
    this.fileToUpload = files.item(0);
    const formData = new FormData();
    formData.append(`singleFile`, this.fileToUpload);
   this.Factura_ser.uploadDocument(formData)
   .subscribe((data) => {
    if (data['file']) {
      this.urlDocumento.push(JSON.stringify({file: data['file'], detalle}));
      this.todosDocumento.push( {file: data['file'], detalle});
      this.showSuccess();
      this.documentForm.reset();
    } else {
      this.showError();
      this.documentForm.reset();
    }



   });

    return false;
  }
  uploadFile() {
    this.toastr.customToastr(

      `<div class="center">
      <h3 class="center" style='color:black;'>
      Subiendo
      <img class="center" width="40px" height="40px"
      src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///
      wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/
      hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+
      QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnA
      dOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQ
      JCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGB
      MRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujI
      jK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQo
      AAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDg
      sGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIp
      z8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradyleso
      jEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WG
      c3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDI
      I7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
      </h3>

    </div>`,
      null,
      {
        enableHTML: true,
        position: 'top-center',
        toastTimeout: 2000
      }
    );
  }
  deleteDocumento(index) {
    if (index > -1) {
      this.urlDocumento.splice(index, 1);
      this.todosDocumento.splice(index, 1);
  }
}
}
