/* tslint:disable */
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { FacturaService } from '../../services/factura.service';
import { AfiliadoService } from '../../services/afiliado.service';
import { SeccionalService } from '../../services/seccional.service';
import { TipoafiliadoService } from '../../services/tipoafiliado.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import Afiliado from '../../models/Afiliado';
import Factura from '../../models/Factura';
@Component({
  selector: 'app-factura-get',
  templateUrl: './factura-get.component.html',
  styleUrls: ['./factura-get.component.css']
})
export class FacturaGetComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  myTable: Boolean;
  IsAdmin: Boolean;
  IsSeccional: Boolean;
  userSec: Boolean;
  findAfiliado: Boolean;
  findFecha: Boolean;
  findNumero: Boolean;
  findEstado: Boolean;
  angForm: FormGroup;
  fechaForm: FormGroup;
  numeroForm: FormGroup;
  estadoForm: FormGroup;
  afiliados: Afiliado[];
  afiliadosFilter: Afiliado[];
  seccionales: Object;
  tiposAfiliado: Object;
  tipoAfiliado: String;
  seccional: String;
  facturas: Factura[];
  facturaFilter: Factura[];
  fileUrl;
  constructor(
    private sanitizer: DomSanitizer,
    private fb: FormBuilder,
    private Factura_ser: FacturaService,
    private AfiliadoSer: AfiliadoService,
    private TipoafiliadoSer: TipoafiliadoService,
    private SeccionalSer: SeccionalService,
    public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      seccional: ['', Validators.required],
      afiliado: ['', Validators.required]
    });
    this.fechaForm = this.fb.group({
      seccional: [null],
      fecha_inicial: ['', Validators.required],
      fecha_final: ['', Validators.required]
    });
    this.numeroForm = this.fb.group({
      seccional: [null],
      numero: ['', Validators.required]
    });
    this.estadoForm = this.fb.group({
      seccional: [null],
      estado: ['', Validators.required]
    });
  }
  ngOnInit() {

    this.myTable = false;
    this.findAfiliado = false;
    this.findFecha = true;
    this.findNumero = true;
    this.findEstado = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[1, 'asc']],
      responsive: true,
  /* below is the relevant part, e.g. translated to spanish */
  language: {
    processing: 'Procesando...',
    search: 'Buscar:',
    lengthMenu: 'Mostrar _MENU_ elementos',
    info: 'Mostrando desde _START_ al _END_ de _TOTAL_ elementos',
    infoEmpty: 'Mostrando ningún elemento.',
    infoFiltered: '(filtrado _MAX_ elementos total)',
    infoPostFix: '',
    loadingRecords: 'Cargando registros...',
    zeroRecords: 'No se encontraron registros',
    emptyTable: 'No hay datos disponibles en la tabla',
    'paginate': {
      'first': '<i class="fa fa-fast-backward" aria-hidden="true"></i>',
      'last': '<i class="fa fa-fast-forward" aria-hidden="true"></i>',
      'next': '<i class="fa fa-step-forward large" aria-hidden="true"></i>',
      'previous': '<i class="fa fa-step-backward" aria-hidden="true"></i>'
  },
    aria: {
      sortAscending: ': Activar para ordenar la tabla en orden ascendente',
      sortDescending: ': Activar para ordenar la tabla en orden descendente'
    }
  }
    };
    const user = JSON.parse(localStorage.getItem('currentUser'));
    if (user.admin) {
      this.IsAdmin = false;
    } else {
      this.IsAdmin = true;
    }

    if (user.permisos === 'SECCIONAL') {
      this.IsSeccional = true;
    }
    if (!user.admin && user.permisos === 'SECCIONAL') {
      [this.userSec] = user.seccional;
    }


    this.getAfiliados();
    this.getSeccionales();
    this.getTiposAfiliado();
    this.getFacturas();
    this.seccional = '';
    this.tipoAfiliado = '';

  }
  getAfiliados() {
    this.AfiliadoSer.getAfiliado()
      .subscribe((data: Afiliado[]) => {
        this.afiliados = data;
        if (this.userSec) {
          this.changeSeccional(this.userSec);
        }
      });
  }
  getSeccionales() {
    this.SeccionalSer.getSeccional()
      .subscribe((data) => {
        this.seccionales = data;
      });
  }
  getTiposAfiliado() {
    this.TipoafiliadoSer.getTipoafiliado()
      .subscribe((data) => {
       this.tiposAfiliado = data;
      });

  }
  getFacturas() {
    this.Factura_ser.getFactura()
      .subscribe((data: Factura[]) => {
       this.facturas = data;
      });

  }
  changeSeccional(data) {
    this.afiliadosFilter = this.afiliados.filter(afiliado => afiliado['seccional'][0]._id === data);
  }
  getFactura(value) {
     this.facturaFilter = this.facturas.filter(factura => factura['afiliado'][0]._id === value.afiliado);
     if (this.facturaFilter.length <= 0) {
      this.myTable = false;
       this.showError();
     } else {
      this.myTable = true;
       this.showSuccess();
     }
  }
  getFacturaNumero({numero}) {
    if (this.userSec) {
      this.facturaFilter = this.facturas.filter(factura => factura['afiliado'][0].seccional[0]._id === this.userSec);
    } else {
      this.facturaFilter = this.facturas;
    }

    this.facturaFilter = this.facturaFilter.filter(factura => factura['numero'] === numero);
    if (this.facturaFilter.length <= 0) {
     this.myTable = false;
      this.showError();
    } else {
     this.myTable = true;
      this.showSuccess();

      this.convert(this.facturaFilter);
    }
 }
 getFacturaEstado({estado, seccional}) {
  this.userSec = seccional;
  if (this.userSec) {
    this.facturaFilter = this.facturas.filter(factura => factura['afiliado'][0].seccional[0]._id === this.userSec);
  } else {
    this.facturaFilter = this.facturas;
  }

  this.facturaFilter = this.facturaFilter.filter(factura => factura['estado'] === estado);
  if (this.facturaFilter.length <= 0) {
   this.myTable = false;
    this.showError();
  } else {
   this.myTable = true;
    this.showSuccess();
    this.convert(this.facturaFilter);
  }
}
  getFacturaFecha(value) {
    if (this.userSec) {
      this.facturaFilter = this.facturas.filter(factura => factura['afiliado'][0].seccional[0]._id === this.userSec);
    } else {
      this.facturaFilter = this.facturas;
    }

  const  ed = new Date(value.fecha_final).getTime();
  const  sd = new Date(value.fecha_inicial).getTime();
   this.facturaFilter = this.facturaFilter.filter(d => {const time = new Date(d.fecha).getTime();
    return (sd < time && time < ed);
   });
   if (this.facturaFilter.length <= 0) {
    this.myTable = false;
     this.showError();
   } else {
    this.myTable = true;
     this.showSuccess();
     this.convert(this.facturaFilter);
   }

  }
  showSuccess() {
    this.toastr.successToastr('Consulta exitosa', 'Facturas');
  }
  showError() {
    this.toastr.warningToastr('Este afiliado no tiene facturas', 'Advertencia');
  }
  findByAfiliado() {
    this.findFecha = true;
    this.findAfiliado = false;
    this.findNumero = true;
    this.findEstado = true;
  }
  findByFecha() {
    this.findFecha = false;
    this.findAfiliado = true;
    this.findNumero = true;
    this.findEstado = true;
  }
  findByNumero() {
    this.findFecha = true;
    this.findAfiliado = true;
    this.findNumero = false;
    this.findEstado = true;
  }
  findByEstado() {
    this.findFecha = true;
    this.findAfiliado = true;
    this.findNumero = true;
    this.findEstado = false;
  }
  verTodas() {
    if (this.userSec) {
      this.facturaFilter = this.facturas.filter(factura => factura['afiliado'][0].seccional[0]._id === this.userSec);
    } else {
      this.facturaFilter = this.facturas;
    }
    this.convert(this.facturaFilter);

    this.findFecha = true;
    this.findAfiliado = true;
    this.findEstado = true;
    this.findNumero = true;
    this.myTable = true;
  }
  deleteFactura(id) {
    this.Factura_ser.deleteFactura(id)
    .subscribe((data) => {

     if (data['status'] === 'deleted') {
      this.toastr.successToastr('Factura eliminada', 'Facturas');
     } else {
      this.toastr.warningToastr('No se pudo borrar el registro', 'Error');
     }
    });
  }
  formatData(data) {
    return {
      numero_factura: data.numero,
      valor_pagado: data.v_pagado,
      fecha_pago: data.fecha_pago,
      afiliado: data.afiliado[0].nombres,
      nit: data.afiliado[0].documento,
      estado: data.estado
    };
  }

  formArrayData(datas) {
    const arrayData = [];
    for ( const data of datas) {
      arrayData.push(this.formatData(data));
    }
    return arrayData;
  }
  convert(data) {
    const otro = this.formArrayData(data);
    const keys = ['numero_factura', 'valor_pagado', 'fecha_pago', 'afiliado', 'nit', 'estado'];
    const archivo = this.ToCSV(otro, keys);

    const blob = new Blob([archivo], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }
  ToCSV(objArray, headerList) {
    const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'N,';
    for (const index in headerList) {
     row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
     let line = (i + 1) + '';
     for (const index in headerList) {
      const head = headerList[index];
      line += ',' + array[i][head];
     }
     str += line + '\r\n';
    }
    return str;
   }
}

