import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { FacturaService } from '../../services/factura.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-factura-detail',
  templateUrl: './factura-detail.component.html',
  styleUrls: ['./factura-detail.component.css']
})
export class FacturaDetailComponent implements OnInit {
  idFactura: String;
  Factura: Object;
  todosDocumento = [];
  constructor(private route: ActivatedRoute,
    public toastr: ToastrManager,
    private FacturaSer: FacturaService,

  ) {

  }

  ngOnInit() {
  this.getIdFactura();
  this.getfacturas();
  }

  getIdFactura() {
    this.route.params.subscribe(params => {
      this.idFactura = params['id'];
    });
  }
  showInfo() {
    this.toastr.infoToastr('El archivo fue subido de manera correcta', 'Archivo Guardado');
  }
  getfacturas() {

    this.FacturaSer.getFacturaById(this.idFactura)
      .subscribe((data) => {
        this.Factura = data;
        this.todosDocumento = JSON.parse('[' + this.Factura['documento'] + ']');
      });
  }


}
