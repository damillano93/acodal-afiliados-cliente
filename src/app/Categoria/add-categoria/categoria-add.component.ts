import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { CategoriaService } from '../../services/categoria.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-categoria-add',
  templateUrl: './categoria-add.component.html',
  styleUrls: ['./categoria-add.component.css']
})
export class CategoriaAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Categoria_ser: CategoriaService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addCategoria(nombre,
descripcion,
estado,
abreviacion ) {
    this.Categoria_ser.addCategoria(nombre,
descripcion,
estado,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
