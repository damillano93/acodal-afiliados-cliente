import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { CategoriaService } from '../../services/categoria.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-categoria-edit',
  templateUrl: './categoria-edit.component.html',
  styleUrls: ['./categoria-edit.component.css']
})
export class CategoriaEditComponent implements OnInit {
     angForm: FormGroup;
  categoria: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: CategoriaService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editCategoria(params['id']).subscribe(res => {
        this.categoria = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateCategoria(nombre,
descripcion,
estado,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateCategoria(nombre,
descripcion,
estado,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['categoria']);
   });
}
}
