import { Component, OnInit } from '@angular/core';
import Categoria from '../../models/Categoria';
import { CategoriaService } from '../../services/categoria.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-categoria-get',
  templateUrl: './categoria-get.component.html',
  styleUrls: ['./categoria-get.component.css']
})

export class CategoriaGetComponent implements OnInit {

  categoria: Categoria[];

  constructor( private bs: CategoriaService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getCategoria()
      .subscribe((data: Categoria[]) => {
        this.categoria = data;
    });
  }

  deleteCategoria(id) {
    this.bs.deleteCategoria(id).subscribe(res => {

      this.bs
      .getCategoria()
      .subscribe((data: Categoria[]) => {
        this.categoria = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

