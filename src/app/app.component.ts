import { Component, OnInit } from '@angular/core';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import { SidebarjsService, SidebarConfig } from 'ng-sidebarjs';
import { SeccionalService } from './services/seccional.service';
import { CategoriaService } from './services/categoria.service';
import { TipoafiliadoService } from './services/tipoafiliado.service';
import { TipoempresaService } from './services/tipoempresa.service';
import { AspirantejuridicoService } from './services/aspirantejuridico.service';
import { TipocontactoService } from './services/tipocontacto.service';
import { ContactoService } from './services/contacto.service';
import { CiudadService } from './services/ciudad.service';
import { GrupocorreosService } from './services/grupocorreos.service';
import { TipodocumentoService } from './services/tipodocumento.service';
import { TipodocumentosoporteService } from './services/tipodocumentosoporte.service';
import { DocumentoService } from './services/documento.service';
import { AspirantenaturalService } from './services/aspirantenatural.service';
import { AfiliadoService } from './services/afiliado.service';
import { GrupoafinidadService } from './services/grupoafinidad.service';
import { SubafinidadService } from './services/subafinidad.service';
import { UploadService } from './services/upload.service';
import { ClasificacionService } from './services/clasificacion.service';
import { User } from './models/User';
import { UserService } from './services/user.service';
import { first } from 'rxjs/operators';


import { NavigationCancel,
        Event,
        NavigationEnd,
        NavigationError,
        NavigationStart,
        Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentUser: User;
  username = '';
  isLogin: Boolean;
  IsAdmin: Boolean;
  users: User[] = [];
  title = 'afiliados';
  version;
  constructor(
    private userService: UserService,
    public readonly sidebarjsService: SidebarjsService,
    private _loadingBar: SlimLoadingBarService,
    private _router: Router , private SeccionalSer: SeccionalService
, private CategoriaSer: CategoriaService
,  private ClasificacionSer: ClasificacionService
, private TipoafiliadoSer: TipoafiliadoService
, private TipoempresaSer: TipoempresaService
, private AspirantejuridicoSer: AspirantejuridicoService
, private TipocontactoSer: TipocontactoService
, private ContactoSer: ContactoService
, private CiudadSer: CiudadService
, private GrupocorreosSer: GrupocorreosService
, private TipodocumentoSer: TipodocumentoService
, private TipodocumentosoporteSer: TipodocumentosoporteService
, private DocumentoSer: DocumentoService
, private AspirantenaturalSer: AspirantenaturalService
, private AfiliadoSer: AfiliadoService
, private GrupoafinidadSer: GrupoafinidadService
, private SubafinidadSer: SubafinidadService
, private UploadadSer: UploadService
) {
    this._router.events.subscribe((event: Event) => {
      this.navigationInterceptor(event);
    });
  }

  ngOnInit() {
    this.isLogin = true;
    this.IsAdmin = true;
   // this.loadAllUsers();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    if ( this.currentUser) {
      this.username = this.currentUser.username;
      this.isLogin = false;
      if ( this.currentUser.admin) {
        this.IsAdmin = false;
      }
    }

   }
  deleteUser() {
    localStorage.removeItem('currentUser');
    this._router.navigate(['login']);
}
// private loadAllUsers() {
//  this.userService.getAll().pipe(first()).subscribe(users => {
//      this.users = users;
//  });
// }
  private navigationInterceptor(event: Event): void {
    if (event instanceof NavigationStart) {
      this._loadingBar.start();
    }
    if (event instanceof NavigationEnd) {
      this._loadingBar.complete();
    }
    if (event instanceof NavigationCancel) {
      this._loadingBar.stop();
    }
    if (event instanceof NavigationError) {
      this._loadingBar.stop();
    }
  }
}
