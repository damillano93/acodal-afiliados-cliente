import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipoafiliadoService } from '../../services/tipoafiliado.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-tipoafiliado-add',
  templateUrl: './tipoafiliado-add.component.html',
  styleUrls: ['./tipoafiliado-add.component.css']
})
export class TipoafiliadoAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Tipoafiliado_ser: TipoafiliadoService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addTipoafiliado(nombre,
descripcion,
estado,
abreviacion ) {
    this.Tipoafiliado_ser.addTipoafiliado(nombre,
descripcion,
estado,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
