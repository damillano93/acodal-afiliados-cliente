import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipoafiliadoService } from '../../services/tipoafiliado.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipoafiliado-edit',
  templateUrl: './tipoafiliado-edit.component.html',
  styleUrls: ['./tipoafiliado-edit.component.css']
})
export class TipoafiliadoEditComponent implements OnInit {
     angForm: FormGroup;
  tipoafiliado: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: TipoafiliadoService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
estado: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editTipoafiliado(params['id']).subscribe(res => {
        this.tipoafiliado = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateTipoafiliado(nombre,
descripcion,
estado,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateTipoafiliado(nombre,
descripcion,
estado,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['tipoafiliado']);
   });
}
}
