import { Component, OnInit } from '@angular/core';
import Tipoafiliado from '../../models/Tipoafiliado';
import { TipoafiliadoService } from '../../services/tipoafiliado.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipoafiliado-get',
  templateUrl: './tipoafiliado-get.component.html',
  styleUrls: ['./tipoafiliado-get.component.css']
})

export class TipoafiliadoGetComponent implements OnInit {

  tipoafiliado: Tipoafiliado[];

  constructor( private bs: TipoafiliadoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getTipoafiliado()
      .subscribe((data: Tipoafiliado[]) => {
        this.tipoafiliado = data;
    });
  }

  deleteTipoafiliado(id) {
    this.bs.deleteTipoafiliado(id).subscribe(res => {

      this.bs
      .getTipoafiliado()
      .subscribe((data: Tipoafiliado[]) => {
        this.tipoafiliado = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

