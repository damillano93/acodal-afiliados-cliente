import { Component, OnInit } from '@angular/core';
import {User} from '../../models/User';
import { UserService } from '../../services/user.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-user-get',
  templateUrl: './user-get.component.html',
  styleUrls: ['./user-get.component.css']
})

export class UserGetComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  myTable: Boolean;
  user: User[];

  constructor( private bs: UserService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.myTable = false;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[1, 'asc']],
      responsive: true,
  /* below is the relevant part, e.g. translated to spanish */
  language: {
    processing: 'Procesando...',
    search: 'Buscar:',
    lengthMenu: 'Mostrar _MENU_ elementos',
    info: 'Mostrando desde _START_ al _END_ de _TOTAL_ elementos',
    infoEmpty: 'Mostrando ningún elemento.',
    infoFiltered: '(filtrado _MAX_ elementos total)',
    infoPostFix: '',
    loadingRecords: 'Cargando registros...',
    zeroRecords: 'No se encontraron registros',
    emptyTable: 'No hay datos disponibles en la tabla',
    'paginate': {
      'first': '<i class="fa fa-fast-backward" aria-hidden="true"></i>',
      'last': '<i class="fa fa-fast-forward" aria-hidden="true"></i>',
      'next': '<i class="fa fa-step-forward large" aria-hidden="true"></i>',
      'previous': '<i class="fa fa-step-backward" aria-hidden="true"></i>'
  },
    aria: {
      sortAscending: ': Activar para ordenar la tabla en orden ascendente',
      sortDescending: ': Activar para ordenar la tabla en orden descendente'
    }
  }
    };

    this.bs
      .getUser()
      .subscribe((data: User[]) => {
        this.myTable = true;
        this.user = data;

    });
  }

  deleteUser(id) {
    this.bs.deleteUser(id).subscribe(res => {

      this.bs
      .getUser()
      .subscribe((data: User[]) => {
        this.user = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

