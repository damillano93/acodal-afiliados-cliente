import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import {AlertService} from '../services/alert.service';
import {UserService} from '../services/user.service';
import Seccional from '../models/Seccional';
import { SeccionalService } from '../services/seccional.service';
@Component({templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    match = true;
    Seccional: Seccional[];
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private seccionalService: SeccionalService) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstname: ['', Validators.required],
            lastname: ['', Validators.required],
            username: ['', Validators.required],
            seccional:  ['', Validators.required],
            cargo: ['', Validators.required],
            permisos: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });

        this.seccionalService
        .getSeccional()
        .subscribe((data: Seccional[]) => {
          this.Seccional = data;
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.addUser(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/usuarios']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
    changePass(pass) {

       if (pass !== this.registerForm.value.password) {
         this.match = false;
       } else {
        this.match = true;
       }
    }
}
