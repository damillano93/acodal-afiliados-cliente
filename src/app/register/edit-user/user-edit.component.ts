import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import {AlertService} from '../../services/alert.service';
import {UserService} from '../../services/user.service';
import Seccional from '../../models/Seccional';
import { SeccionalService } from '../../services/seccional.service';
@Component({templateUrl: 'user-edit.component.html'})
export class UserEditComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    match = true;
    Seccional: Seccional[];
    user = {};
    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private seccionalService: SeccionalService) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.userService.editUser(params['id']).subscribe(res => {
              this.user = res;

            });
          });

        this.registerForm = this.formBuilder.group({
            firstname: [null],
            lastname: [null],
            username: [null],
            seccional:  [null],
            cargo: [null],
            permisos: [null],
            password: [null]
        });

        this.seccionalService
        .getSeccional()
        .subscribe((data: Seccional[]) => {
          this.Seccional = data;
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.route.params.subscribe(params => {
            this.userService.updateUser( this.registerForm.value , params['id']);

            this.router.navigate(['usuarios']);
         });
    }

}
