import { Component, OnInit } from '@angular/core';
import Aspirantejuridico from '../../models/Aspirantejuridico';
import { AspirantejuridicoService } from '../../services/aspirantejuridico.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-aspirantejuridico-get',
  templateUrl: './aspirantejuridico-get.component.html',
  styleUrls: ['./aspirantejuridico-get.component.css']
})

export class AspirantejuridicoGetComponent implements OnInit {

  aspirantejuridico: Aspirantejuridico[];

  constructor( private bs: AspirantejuridicoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getAspirantejuridico()
      .subscribe((data: Aspirantejuridico[]) => {
        this.aspirantejuridico = data;
    });
  }

  deleteAspirantejuridico(id) {
    this.bs.deleteAspirantejuridico(id).subscribe(res => {

      this.bs
      .getAspirantejuridico()
      .subscribe((data: Aspirantejuridico[]) => {
        this.aspirantejuridico = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

