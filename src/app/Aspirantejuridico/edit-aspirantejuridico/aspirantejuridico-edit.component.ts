import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Ciudad from '../../models/Ciudad';
import Seccional from '../../models/Seccional';
import Grupoafinidad from '../../models/Grupoafinidad';
import Tipoempresa from '../../models/Tipoempresa';

import { AspirantejuridicoService } from '../../services/aspirantejuridico.service';
import { CiudadService } from '../../services/ciudad.service';
import { SeccionalService } from '../../services/seccional.service';
import { TipoempresaService } from '../../services/tipoempresa.service';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-aspirantejuridico-edit',
  templateUrl: './aspirantejuridico-edit.component.html',
  styleUrls: ['./aspirantejuridico-edit.component.css']
})
export class AspirantejuridicoEditComponent implements OnInit {
  Ciudad: Ciudad[];
Seccional: Seccional[];
Tipoempresa: Tipoempresa[];
   angForm: FormGroup;
  aspirantejuridico: any = {};
  Grupoafinidad: Grupoafinidad[];
  Subafinidad: Array<any>;
  constructor(private route: ActivatedRoute, private CiudadSer: CiudadService,
 private SeccionalSer: SeccionalService,
 private GrupoafinidadSer: GrupoafinidadService,
 private TipoempresaSer: TipoempresaService,

    private router: Router,
    public toastr: ToastrManager,
    private bs: AspirantejuridicoService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        fecha: ['', Validators.required ],
razonsocial: ['', Validators.required ],
CIUU: ['', Validators.required ],
Nit: ['', Validators.required ],
direccion: ['', Validators.required ],
telefono: ['', Validators.required ],
email: ['', Validators.required ],
ciudad: ['', Validators.required ],
servicios: ['', Validators.required ],
nombregerente: ['', Validators.required ],
emailgerente: ['', Validators.required ],
celulargerente: ['', Validators.required ],
paginaweb: ['', Validators.required ],
subafinidad: ['', Validators.required ],
seccional: ['', Validators.required ],
tipoempresa: ['', Validators.required ],
abeasdata: ['', Validators.required ],
lavadoactivos: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editAspirantejuridico(params['id']).subscribe(res => {
        this.aspirantejuridico = res;
      });
    });
     this.CiudadSer
.getCiudad()
.subscribe((data: Ciudad[]) => {
this.Ciudad = data;
});
this.SeccionalSer
.getSeccional()
.subscribe((data: Seccional[]) => {
this.Seccional = data;
});
this.TipoempresaSer
.getTipoempresa()
.subscribe((data: Tipoempresa[]) => {
this.Tipoempresa = data;
});
this.GrupoafinidadSer
.getGrupoafinidad()
.subscribe((data: Grupoafinidad[]) => {
this.Grupoafinidad = data;
});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateAspirantejuridico(fecha,
razonsocial,
CIUU,
Nit,
direccion,
telefono,
email,
ciudad,
servicios,
nombregerente,
emailgerente,
celulargerente,
paginaweb,
seccional,
tipoempresa,
abeasdata,
lavadoactivos ) {
   this.route.params.subscribe(params => {
      this.bs.updateAspirantejuridico(fecha,
razonsocial,
CIUU,
Nit,
direccion,
telefono,
email,
ciudad,
servicios,
nombregerente,
emailgerente,
celulargerente,
paginaweb,
seccional,
tipoempresa,
abeasdata,
lavadoactivos  , params['id']);
      this.showInfo();
      this.router.navigate(['aspirantejuridico']);
   });
}

changeCountry(count) {
  this.Subafinidad = this.Grupoafinidad.find(con => con.nombre === count).subafinidad;

}
}
