import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Ciudad from '../../models/Ciudad';
import Grupoafinidad from '../../models/Grupoafinidad';
import Seccional from '../../models/Seccional';
import Tipoempresa from '../../models/Tipoempresa';

import { AspirantejuridicoService } from '../../services/aspirantejuridico.service';
import { CiudadService } from '../../services/ciudad.service';
import { GrupoafinidadService } from '../../services/grupoafinidad.service';
import { SeccionalService } from '../../services/seccional.service';
import { TipoempresaService } from '../../services/tipoempresa.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-aspirantejuridico-add',
  templateUrl: './aspirantejuridico-add.component.html',
  styleUrls: ['./aspirantejuridico-add.component.css']
})
export class AspirantejuridicoAddComponent implements OnInit {

  constructor(
private CiudadSer: CiudadService,
private GrupoafinidadSer: GrupoafinidadService,
private SeccionalSer: SeccionalService,
private TipoempresaSer: TipoempresaService,

   private fb: FormBuilder,
   private Aspirantejuridico_ser: AspirantejuridicoService,
   public toastr: ToastrManager) {
    this.createForm();
  }
   Ciudad: Ciudad[];
   Grupoafinidad: Grupoafinidad[];
Seccional: Seccional[];
Tipoempresa: Tipoempresa[];
   angForm: FormGroup;


// countryList: Array<any> = [
//    { nombre: 'Germany', subafinidad: [{nombre:'Duesseldorf'}, {nombre:'Leinfelden-Echterdingen'}, {nombre:'Eschborn'}] },
//    { nombre: 'Spain', subafinidad: [{nombre:'Barcelona'}] },
//    { nombre: 'USA', subafinidad: [{nombre:'Downers Grove'}] },
//    { nombre: 'Mexico', subafinidad: [{nombre:'Puebla'}] },
//    { nombre: 'China', subafinidad: [{nombre:'Beijing'}] },
//  ];

  Subafinidad: Array<any>;

  createForm() {
    this.angForm = this.fb.group({
       fecha: ['', Validators.required ],
razonsocial: ['', Validators.required ],
CIUU: ['', Validators.required ],
Nit: ['', Validators.required ],
direccion: ['', Validators.required ],
telefono: ['', Validators.required ],
email: ['', Validators.required ],
ciudad: ['', Validators.required ],
servicios: ['', Validators.required ],
nombregerente: ['', Validators.required ],
emailgerente: ['', Validators.required ],
celulargerente: ['', Validators.required ],
paginaweb: ['', Validators.required ],
seccional: ['', Validators.required ],
subafinidad: ['', Validators.required ],
tipoempresa: ['', Validators.required ],
abeasdata: ['', Validators.required ],
lavadoactivos: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addAspirantejuridico(fecha,
razonsocial,
CIUU,
Nit,
direccion,
telefono,
email,
ciudad,
servicios,
nombregerente,
emailgerente,
celulargerente,
paginaweb,
seccional,
tipoempresa,
subafinidad,
abeasdata,
lavadoactivos ) {

    this.Aspirantejuridico_ser.addAspirantejuridico(fecha,
razonsocial,
CIUU,
Nit,
direccion,
telefono,
email,
ciudad,
servicios,
nombregerente,
emailgerente,
celulargerente,
paginaweb,
seccional,
tipoempresa,
subafinidad,
abeasdata,
lavadoactivos );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {




    this.CiudadSer
.getCiudad()
.subscribe((data: Ciudad[]) => {
this.Ciudad = data;
});
this.GrupoafinidadSer
.getGrupoafinidad()
.subscribe((data: Grupoafinidad[]) => {
this.Grupoafinidad = data;

});
this.SeccionalSer
.getSeccional()
.subscribe((data: Seccional[]) => {
this.Seccional = data;
});
this.TipoempresaSer
.getTipoempresa()
.subscribe((data: Tipoempresa[]) => {
this.Tipoempresa = data;
});

  }



  changeCountry(count) {
    this.Subafinidad = this.Grupoafinidad.find(con => con.nombre === count).subafinidad;

  }
}
