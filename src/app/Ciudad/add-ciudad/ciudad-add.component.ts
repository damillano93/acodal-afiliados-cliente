import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { CiudadService } from '../../services/ciudad.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-ciudad-add',
  templateUrl: './ciudad-add.component.html',
  styleUrls: ['./ciudad-add.component.css']
})
export class CiudadAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Ciudad_ser: CiudadService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addCiudad(nombre,
descripcion,
abreviacion ) {
    this.Ciudad_ser.addCiudad(nombre,
descripcion,
abreviacion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
