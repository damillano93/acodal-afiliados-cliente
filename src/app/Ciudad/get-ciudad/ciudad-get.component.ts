import { Component, OnInit } from '@angular/core';
import Ciudad from '../../models/Ciudad';
import { CiudadService } from '../../services/ciudad.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-ciudad-get',
  templateUrl: './ciudad-get.component.html',
  styleUrls: ['./ciudad-get.component.css']
})

export class CiudadGetComponent implements OnInit {

  ciudad: Ciudad[];

  constructor( private bs: CiudadService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getCiudad()
      .subscribe((data: Ciudad[]) => {
        this.ciudad = data;
    });
  }

  deleteCiudad(id) {
    this.bs.deleteCiudad(id).subscribe(res => {

      this.bs
      .getCiudad()
      .subscribe((data: Ciudad[]) => {
        this.ciudad = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

