import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { CiudadService } from '../../services/ciudad.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-ciudad-edit',
  templateUrl: './ciudad-edit.component.html',
  styleUrls: ['./ciudad-edit.component.css']
})
export class CiudadEditComponent implements OnInit {
     angForm: FormGroup;
  ciudad: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: CiudadService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
abreviacion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editCiudad(params['id']).subscribe(res => {
        this.ciudad = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateCiudad(nombre,
descripcion,
abreviacion ) {
   this.route.params.subscribe(params => {
      this.bs.updateCiudad(nombre,
descripcion,
abreviacion  , params['id']);
      this.showInfo();
      this.router.navigate(['ciudad']);
   });
}
}
