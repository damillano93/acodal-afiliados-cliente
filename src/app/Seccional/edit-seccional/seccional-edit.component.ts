import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { SeccionalService } from '../../services/seccional.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-seccional-edit',
  templateUrl: './seccional-edit.component.html',
  styleUrls: ['./seccional-edit.component.css']
})
export class SeccionalEditComponent implements OnInit {
     angForm: FormGroup;
  seccional: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: SeccionalService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
direccion: ['', Validators.required ],
ciudad: ['', Validators.required ],
cubrimiento: ['', Validators.required ],
estado: ['', Validators.required ],
telefono: ['', Validators.required ],
email: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editSeccional(params['id']).subscribe(res => {
        this.seccional = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateSeccional(nombre,
descripcion,
direccion,
ciudad,
cubrimiento,
estado,
telefono,
email ) {
   this.route.params.subscribe(params => {
      this.bs.updateSeccional(nombre,
descripcion,
direccion,
ciudad,
cubrimiento,
estado,
telefono,
email  , params['id']);
      this.showInfo();
      this.router.navigate(['seccional']);
   });
}
}
