import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { SeccionalService } from '../../services/seccional.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-seccional-add',
  templateUrl: './seccional-add.component.html',
  styleUrls: ['./seccional-add.component.css']
})
export class SeccionalAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Seccional_ser: SeccionalService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
direccion: ['', Validators.required ],
ciudad: ['', Validators.required ],
cubrimiento: ['', Validators.required ],
estado: ['', Validators.required ],
telefono: ['', Validators.required ],
email: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addSeccional(nombre,
descripcion,
direccion,
ciudad,
cubrimiento,
estado,
telefono,
email ) {
    this.Seccional_ser.addSeccional(nombre,
descripcion,
direccion,
ciudad,
cubrimiento,
estado,
telefono,
email );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
