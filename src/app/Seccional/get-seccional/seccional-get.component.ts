import { Component, OnInit } from '@angular/core';
import Seccional from '../../models/Seccional';
import { SeccionalService } from '../../services/seccional.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-seccional-get',
  templateUrl: './seccional-get.component.html',
  styleUrls: ['./seccional-get.component.css']
})

export class SeccionalGetComponent implements OnInit {

  seccional: Seccional[];

  constructor( private bs: SeccionalService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getSeccional()
      .subscribe((data: Seccional[]) => {
        this.seccional = data;
    });
  }

  deleteSeccional(id) {
    this.bs.deleteSeccional(id).subscribe(res => {

      this.bs
      .getSeccional()
      .subscribe((data: Seccional[]) => {
        this.seccional = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

