
FROM nginx:1.16.0-alpine
RUN mkdir -p /data
WORKDIR /data
COPY . .
COPY /nginx.conf /etc/nginx/conf.d/default.conf
COPY /dist/angular7crud  /usr/share/nginx/html

# expose port 80
EXPOSE 80

# run nginx
CMD ["nginx", "-g", "daemon off;"]